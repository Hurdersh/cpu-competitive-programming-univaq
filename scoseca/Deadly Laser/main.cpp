#include <iostream>
using namespace std;

int main(){
    int t;
    cin >> t;
    int n, m;
    int DP[ 1000 ] [ 1000 ];
    int Lx, Ly;
    int d;
    while( t-- ){
        cin >> n >> m;
        cin >> Lx >> Ly;
        cin >> d;
        Lx--;
        Ly--;

        //generazione laser dove c'è il laser viene inserito il valore 2002
        for( int i = Lx - d; i <= Lx + d && i < n; i++ ){
            for( int j = Ly - d; j <= Ly + d && j < m; j++ ){
                if( i < 0 ){
                    while ( i < 0 ){
                        i++;
                    }
                }
                if( j < 0 ){
                    while( j < 0 ){
                        j++;
                    }
                }
                DP[ i ][ j ] = 2002;
            }
        }
        
        //caso base
        DP[ 0 ] [ 0 ] = 0;
        
        //generazione prima riga
        for( int i = 1; i < n; i++ ){
            if( DP[ i ][ 0 ] == 2002 )
                continue;
            DP[ i ][ 0 ] = DP[ i - 1 ][ 0 ] + 1;
        }

        //generazione prima colonna
        for( int i = 1; i < m; i++ ){
             if( DP[ 0 ][ i ] == 2002 )
                continue;
            DP[ 0 ][ i ] = DP[ 0 ][ i - 1 ] + 1;
        }
        
        //generazione soluzione
        for( int i = 1; i < n; i++ ){
            for( int j = 1; j < m; j++ ){
                if( DP[ i ][ j ] == 2002 )
                    continue;
                DP[ i ] [ j ] = min( DP[ i - 1 ][ j ], DP[ i ][ j - 1 ] ) + 1;
            }
        }
        
        if( DP[ n - 1 ][ m - 1 ] >= 2002 )
            cout << "-1" << endl;
        else   
            cout << DP[ n - 1 ][ m - 1 ] << endl; 
        for( int i = 0; i < n; i++ ){
            for( int j = 0; j < m; j++ ){
                DP[ i ][ j ] = 0;
            }
        }
    }
    return 0;
}