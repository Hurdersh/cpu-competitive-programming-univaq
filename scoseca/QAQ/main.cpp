#include <iostream>

using namespace std;

int main(){
    string s;
    cin >> s;
    int count = 0;
    int numberOfQ = 0;
    int numberOfA = 0;

    for( int i = 0; i < s.size(); i++ ){
        //Per ogni Q che trovo posso aumentare di quante A ho trovato fino a quel momento
        //Al primo ciclo essendo inizializzati a 0 alla prima Q che trovo il counter rimarrà a 0 aumenterà solo il numero di Q
        if( s[ i ] == 'Q' ){
            count+=numberOfA;
            numberOfQ++;
        }
        //Per ogni A che trovo posso aumentare massimo del numero di Q che ho trovato fino a quel momento
        //Se una stringa inizia con delle A verrà eseguito 0 + 0 fino a che non verrà trovata una Q e quindi non conteggiate nell'enumerazione totale delle QAQ
        if( s[ i ] == 'A' ){
            numberOfA += numberOfQ;
        }
    }

    cout << count;
    return 0;
}
