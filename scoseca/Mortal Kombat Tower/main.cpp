#include <iostream>

using namespace std;


/*
Mi devo fare un quadro con la cit. di Giordano Colli

        |******************************|
        |                              |
        |                              |
        |          Top-Down            |
        |            non               |
        |          Bottom-Up           |
        |                              |
        |                              |
        |******************************|


Formulazione DP

Inverto l'ordine della sequenza a per facilitare lo scorrimento della sequenza per creare la soluzione

DP[ i ][ 0 ] = numero minimo di skip quando affronto l'i-esimo boss come scarso
DP[ i ][ 1 ] = numero minimo di skip quando affronto l'i-esimo boss come forte

DP[ i ][ 1 ] = min( DP[ i - 2 ][ 0 ], DP[ i - 1 ][ 0 ] )  cio� scelgo il minimo tra sconfiggere un boss o due boss e passo allo scarso

Per DP[ i ][ 0 ] invece bisogna specificare tutti i casi in cui possiamo cadere

Se a[ i ] = 0 e a[ i - 1 ] = 0  DP[ i ][ 0 ] = min( DP[ i - 1 ][ 1 ], DP[ i - 2 ][ 1 ] ) cio� sconfiggo un boss o due boss e passo all'amico forte

Se a[ i ] = 0 e a[ i - 1 ] = 1  DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 1, DP[ i - 1 ][ 1 ] ) cio� sconfiggo un boss e passo oppure due boss e passo

Se a[ i ] = 1 e a[ i - 1 ] = 0  DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 1, DP[ i - 1 ][ 1 ] + 1 ) cio� sconfiggo il primo boss difficile tramite skip e passo oppure sconfiggo il primo boss tramite skip ed anche il secondo e poi passo

Se a[ i ] = 1 e a[ i - 1 ] = 1  DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 2, DP[ i - 1 ][ 1 ] + 1 ) cio� sconfiggo il primo boss tramite skip e passo oppure sconfiggo sia il primo che il secondo boss tramite skip e passo

*/





int main(){
    int a[ 200000 ];
    // quando la riga � uguale a 0 si intende lo scarso quando � 1 si intende quello forte
    int DP[ 200000 ][ 2 ];
    int t;
    int n;
    cin >> t;

    while( t-- ) {
        cin >> n;
        for( int i = 0; i < n; i++ ){
            cin >> a[ n - i - 1 ];
        }

        //caso base comprendendo caso in cui n = 1
        DP[ 0 ][ 1 ] = 0;
        DP[ 1 ][ 1 ] = 0;
        DP[ 0 ][ 0 ] = a[ 0 ];
        if( n == 1 ){
            cout << DP[ 0 ][ 0 ] << endl;
        } else {
            DP[ 1 ][ 0 ] = a[ 1 ];

            for( int i = 2; i < n; i++ ){
                if( a[ i ] == 0 ){
                    if( a[ i - 1 ] == 0 ){
                        // caso 00
                        DP[ i ][ 0 ] = min( DP[ i - 1 ][ 1 ], DP[ i - 2 ][ 1 ] );
                    } else {
                        // caso 01
                        DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 1, DP[ i - 1 ][ 1 ] );
                    }
                } else {
                    if( a[ i - 1 ] == 0 ){
                        // caso 10
                        DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 1, DP[ i - 1 ][ 1 ] + 1 );
                    } else {
                        // caso 11
                        DP[ i ][ 0 ] = min( DP[ i - 2 ][ 1 ] + 2, DP[ i - 1 ][ 1 ] + 1 );
                    }
                }
                DP[ i ][ 1 ] = min( DP[ i - 2 ][ 0 ], DP[ i - 1 ][ 0 ] );
            }
            cout << DP[ n - 1 ][ 0 ] << endl;
        }
    }
    system("pause");
    return 0;
}
