/* 
    quanti elementi devo togliere affinchè se faccio lo spostamento a sx e dx sono gli stessi?
    un array per essere "buono" o deve avere 2 elementi, oppure può avere più di due elementi ma che siano identici: ad esempio "0000", "11111","222222" ecc...
    quindi se dopo aver fatto entrambi gli spostamenti vedo che ci sono elementi diversi, allora so che dovrò eliminare quegli elementi per render buono l'array, quindi uso dp per segnare gli elementi che dovrò togliere.
*/
#include <iostream>
using namespace std;

const int MAXN = 200005;
int t, n;        // t = numero di set, n = numero di numeri da 0 - 9
long a[MAXN];    // array originale
long dp[MAXN];   // numeri di elementi che dovrò eliminare per avere la stringa identica.
int main(void) {

  cin >> t;
    while (t--) {
        cin >> n;
        for (int i = 1; i <= n; i++) {  
            cin >> a[i]; // inizializzo l'array di boss
        }
/*    
long temp1[n];

    for(int i = 1; i < n; i++){         
        temp1[i] = a[i+1];
//        cout<<temp1[i];
    }
    temp1[n] = a[1];
//        cout<<temp1[n];
     

long temp2[n];

    temp2[1] = a[n];
   
    for (int i = 2; i <= n; i++){ // spostamento a dx
        temp2[i] = a[i-1];
        }
*/

// confronti per capire quante eliminazioni si dovrebbero pare per avere la stringa buona.            

for(int i = 1; i<=n; i++){
    for (int j = 1; j < n; j++){
        if(a[i] == a[j])
            dp[i] += 1;
    }

    if(i > 1){    
    dp[i] = max(dp[i-1], dp[i+1]);
    }
}
    int count;

    int eliminoElementi;
    
    if(dp[n] > 2){
      eliminoElementi = n - dp[n];
    }
    else{
      eliminoElementi = n - 2;
    }
    

     cout << eliminoElementi << endl;
}    
    return 0;
}
  
// conto numeri elementi uguali, se gli elementi uguali sono maggiori di 3 allora si fa n - 3 o più, altrimenti n-2






