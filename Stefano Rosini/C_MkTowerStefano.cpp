#include <iostream>
using namespace std;
/* array a =          [1,0,1,1,0,1,1,1]
                    /                   \
   amico uccide un boss                  //amico uccide 2 boss
        [0,1,1,0,1,1,1]                      [1,1,0,1,1,1]
                /   \                        /            \
io uccido 1 boss    io uccido 2 boss ; uccido 1 boss    uccido 2 boss
    [1,1,0,1,1,1]   [1,0,1,1,1]*       [1,0,1,1,1]*       [0,1,1,1]

    * = soluzioni che si ripetono
*/


const int MAXN = 200005;
int t, n;        // t = numero di set, n = numero di boss
long long a[MAXN];     // array dei boss dove viene inserita la difficoltà
long long dp[MAXN][2]; // array bidimensionale dove si inserisce il numero di skip usati per i boss. [2] perchè vogliamo vedere il caso in cui è il tunro dell'amico e il caso in cui è il mio turno.
int main() {
  cin >> t;
    while (t--) {
        cin >> n;
        for (int i = 1; i <= n; i++) {  
            cin >> a[i]; // inizializzo l'array di boss
        }
        dp[1][0] = a[1]; // l'amico cominincia e deve uccidere per forza un boss, quindi restituisco il valore di a[1].
        dp[1][1] = 0;    // io non faccio niente al primo turno, quindi alla posizione 1 assegno 0.
        for (int i = 2; i <= n; i++) {
            dp[i][0] = min(dp[i - 1][0], dp[i - 1][1]) + a[i];     /* numero minimo di skip usati nel caso in cui è il turno dell'amico (pongo il caso in cui l'amico uccide 1 boss perchè se ho  ad esempio la stringa "0,1" 
                                                                      l'amico uccide solamente il boss 0; nel caso in cui si ha "1,0" l'amico uccide entrambi i boss, ma io per avere l'aggiornamento dello skip basta che 
                                                                      uccido 1 solo boss in questo caso. Non prendo in considerazione il caso in cui l'amico uccide 2 boss perchè so gia che la soluzione non restituirebbe 
                                                                      il minimo numero di skip);
                                                                   */   
            dp[i][1] = min(dp[i - 2][0], dp[i - 2][1]) +(1-a[i])+ a[i-1];   // numero di skip usati nel caso in cui è il mio turno (quindi uccido 2 boss);
            
        ;
        }
          cout << min(dp[n][0], dp[n][1])<< endl;
    }
          
           return 0;
}
