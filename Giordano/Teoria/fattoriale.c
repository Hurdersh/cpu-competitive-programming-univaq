/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Problem name: Fattoriale
 *
 */

#include<stdio.h>

unsigned long long factorialIterative(unsigned long long n){
  //inizializzo ad 1 la variabile result per poterla moltiplicare
  unsigned long long result = 1;
  //moltiplico tutti i numeri minori di n per result
  //se n=3 => 2 cicli, result = result * 2, result = result * 3 
  for(unsigned long long i=2;i<=n;i++){
    result = result * i;
  }
  return result;
}

long long factorialRecursive(unsigned long long n){
  //base della ricorsione
  if(n == 0 || n == 1){
    return 1;
  }
  //se non sono arrivato a 0 o ad 1 ritorno la moltiplicazione 
  //del numero attuale con il fattoriale di n-1
  return n * factorialRecursive(n-1);
}

//inizia a leggere il codice a partire da qui

int main(void){
  unsigned long long n;
  printf("inserisci il numero di cui fare il fattoriale: ");
  scanf("%llu",&n);
  //se il numero inserito e' negativo ritorna un errore (255)
  if(n < 0){
    return 255;
  }
  printf("il fattoriale calcolato iterativamente vale: %llu\n", factorialIterative(n));
  printf("il fattoriale calcolato ricorsivamente vale: %llu\n", factorialRecursive(n));
  return 0;
}


