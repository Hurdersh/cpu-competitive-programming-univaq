/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: maximum increasing subsequence, Dynamic Programming
 *  Problem name: Maximum Increase
 *  Link Problem: https://codeforces.com/problemset/problem/702/A
 *
 */

#include <stdio.h>
#include <stdlib.h>

//questo si potrebbe addirittura applicare durante 
//i tempi di input
int solve(int* A,int n){
  int max = 1;
  int dp=1;
  for(int i=1;i<n;i++){
    if(A[i] > A[i-1]){
      dp++;
      if(dp > max){
        max = dp;
      }
    }
    else{
      dp = 1;
    }
  }
  return max;
}

int solve_naive(int* A,int n){
  int* dp;
  int max;
  dp = malloc(sizeof(int) * n);
  //dp[i] = lunghezza della massima subsequence presente nelle prime i locazioni
  //ovviamente dp[0] = 1, un numero e' una sottosequenza
  dp[0] = 1; 
  max = dp[0];
  //se aggiungo un numero o esso si somma alla precedente subsequence crescente massima
  //oppure ne inizia una nuova :)
  for(int i=1;i<n;i++){
    if(A[i] > A[i-1]){
      dp[i] = dp[i-1] + 1;
    }
    else{
      dp[i] = 1;
    }
  }

  for(int i=1;i<n;i++){
    if(dp[i] > max){
      max = dp[i];
    }
  }
  free(dp);
  return max;
}

int main(void){
  int n;
  int* A;
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }
  printf("%d\n",solve(A,n));
  free(A);
  return 0;
}

