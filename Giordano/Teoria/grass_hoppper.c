/*
 *  Author: Giordano Colli
 *  Date: 27-10-2022
 *  Language: C
 *  Problem name: Grass Hopper
 *
 */

#include<stdio.h>
#include<stdlib.h>

int min(int a,int b){
  if(a < b)
    return a;
  return b;
}
int main( void ) {
  //suppongo n >= 3;
  int n;
  int j;
  int* A;
  int* DP;
  scanf("%d",&n);
  
  A = malloc(sizeof(int) * n);
  //minimo costo per arrivare in DP[i]
  DP = malloc(sizeof(int) * n);

  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }

  //casi base
  DP[0] = 0; //per andare nella posizione iniziale il costo e' nullo
  DP[1] = A[1]; //per andare nella prima posizione il costo e' il salto da A[0] ad A[1] 
  //DP
  for(int i=2;i<n;i++){
    DP[i] = min(DP[i-1]+A[i],DP[i-2]+A[i]);
  }
  //BackTracking
  j = n-1;
  printf("%d ",j);
  while(j >= 2){
    if(DP[j] == DP[j-1] + A[j]){
      printf("%d ",j-1);
      j--;
    }
    if(DP[j] == DP[j-2] + A[j]){
      printf("%d ",j-2);
      j-=2;
    }
  }

  return 0;
}

