/*
 *  Author: Giordano Colli
 *  Date: 25-08-2022
 *  Language: C
 *  Problem name: Ordering
 *  Credits: Dijkstra
 *  Algorithm: Merge Sort
 *  Time Complexity: Theta(n*log(n))
 */

#include<stdio.h>
#include<stdlib.h>

void merge(int* A, int l, int m,int r){
  int i = l,j = m+1,k=0;
  int* result = malloc(sizeof(int) * (r-l+1));
  while(i <= m && j <= r){
    if(A[i] < A[j]){
      result[k] = A[i];
      i++;
    }
    else{
      result[k] = A[j];
      j++;
    }
    k++;
  }
  while(i <= m){
    result[k] = A[i];
    i++;
    k++;
  }
  while(j <= r){
    result[k] = A[j];
    j++;
    k++;
  }
  for(int h = 0;h < r-l+1; h++){
    A[h + l] = result[h];
  }
  free(result);
}

void mergesort(int* A, int l, int m,int r){
  if(l < r){
    mergesort(A,l,(l+m)/2,m);
    mergesort(A,m+1,(m+1+r)/2, r);
    merge(A,l,m,r);
  }
}





int main( void ){
  int n;
  int* array;
  scanf("%d",&n);
  for(int i=0;i<n;i++){
    scanf("%d",array+i);
  }
  mergesort(array,0,n/2,n-1);
  for(int i=0;i<n;i++){
    printf("%d ",*(array + i));
  }

  return 0;
}


