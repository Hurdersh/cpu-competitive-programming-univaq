/*
 *  Author: Giordano Colli
 *  Date: 13-10-2022
 *  Language: C
 *  Problem name: Sum Segment Tree 
 */

#include<stdio.h>
#include<stdlib.h>

//i is the node to update, it has univoque corrispondence with range [i-i]
//and v is the value to add
//[x,y] is the range represented by node
//v is always called as 0
void update(int i, int value, int x, int y, int v, int* segment_tree, int segment_tree_size){
  //se sono arrivato al nodo foglia
  if(i == x && i == y){
    segment_tree[v] += value;
    return;
  }
  //se i e' compresa nel segmento cioe' [i,i] e' contenuto in [x,y]
  if(i >= x && i <= y){
    //aggiorno il valore
    segment_tree[v] += value;
    //chiamo ricorsivamente i 2 nodi
    int mid = (x+y)/2;
    update(i,value,x,mid,2*v + 1,segment_tree,segment_tree_size);
    update(i,value,mid+1,y,2*v + 2,segment_tree,segment_tree_size);
  }
}
//[i,j] is a segment that i want to find, it is fixed
//[x,y] is the segment represented from tree node
//v is tree node
//la somma segue effettivamente al piu' 2 path per ogni piano => O(4log_n) 
//per via delle chiamate in cui mi 'accorgo' di essere sceso troppo di livello
//=> O(logn)
int sum(int i,int j,int x,int y,int v,int* segment_tree,int segment_tree_size){
  if(i > y || x > j){
    return 0;
  }
  if(x >= i && y <= j){
    return segment_tree[v];
  }
  int mid = (x+y)/2;
  return sum(i,j,x,mid,2*v+1,segment_tree,segment_tree_size) +
         sum(i,j,mid+1,y,2*v+2,segment_tree,segment_tree_size);
  
}

//[x,y] is the range represented by node
//v is the node
//creo il segment_tree partendo dal nodo 0 e delegando ricorsivamente la costruzione
//del nodo se e solo se esso non e' un nodo foglia
int build_segment_tree(int* A,int n, int* segment_tree, int segment_tree_size,int x, int y,int v){
  if(x == y){
    segment_tree[v] = A[x];
    return segment_tree[v];
  }
  int mid = (x+y)/2;
  segment_tree[v] = build_segment_tree(A,n,segment_tree,segment_tree_size,x,mid,2*v + 1) + 
    build_segment_tree(A,n,segment_tree,segment_tree_size,mid+1,y,2*v + 2);
  return segment_tree[v];
}

int main( void ){
  int n;
  int segment_tree_size;
  int* A;
  int* segment_tree;
  scanf("%d",&n);
  segment_tree_size = (2 * n) - 1;
  segment_tree = malloc(sizeof(int)* segment_tree_size);
  A = malloc(sizeof(int)* n);
  //input array
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }
  
  //costruzione segment tree ricorsiva
  build_segment_tree(A,n,segment_tree,segment_tree_size,0,n-1,0);

  
  
  
  //debug print
  
  for(int i=0;i<segment_tree_size;i++){
    printf("%d ",segment_tree[i]);
  }
  printf("\n");
  /*
  printf("sum(%d,%d) = %d\n",0,28,sum(0,28,0,n-1,0,segment_tree,segment_tree_size));
  update(13, -1, 0, n-1, 0, segment_tree, segment_tree_size);
  printf("updated sum(%d,%d) = %d\n",0,28,sum(0,28,0,n-1,0,segment_tree,segment_tree_size));
  
  update(4, 1, 0, n-1, 0, segment_tree, segment_tree_size);
  printf("updated sum(%d,%d) = %d\n",2,4,sum(2,4,0,n-1,0,segment_tree,segment_tree_size));
  for(int i=0;i<segment_tree_size;i++){
    printf("%d ",segment_tree[i]);
  }
  printf("\n");
  */
  return 0;
}


