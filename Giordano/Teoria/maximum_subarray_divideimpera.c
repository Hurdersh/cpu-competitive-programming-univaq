/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Problem name: Maximum Subarray
 *  Method: Divide et Impera
 *
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct{
  int left;
  int right;
  int sum;
}indexes;


indexes maximum_center_subarray(int* A, int left, int middle, int right){
  indexes result;
  int left_max = middle;
  int right_max = middle+1;
  int sum_left_max = A[middle];
  int sum_right_max = A[middle+1];
  int sum = 0;
  for(int i = middle;i >= 0; i--){
    sum = sum + A[i];
    if(sum >= sum_left_max){
      sum_left_max = sum;
      left_max = i;
    }
  }
  sum = 0;
  for(int i=middle+1;i<=right;i++){
    sum = sum + A[i];
    if(sum >= sum_right_max){
      sum_right_max = sum;
      right_max = i;
    }
  }
  result = (indexes) {left_max,right_max, sum_left_max + sum_right_max};
  return result;
}

indexes maximum_subarray(int* A, int left, int right){
  //certco il centro
  int middle = (left+right)/2;
  indexes result;
  indexes right_max_subarray;
  indexes left_max_subarray;
  indexes center_max_subarray;
  //caso base, ritorno la lunghezza del singolo subarray(me stesso)  se il subarray e' un singolo blocco
  if(left == right){
    result = (indexes) {left,right,A[left]};
    return result;
  }
  //chiamo le tre subroutines, il massimo subarray o comprende il centro, o sta a sinistra del centro
  //oppure a destra
  left_max_subarray = maximum_subarray(A,left,middle);
  right_max_subarray  = maximum_subarray(A,middle+1,right);
  center_max_subarray = maximum_center_subarray(A,left,middle,right);

  //prendo il massimo
  if(left_max_subarray.sum >= right_max_subarray.sum && left_max_subarray.sum >= center_max_subarray.sum){
    return left_max_subarray;
  }
  if(right_max_subarray.sum >= left_max_subarray.sum && right_max_subarray.sum >= center_max_subarray.sum){
    return right_max_subarray;
  }
  return center_max_subarray; 
}

int main( void ){
  int n;
  int* A;
  indexes result;
  printf("lunghezza array: ");
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    //accedo direttamente al puntatore A+i = &(A[i])
    scanf("%d",A+i);
  }
  //chiamo la routine principale
  result = maximum_subarray(A,0,n-1);
  printf("maximum subarray sum: %d\n",result.sum);
  return 0;
}


