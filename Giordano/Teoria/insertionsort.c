/*
 *  Author: Giordano Colli
 *  Date: 28-08-2022
 *  Language: C
 *  Problem name: Ordering
 *  Algorithm: Insertion
 *  Time Complexity: O(n^2)
 */

#include<stdio.h>
#include<stdlib.h>

void insertion_sort(int* A, int n){
  int up;
  int j;
  for(int i = 1;i<n;i++){
    up = A[i];
    j = i-1;
    while(j >= 0 && up < A[j]){
      A[j+1] = A[j];
      j--;
    }
    A[j+1] = up;
  }
}




int main( void ){
  int n;
  int* array;
  scanf("%d",&n);
  for(int i=0;i<n;i++){
    scanf("%d",array+i);
  }
  insertion_sort(array,n);
  for(int i=0;i<n;i++){
    printf("%d ",*(array + i));
  }

  return 0;
}


