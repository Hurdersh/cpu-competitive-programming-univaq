/*
 *  Author: Giordano Colli
 *  Date: 23-09-2022
 *  Language: C
 *  Problem name: Knapsack
 *  Algorithm: Knapsack DP 
 *  Time Complexity: Theta(n*q)
 */

#include<stdio.h>
#include<stdlib.h>

int max(int a,int b){
  if(a > b)
    return a;
  return b;
}


int* backtracking(int** DP,int* w, int* c, int n, int S){
  /*
   * per verificare se ho selezionato o meno degli elementi
   * utilizzo la solita tecnica di backtracking:
   * parto dalla soluzione del problema in DP[n][S]
   * verifico che esso sia uguale ad DP[n-1][S],in questo caso non l'ho selezionato,
   * quindi analizzo DP[n-1][S], cercando di capire se ho preso quel determinato oggetto.
   *
   * quando trovo un determinato oggetto e questo e' stato selezionato => DP[n][S] = DP[n-1][S - w[n]] + c[n] 
   * allora seleziono l'elemento,ergo devo ripetere la ricerca a partire da DP[n-1][S-w[n]], cioe' controllo
   * l'oggetto n-1 esimo, ma lo zaino ha una dimensione minore
   * 
   * */
  int* selected = malloc(sizeof(int) * n);
  int j = n-1;
  int k = S-1;
  //struttura ad accesso diretto che utilizzo per segnarmi gli oggetti presi,
  //possibile anche un array dinamico per risparmiare "spazio"
  for(int i=0;i<n;i++){
    selected[i] = 0;
  }

  //fino a che non ho analizzato tutti gli oggetti
  while(j > 0){
    //ovviamente l'if mostra il calcolo perche' e' didattico!
    if(DP[j][k] == DP[j-1][k-1]){
      //non selezionato
      selected[j] = 0;
    }
    else if(DP[j][k] == DP[j-1][k-w[j]] + c[j]){
      selected[j] = 1;
      k = k-w[j]; 
    }
    j--;
  }
  //ovviamente quest'ultima verifica non si puo' fare con il primo elemento
  //ma una volta arrivati alla fine del backtracking abbiamo un valore k che rappresenta
  //il valore del peso restante dello zaino, se esso e' abbastanza per inserire il primo oggetto
  //lo inserisco,altrimenti no
  if(w[0] <= k){
    selected[0] = 1;
  }
  return selected;

}

int** knapsack(int* w,int* c,int n,int S){
  //bisogna creare una struttura matriciale
  //grande nxS, cioe' numero di oggetti x Spazio zaino
  //in cui DP[n][S] rappresenta il massimo ottenibile con n oggetti per un peso S
  //ovviamene il caso base e' DP[0][S], per ogni S. in questo caso il massimo
  //ottenibile e' dato dal poter prendere o non poter prendere l'oggetto
  //successivamente bisogna analizzare DP[n][S]. si hanno 2 scelte
  // 1. non prendo l'oggetto n => DP[n][S] = DP[n-1][S]
  // 2. prendo l'oggetto n, ovviamente se prendo l'oggetto n
  //    devo fare spazio nello zaino di tanto quanto l'oggetto mi occupa
  //    e tenere il massimo,aggiungendo il peso 
  //    mio oggetto selezionato => DP[n][S] = DP[n-1][S - w[n]] + c[n] 
  //
  // La soluzione si trovera' nella matrice in basso a destra ovviamente,dove
  // utilizziamo tutti gli oggetti con il massimo peso.
  // se ovviamente S - w[n] e' < 0 => non posso neanche prendere l'oggetto,quindi
  // il problema non si pone
  int** DP;
  DP = malloc(sizeof(int*) * n);
  for(int i=0;i<n;i++){
    DP[i] = malloc(sizeof(int) * S);
  }
  //caso base,riempio la prima riga se l'oggetto e' prendibile
  //i rappresenta il peso
  for(int i=0;i<S;i++){
    if(w[0] <= i){
      DP[0][i] = c[0];
    }
    else{
      DP[0][i] = 0;
    }
  }

  for(int j=1;j<n;j++){
    for(int i=0;i<S;i++){
      // se il peso dell'oggetto e' almeno il peso
      // dello attuale allora valuto di inserirlo,altrimenti il massimale
      // e' proprio il precedente
      if(i - w[j] >= 0){
        DP[j][i] = max(DP[j-1][i],DP[j-1][i-w[j]] + c[j]);
      }
      else{
        DP[j][i] = DP[j-1][i];
      }
    }
  }

  return DP;
}

int main( void ){
  //numero di oggetti
  int n;
  //weight and cost
  int* w;
  int* c;
  //knapsack size
  int S;
  //struttura dati a matrice che contiene il risultato
  int** result;
  int* selected;
  scanf("%d",&n);
  scanf("%d",&S);
  S++; //considero che la colonna 0 sara' inutile nella matrice finale
  w = malloc(sizeof(int) * n);
  c = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    scanf("%d %d",c+i,w+i);
  } 
  result = knapsack(w,c,n,S);
  /*print of a structure*/
  for(int i=0;i<n;i++){
    for(int j=0;j<S;j++){
      printf("%d ",result[i][j]);
    }
    printf("\n");
  }
  printf("\n\n");

  printf("il ladro ruba al massimo un valore di %d\n",result[n-1][S-1]);

  //ricostruisco la soluzione ottima
  selected = backtracking(result,w,c,n,S);

  for(int i=0;i<n;i++){
    if(selected[i]){
      printf("seleziono l'oggetto: %d \n",i+1);
    }
  }
  printf("\n");

  //free of memory
  free(selected);
  for(int i=0;i<n;i++){
    free(result[i]);
  }

  return 0;
}


