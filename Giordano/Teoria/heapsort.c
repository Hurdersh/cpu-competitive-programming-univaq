/*
 *  Author: Giordano Colli
 *  Date: 25-08-2022
 *  Language: C
 *  Problem name: Ordering
 *  Credits: Williams
 *  Algorithm: Heap Sort
 *  Time Complexity: Theta(n*log(n))
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct{
  int n;
  int* array;
} heap;

int max(int a,int b){
  if(a > b)
    return a;
  return b;
}

void fixheap(heap* A, int v){
  //figlio di destra e di sinistra
  int f1 = v*2+1;
  int f2 = v*2+2;
  int temp;
  //verifico se esistono i figli
  if(f1 < (*A).n && f2 < (*A).n){
    //il figlio uno e' il piu' grande
    if((*A).array[f1] > (*A).array[f2]){
      if((*A).array[v] < (*A).array[f1]){
        temp = (*A).array[v];
        (*A).array[v] = (*A).array[f1];
        (*A).array[f1] = temp;
      }
      fixheap(A,f1);
    }
    //il figlio due e' il piu' grande
    else{
      if((*A).array[v] < (*A).array[f2]){
        temp = (*A).array[v];
        (*A).array[v] = (*A).array[f2];
        (*A).array[f2] = temp;
      }
      fixheap(A,f2);
    }
  }
  //esiste solo il figlio di sinistra
  else if (f1 < (*A).n){
      if((*A).array[v] < (*A).array[f1]){
        temp = (*A).array[v];
        (*A).array[v] = (*A).array[f1];
        (*A).array[f1] = temp;
      }
      fixheap(A,f1);
  }
}
void heapify(heap* A){
  for(int i=((*A).n)/2;i>=0;i--){
    fixheap(A,i);
  }
}


int maxRetrieve(heap* A){
  int maxe = (*A).array[0];
  //radice = ultima foglia
  (*A).array[0] = (*A).array[(*A).n-1];
  //ultima foglia uguale max element
  (*A).array[(*A).n-1] = maxe;
  //dopo aver diminuito la dimensione dell'heap

  (*A).n = ((*A).n)-1;
  //fixheap sulla radice per mantere la proprieta' di ordinamento parziale
  fixheap(A,0);
}

void heapsort(heap* A){
  //questo n cambiera' per ridurre la dimensione dell'heap
  int n = (*A).n;
  heapify(A);
  //per l'ultimo maxRetrieve non posso scambiare con l'ultima foglia
  //e quindi lascio in A.array[0] l'unico elemento rimasto
  //(nu elemento da solo e' sempre ordinato)
  for(int i=0;i<n-1;i++){
    maxRetrieve(A);
  }
}

int main( void ){
  heap* A;
  int n;
  A = malloc(sizeof(heap));
  scanf("%d",&((*A).n));
  n = (*A).n;
  (*A).array = malloc(sizeof(int) * (*A).n);
  for(int i=0;i<(*A).n;i++){
    scanf("%d",(*A).array+i);
  }
  heapsort(A);
  for(int i=0;i<n;i++){
    printf("%d ",(*A).array[i]);
  }
  printf("\n");

  return 0;
}


