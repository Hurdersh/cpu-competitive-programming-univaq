/*
 *  Author: Giordano Colli
 *  Date: 25-08-2022
 *  Language: C
 *  Problem name: Ordering
 *  Credits: Dijkstra
 *  Algorithm: Integer Sort applied to strings
 *  Time Complexity: Theta(n)
 */

#include<stdio.h>
#include<stdlib.h>


int main( void ){
  int n;
  char* array;
  char* result;
  scanf("%d",&n);
  array = malloc(sizeof(char)*n);
  for(int i=0;i<n;i++){
    *(array+i) = getchar();
  }
  result = malloc(sizeof(char) * 'z');
  for(int i=0;i<'z';i++){
    result[i] = 0;
  }
  for(int i=0;i<n;i++){
    result[array[i]]++;
  }
  for(int i=0;i<'z';i++){
    if(result[i] > 0){
      for(int j=0;j<result[i];j++){
        printf("%c", (char)i);
      }
    }
  }
  printf("\n");
  free(array);
  free(result);
  return 0;
}


