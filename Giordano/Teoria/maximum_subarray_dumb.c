/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Problem name: Maximum Subarray
 *  Method: Dumb
 *  Complexity: O(n^2)
 *
 */

#include<stdio.h>
#include<stdlib.h>


int main( void ){
  int* A;
  int sum = 0;
  int n;
  int bestsum;
  printf("lunghezza array: ");
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    //accedo direttamente al puntatore A+i = &(A[i])
    scanf("%d",A+i);
  }
  bestsum = A[0];
  //l'idea stupida e' scorrere tutti i subarray in modo progressivo
  //scorro tutte le possibili dimensioni di subarray, da 1 ad n
  for(int i=1;i<=n;i++){
    sum = 0;
    //calcolo il primo subarray di dimensione esattamente i
    for(int j=0;j<i;j++){
      sum = sum + A[j]; 
    }
    //verifico se la somma e' maggiore della bestsum
    if(sum > bestsum){
      bestsum = sum;
    }
    //vado avanti per quanti subarray rimangono(ovviamente n-i)
    //ad esempio tutti i sottoarray da 2 in un array da 5 sono (5-2)+1 ma uno l'ho gia' considerato
    //quindi semplicemente 5-2 cioe' n-i
    for(int j = 0;j<n-i;j++){
      //aggiorno la somma con un elemento in avanti e rimuovendo un elemento all'indietro
      sum = sum-A[j];
      sum = sum+A[j+i];
      if(sum > bestsum){
        bestsum = sum;
      }
    }
  }
  printf("maximum sum = %d\n",bestsum);
  return 0;
}


