/*
 *  Author: Giordano Colli
 *  Date: 15-09-2022
 *  Language: C
 *  Problem name: Integer Positive Power 
 *  Algorithm Divide et impera
 *  Time Complexity: Theta(n),Theta(log(n))
 */

#include<stdio.h>
#include<stdlib.h>

//naive running in Theta(n)
int power_naive(int a,int n){
  int temp = a;
  for(int i=0;i<n-1;i++){
    temp = temp * a;
  }
  return temp;
}

//divide et impera running in Theta(log(n))
int power(int a,int n){
  int temp;
  if(n == 0)
    return 1;
  if(n == 1)
    return a;
  temp = power(a,n/2);
  if(n % 2 == 0){
    return temp * temp;
  }
  return temp*temp*a;
}
int main( void ){
  int a,n,result;
  printf("inserisci la base : \n");
  scanf("%d",&a);
  printf("inserisci l'esponente : \n");
  scanf("%d",&n);
  if(a >= 0 && n >= 0 && !(a == 0 && n == 0) ){
    result = power(a,n);
    printf("%d\n",result);
    return 0; 
  }
  printf("sia la base che l'esponente devono essere NON negativi e diversi da (0,0)\n");
  return 1;
}


