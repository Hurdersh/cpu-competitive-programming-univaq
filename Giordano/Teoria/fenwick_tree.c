/*
 *  Author: Giordano Colli
 *  Date: 15-09-2022
 *  Language: C
 *  Problem name: Fenwick Tree
 *  Time Complexity: Creation: O(n), Query: O(log(n))
 */

#include<stdio.h>
#include<stdlib.h>


//la magic function, in realta' quest'operazione bitwise
//permette di trovare lo zero piu' a destra e sostituirlo
//con tutti zeri -> provare per credere
int p(int i){
  return (i & (i+1));
}

//faccio update di tutti i segmenti che contengono i cioe'
//quelli tali per cui j >= i >= p(j) e data la funzione magica
//si ottengono tenendo lo stesso prefisso per le i
//ed andando ad aggiungere uni progressivamente, ovviamente 
//gli update sono in numero al piu' quanti sono i bit utilizzati
//per rappresentare il numero => log(n)
void update(int i,int v,int* fenwick_tree, int* A,int n){
  A[i] += v;
  int j = i;
  while(j < n){
    fenwick_tree[j] += v;
    j = (j | (j+1));
  }
}

//somma da 0 al valore i
//cerco tutti i segmenti "a restroso", banalmente se ho memorizzato 
// in fenwick_tree[i] la somma di A[i] ed A[p(i)] basta che io vada 
// a p(i)-1, ovviamente con la funzione magica ogni volta "rimpiccioliamo"
// il numero binario azzerando il primo dei bit piu' a destra => tolgo almeno un bit a step
// tanti step quanti i bit di n => log(n)
int sum(int i,int* fenwick_tree, int n){
  int res = 0;
  int j;
  j = i;
  while(j >= 0){
    res += fenwick_tree[j];
    j = p(j) - 1;
  }
  return res;
}


//sum(i,j) = sum(0,i) - sum(0,j-1)
//mi baso semplicemente sullo stesso principio sotto le prefix sums
//pongo come ipotesi molto forte che n > j >= i >= 0
int sum_segment(int i, int j, int* fenwick_tree, int n){
  if(i > 0){
    return sum(j,fenwick_tree,n) - sum(i-1,fenwick_tree,n);
  }
  return sum(i,fenwick_tree,n); 
}


//banali inizializzazione della struttura dati
//inserisco in fenwick_tree la somma da A[i] ad A[p(i)-1]
int* initialize_fenwick_tree(int* partial_sums, int n){
 int* fenwick_tree = malloc(sizeof(int) * n);
 for(int i=0;i<n;i++){
   if(p(i) > 0){
    fenwick_tree[i] = partial_sums[i] - partial_sums[p(i)-1];
   }
   else{
    fenwick_tree[i] = partial_sums[i];
   }
 } 
 return fenwick_tree;
}

int main( void ){
  int n;
  int* A;
  int* partial_sums;
  int* fenwick_tree;
  int i;

  //prendo in input l'array di partenza con relativa dimensione
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(i=0;i<n;i++){
    scanf("%d",A+i);
  }

  //creo le somme parziali che mi serviranno ad inizializzare il
  //fenwick tree
  partial_sums = malloc(sizeof(int) * n);
  partial_sums[0] = A[0];
  for(int i=1;i<n;i++){
    partial_sums[i] = partial_sums[i-1] + A[i];
  }
  //inizializzo la struttura dati
  fenwick_tree = initialize_fenwick_tree(partial_sums,n);
  free(partial_sums);
  //chiamate di esempio
  /*printf("%d\n",sum_segment(3,7,fenwick_tree,n));
  update(4,1,fenwick_tree,A,n);
  printf("%d\n",sum_segment(3,7,fenwick_tree,n));*/
  free(fenwick_tree);
  return 0;
}


