/*
 *  Author: Giordano Colli
 *  Date: 03-12-2022
 *  Language: C++
 *  Problem name: Edit Distance
 *  Method: Dynamic Programming
 *  Credits: Levenshtein
 *
 */

#include<iostream>
#include<string>
#include<stdlib.h>

using namespace std;

int min(int a,int b){
  if(a < b)
    return a;
  return b;
}

int main( void ){
  string s1,s2;
  cin >> s1 >> s2;
  int m[s1.size()+1][s2.size()+1];

  //casi base
  for(int i=0;i<s1.size()+1;i++){
    m[i][0] = i;
  }
  for(int i=0;i<s2.size()+1;i++){
    m[0][i] = i;
  }
  

  
  for(int i=1;i<s1.size()+1;i++){
    for(int j=1;j<s2.size()+1;j++){
      if(s1[i-1] == s2[j-1]){
        //stesso carattere no operations
        m[i][j] = m[i-1][j-1];
      }
      else{
        //dp che prova tutti i casi nella optimal substructure
        m[i][j] = 1 + min(m[i-1][j-1],min(m[i-1][j],m[i][j-1]));
      }
    }
  } 

  //print matrice
  for(int i=0;i<s1.size()+1;i++){
    for(int j=0;j<s2.size()+1;j++){
      cout << m[i][j] << " ";
    }
    cout << endl;
  }
  cout << endl;
  return 0;
}
