/*
 *  Author: Giordano Colli
 *  Date: 27-07-2022
 *  Language: C
 *  Problem name: 2-Sat
 *  Method: Condensation of Strongly Connected Components
 *  Credits: Kosaraju/Sharir
 *
 */

#include<stdio.h>
#include<stdlib.h>


typedef struct {
  int first;
  int second;
} clause;


//per rendere piu' comprensibile il codice creo una struttura dati vector
//con un paio di funzioni che operano su di essa
typedef struct {
  int* arr;
  int size; //elementi effettivamente inseriti
  int dim; //dimensione fisica del vector
} vector;

//struttura dato grafo
typedef struct{
  vector* adj;
  vector* inv;
} graph;
graph G;

//vettore di componenti fortemente connesse
//ad ogni elemento assegno a quale componente connessa fa parte
//strongly connected components
int* scc;
//array di nodi gia' visitati dalla dfs
int* visited;
//numero di componenti connesse trovate
int scc_number = 0;

//inizializza il vettore v
void vector_init(vector* v){
  (*v).arr = malloc(sizeof(int));
  (*v).size = 0;
  (*v).dim = 1;
}

//aggiunge un elemento al vettore
void vector_add(vector* v,int e){
  (*v).size++;
  //utilizzo il metodo dimezza/raddoppia per ammortizzare il costo computazionale 
  //legato all'estendere l'array, sprecando pero' un certo quantitativo di memoria
  if((*v).size > (*v).dim){
    (*v).dim = 2 * (*v).dim;
    (*v).arr = realloc((*v).arr,sizeof(int) * (*v).dim);
  }
  (*v).arr[(*v).size-1] = e;
}

//utilizzo questa funzione per chiarezza,sarebbe stato tutto troppo incasinato senza di questa
void add_edge(graph* G, int a, int b){
  vector_add(&(*G).adj[a], b);
}
//aggiungo separatamente gli archi reversed
void add_reverse_edge(graph* G,int a, int b){
  vector_add(&(*G).inv[a], b);
}

int not(int a){
  if(a < 0){
    return ((a * -1)-1)*2;
  }
  return ((a-1)*2)+1;
}
 
int loc(int a){
  if(a < 0){
    return (((-1*a)-1)*2)+1;
  }
  return (a-1)*2;
}

void create_graph(clause* A, int n,int m){
  //ci sono esattamente n * 2 nodi
  //mantengo anche il grafo di adiacenza inverso
  G.adj = malloc(sizeof(vector) * m * 2);
  G.inv = malloc(sizeof(vector) * m * 2);
  //inizializzazione dei vettori di adiacenza
  for(int i=0;i<m * 2; i++){
    vector_init(&(G.adj[i]));
    vector_init(&(G.inv[i]));
  }
  //creazione degli archi
  for(int i=0;i<n;i++){
    // A[i] e' una cosa del tipo (a v b), genera gli archi not(a) -> b e not(b) -> a
    // ovviamente ad ogni nodo devo assegnare una posizione e ordino l'array in questo modo
    // a | not(a) | b | not(b) etc
    // quindi per ogni generico a 
    // se a e' positivo => se stesso e' (a-1)*2 
    // se a e' positivo => il suo negato e' (a-1)*2+1
    // se a e' negativo => se stesso e' ((abs(a)-1) * 2)+1
    // se a e' negativo => il suo negato ((abs(a)-1) * 2)   Es: neg(-1) = 1-1 = 0 
    // rimando questo tediosissimo compito di traduzione alla funzione not e loc

    //aggiungo not(a) -> b
    add_edge(&G,not(A[i].first),loc(A[i].second));
    //aggiungo not(b) -> a
    add_edge(&G,not(A[i].second),loc(A[i].first));
    //mantengo l'gli archi reversed che mi serviranno
    add_reverse_edge(&G,loc(A[i].first),not(A[i].second));
    add_reverse_edge(&G,loc(A[i].second),not(A[i].first));
  }
  
  //stampa debug
  /*
  printf("grafo iniziale: \n");
  for(int i=0;i<2*n;i++){
    for(int j=0;j<G.adj[i].size;j++){
      printf("%d -> %d\n", i , G.adj[i].arr[j]);
    }
  }
  printf("grafo reversed\n");
  for(int i=0;i<2*n;i++){
    for(int j=0;j<G.inv[i].size;j++){
      printf("%d -> %d\n", i , G.inv[i].arr[j]);
    }
  }
  */
}
vector p;

void dfs(int v){
  visited[v] = 1;
  for(int i=0;i<G.adj[v].size;i++){
    if(!visited[G.adj[v].arr[i]]){
      dfs(G.adj[v].arr[i]);
    }
  }
  //si puo' allocare solo una volta, ma abbiamo creato vector
  //per semplificarci la vita
  vector_add(&p,v);
}

void dfs_reverse(int v,int counter){
  visited[v] = 1;
  //printf("visito %d\n",v);
  for(int i=0;i<G.inv[v].size;i++){
    //printf("guardo %d\n", G.inv[v].arr[i]);
    if(!visited[G.inv[v].arr[i]]){
      //printf("visito %d\n", G.inv[v].arr[i]);
      dfs_reverse(G.inv[v].arr[i],counter);
    }
  }
  //si puo' allocare solo una volta, ma abbiamo creato vector
  //per semplificarci la vita
  scc[v] = counter;
}

void generate_scc(int m){
  /*
   * svolgo una dfs classica per trovare gli elementi connessi
   * che sono "punto di ingresso" delle componenti fortemente connesse
   * piu' a sinistra possibile
  */
  int scc_counter = 0;
  vector_init(&p);
  int temp;
  visited = malloc(sizeof(int) * m);
  for(int i=0;i<m;i++){
    visited[i] = 0; 
  }
  for(int i=0;i<m;i++){
    if(!visited[i]){
      dfs(i);
    }
  }
  //stampa debug
  //for(int i=0;i<p.size;i++){
  //  printf("%d ",p.arr[i]);
  //}
  //printf("\n");
  
  //inverto l'array p per trovare le componenti piu' a sinistra
  //questa operazione non e' necessaria, basterebbe scorrere da destra a sinista
  //MA E' IMPORTANTE DIDATTICAMENTE, il motivo per cui swappo l'array e' che voglio i punti
  //di ingresso piu' a sinistra delle componenti fortemente connesse
  for(int i=0;i<p.size/2;i++){
    temp = p.arr[p.size-i-1];
    p.arr[p.size-i-1] = p.arr[i];
    p.arr[i] = temp;
  }
  //stampa debug
  //for(int i=0;i<p.size;i++){
  //  printf("%d ",p.arr[i]);
  //}
  //printf("\n");
  
  //reinizializzo le visite
  for(int i=0;i<m;i++){
    visited[i] = 0; 
  }

  //dopo aver trovato i punti di ingresso rigirando gli archi rimangono tutti connessi
  //tranne i componenti di altre componenti piu' a destra
  //A -> B diventa A <- B  e siccome i punti di a in ingresso sono il piu' a destra possibile
  //non ottengo altro che A 
  for(int i=0;i<m;i++){
    if(!visited[p.arr[i]]){
      dfs_reverse(p.arr[i],scc_counter);
      scc_counter++;
    }
  }
  scc_number = scc_counter; 
  //debug print
  //for(int i=0;i<m;i++){
  //  printf("il nodo %d appartiene alla scc %d \n", i, scc[i]);
  //}
}

int is2sat(int m){
  //verifico che una scc contenga o x o -x, in caso contrario si giungerebbe
  //ad una ovvia contraddizione ed il problema non sarebbe risolubile
  int i = 0;
  while(i < m && scc[i] != scc[i+1]){
      i = i + 2;
  } 
  if(i >= m){
    return 1;
  }
  return 0;
}

int main( void ){
  //prendiamo in input le clausole, ognuna di esse formata da 2 input
  //sono interi, se negativi valori negati
  //Esempio: (a v b) && (b v not(c)) = 1 2 2 -3
  //not c e' not 3
  int n;
  int m;
  clause* A;

  //printf("numero clausole:\n");
  scanf("%d",&n);
  A = malloc(sizeof(clause) * n);
  //printf("numero variabili:\n");
  scanf("%d",&m);
  for(int i=0;i<n;i++){
    scanf("%d",&((*(A+i)).first) );
    scanf("%d",&((*(A+i)).second) );
  }

  /*
   * per generare gli archi considero la clausola generica (a v b)
   * gli archi saranno not(a) -> b e not(b) -> a
   * passando per la definizione implicativa, o osservando informalmente 
   * che per essere verificata la formula se a e' "spento" => b deve essere "acceso"
   * inoltre decido di tenere 2 liste di adiacenza, una normale ed una inversa
   */
  create_graph(A,n,m);

  /*
   *  Utilizzo questo metodo per generare le componenti fortemente connesse
   *
   */
  scc = malloc(sizeof(int) * 2 * m);
  generate_scc(2 * m);

  // verifico in maniera banale se 2 componenti negate 
  // sono presenti nella stessa componente connessa 
  if( is2sat(2 * m) ){
    printf("il problema e' risolubile\n");
    //se il problema e' risolubile il numero di componenti connesse e' pari
    //grazie al LEMMA della simmetria, non e' semplice pero' ricavare il numero di soluzioni
  }
  else{
    printf("il problema non e' risolubile\n");
  }
  free(A);
  //chiamo la routine principale
  return 0;
}
