#include<stdio.h>
#include<stdlib.h>


int binary_search(int* A,int n,int k){
  for(int i=0;i<n;i++){
    if(A[i] == k){
      return i;
    }
  }
  return -1;
}

int main( void ){
  int* A;
  //k is the searched number
  int k;
  int n;
  int result;

  printf("n: \n");
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }
  printf("k: \n");
  scanf("%d", &k); 
  result = binary_search(A,n,k);
  if(result == -1){
    printf("errore,elemento non trovato\n");
  }
  else{
    printf("il valore %d si trova in posizione %d\n",k,result+1);
  }
}
