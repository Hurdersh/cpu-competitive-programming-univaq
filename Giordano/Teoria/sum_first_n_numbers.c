/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sum First n numbers
 *  Method: Dynamic Programming
 *
 */

#include<stdio.h>

int sum_first_n_linear(int n){
  int sum = 0;
  for(int i=1;i<=n;i++){
    sum = sum + i;
  }
  return sum;
}

int sum_first_n_oneshot(int n){
  return (n*(n+1))/2;
}

int main( void ){
  int n = 0;
  printf("insert the number n: ");
  scanf("%d",&n);
  if(n < 0){
    return 255;
  }
  printf("sum of first n numbers: %d\n",sum_first_n_linear(n));
  printf("sum of first n numbers: %d\n",sum_first_n_oneshot(n));
  return 0;
}


