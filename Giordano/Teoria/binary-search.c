#include<stdio.h>
#include<stdlib.h>

int binary_search_it(int* A,int n,int k){
  int left = 0;
  int right = n-1;
  int medium = (left+right)/2;
  while(left <= right && A[medium] != k){
    if(A[medium] > k){
      right = medium-1; 
    }
    else{
      left = medium+1; 
    }
    medium = (left+right)/2;
  }
  if(A[medium] == k){
    return medium;
  }
  return -1;
}

int binary_search(int* A,int n,int k,int left,int right){
  int index = (left+right)/2;
  if(left > right){
    return -1;
  }
  //trovo l'elemento
  if(A[index] == k){
    return index;
  }
  //sinistra
  if(A[index] > k){
    return binary_search(A,n,k,left,index-1);
  }
  //destra
  if(A[index] < k){
    return binary_search(A,n,k,index+1,right);
  }
}

int main( void ){
  int* A;
  //k is the searched number
  int k;
  int n;
  int result;

  printf("n: \n");
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }
  printf("k: \n");
  scanf("%d", &k); 
  result = binary_search_it(A,n,k);
  if(result == -1){
    printf("errore,elemento non trovato\n");
  }
  else{
    printf("il valore %d si trova in posizione %d\n",k,result+1);
  }
}
