/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Problem name: Maximum Subarray
 *  Method: Dynamic Programming
 *  Credits: Kadane's Algorithm
 *
 */

#include<stdio.h>
#include<stdlib.h>

int max(int a, int b){
  if(a >= b){
    return a;
  }
  return b;
}

int maximum_subarray(int* A,int n){
  //questo vettore contiene il massimo 
  int* maximum_ends_at;
  int result;
  maximum_ends_at = malloc(sizeof(int) * n);
  maximum_ends_at[0] = A[0]; //ovviamente 
  result = maximum_ends_at[0];
  for(int i=1;i<n;i++){
    maximum_ends_at[i] = max(A[i], A[i]+maximum_ends_at[i-1]);
    if(maximum_ends_at[i] > result){
      result = maximum_ends_at[i];
    }
  }
  return result;
}

int main( void ){
  int n;
  int* A;
  //printf("lunghezza array: ");
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  for(int i=0;i<n;i++){
    //accedo direttamente al puntatore A+i = &(A[i])
    scanf("%d",A+i);
  }
  //chiamo la routine principale
  //printf("maximum subarray sum: %d\n",maximum_subarray(A,n));
  printf("%d\n",maximum_subarray(A,n));
  return 0;
}


