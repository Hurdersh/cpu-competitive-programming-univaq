/*
 *  Author: Giordano Colli
 *  Date: 10-04-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1000 (B)
 *  Algorithm Or Idea used: Greedy
 *  Credits: Me
 *  Problem name: Count the Number of Pairs
 *  Link Problem: https://codeforces.com/contest/1800/problem/B
 *  Time Complexity: O(n)
 *
 */
#include<iostream>
#include<string>
using namespace std;

/* 
 *  let fix a letter, 'a'.
 *  #a = number of lowercase a readed
 *  #A = numeber of uppercase A readed
 *  how many couples we have currently? min(#a,#A)
 *  #pairs = min(#x,#X) for all x 
 *  can we increment numbers of couple? Yes, WLOG adding one to #a and subtracting one to #A
 *  how much times we can do this +1 -1 operation ? when #a and #a becomes balanced =>(#A + #a)/2
 *  the median
 *  let's note that increment of letter x doesn't affect other letters.
 *  increment of a letter is what we can obtain minus what we have = (#A + #a)/2 - min(#A,#a)
 *  let's calculate the increments and if sum of increments is < k we can do #pairs + increment
 *  else we can do #pairs + k
 *
 *
 */

void solve(){
  int n;
  int k;
  string s;
  cin >> n;
  cin >> k;
  cin >> s;

  /*array map*/
  int lower_char[26];
  int upper_char[26];
  int increment = 0;
  int sum = 0;
  for(int i=0;i<26;i++){
    lower_char[i] = upper_char[i] = 0;
  }

  for(int i=0;i<n;i++){
    if(s[i] - 'a' >= 0){
      //lowercase
      lower_char[s[i] - 'a']++;
    }
    else{
      upper_char[s[i] - 'A']++;
    }
  }

  for(int i=0;i<26;i++){
    sum += min(lower_char[i],upper_char[i]);
    increment += (lower_char[i] + upper_char[i])/2 - min(lower_char[i],upper_char[i]);
  }
  if(increment < k){
    cout << sum + increment << endl;
  }
  else{
    cout << sum + k << endl;
  }
  
  /*for(int i=0;i<26;i++){
    cout << upper_char[i] << " ";
  } cout << endl;
  for(int i=0;i<26;i++){
    cout << lower_char[i] << " ";
  } cout << endl;*/
  

}


int main(void){
  int t;
  cin >> t;
  while(t--){
    solve();
  }
  return 0;
}
