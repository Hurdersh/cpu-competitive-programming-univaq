#include<iostream>
#define MAXN 200000
using namespace std;

int a[MAXN];
void solve(){
  int n;
  int zero = 0;
  int one = 0;
  int greater_one = 0;
  cin >> n;
  for(int i=0;i<n;i++){
    cin >> a[i];
    if(a[i] == 0){
      zero++;
    }
    else if(a[i] == 1){
      one++;
    }
    else if(a[i] > 1){
      greater_one++;
    }
  }
  //edge case
  if(zero == n){
    cout << "1" << endl;
    return;
  }
  if(zero > one+greater_one+1){
    if(greater_one > 0){
      cout << "1" << endl;
    }
    else{
      cout << "2" << endl;
    }
  }
  else{
    //zero <= one+greater_one+1
    cout << "0" << endl;
  }

  
}

int main(void){
  int t;
  cin >> t;
  while(t--){
    solve();
  }
  return 0;
}
