/*
 *  Author: Giordano Colli
 *  Date: 08-04-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: B
 *  Algorithm Or Idea used: Greedy
 *  Credits: 
 *  Problem name: The String Has a Target
 *  Link Problem: https://codeforces.com/contest/1805/problem/B
 *  Time Complexity: O(n)
 *
 */

#include<iostream>
#include<string>
#define ll long long
using namespace std;

/*
 * Main idea is easy, choice the rightmost lexicographically lower character
 * and push it in first position
 * Proof:
 * if there are only one lexicographically lower character is easy to see the best choice
 * if there are 2 or more lexicographically lower characters(we denote a)
 *
 *      original string =  xxxaxxxxxaxxxxxx
 *      picking first a =  axxxxxxxxaxxxxxx
 *      picking second a = axxxaxxxxxxxxxxx
 * of course picking the second a the distance from start to the first a is less than distance from
 * the start and the second a => picking rightmost a is the best greedy choice 
 */

void solve(){
  ll n;
  string s;
  ll posbest = 0;
  ll bestchar;
  cin >> n;
  cin >> s;
  for(ll i=0;i<n;i++){
    if(s[i] <= s[posbest]){
      posbest = i;
    }
  }
  string k(1,s[posbest]);
  s.erase(posbest,1);
  s = k + s;
  cout << s << endl;
}

int main(void){
  ll t;
  cin >> t;
  while(t--){
    solve();
  }
  return 0;
}
