/*
 *  Author: Giordano Colli
 *  Date: 08-04-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: A
 *  Algorithm Or Idea used: Greedy
 *  Credits: Me,Myself and I
 *  Problem name: We Need The Zero
 *  Link Problem: https://codeforces.com/contest/1805/problem/A
 *  Time Complexity: O(n)
 */

#include<iostream>
#define ll long long
using namespace std;

/*
 * Main idea is simple, using xor properties
 * if the array has an even length the operation is =>
 *      b1 xor b2 xor b3 xor b4 ....  xor bn =
 *      (a1 xor x) xor (a2 xor x) xor (a3 xor x) xor (a4 xor x)  ..... xor (an xor x) =
 *      using associativity and commutativity
 *      (a1 xor a2 xor a3 xor a4 .... xor an) xor (x xor x xor x ... xor x)
 *      there are n 'x',  (x xor x) xor (x xor x) xor (x xor x) where (x xor x) = 0 
 *      all the chain must be 0
 *      (a1 xor a2 xor a3 xor a4 .... xor an) xor 0 =
 *      a1 xor a2 xor a3 xor a4 .... xor an 
 *
 *      ok, it is possible to find an x iff the xor chain is = 0
 *      and no means what x we pick.
 * if the array has an odd length the reasoning is the same but we obtain
 *      a1 xor a2 xor a3 xor a4 .... xor an xor x
 *      with an x to the end. It is possible to find an x iff the xor chain of a is equal to x
 *      we are going to choose exactly this x
 *
 *
 * */

void solve(){
  ll n;
  ll temp;
  ll xored = 0;
  cin >> n;
  for(ll i=0;i<n;i++){
    cin >> temp;
    xored ^= temp;
  }
  if(n%2 == 0){
    if(xored == 0)
      cout << "0" << endl;
    else
      cout << "-1" << endl;
  }
  else{
    cout << xored << endl;
  }
}

int main(void){
  ll t;
  cin >> t;
  while(t--){
    solve();
  }
  return 0;
}
