/*
 *  Author: Giordano Colli
 *  Date: 09-04-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 800 (A)
 *  Algorithm Or Idea used: Regex
 *  Credits: Me
 *  Problem name: Is It a Cat?
 *  Link Problem: https://codeforces.com/contest/1800/problem/A
 *  Time Complexity: O(n)
 *
*/
#include<iostream>
#include<string>
#define ll long long
using namespace std;

/*
 *The idea is to check if the string respect the following regex mm*ee*oo*ww*
 *to do this we're using a finite state automa with following states
 * initial [0] , m [1], e [2], o[3], w[4]
 * if i am in the initial state 0 i can read only the m to do ahead to the next state
 * if i read m (state 1) can read a number of m remaining in the same state
 * if i read in state 1 a letter that isn't 'e' the string don't match
 * same for all other states
 */

void solve(){
  ll state = 0;
  ll n;
  string s;
  cin >> n;
  cin >> s;
  for(ll i=0;i<n;i++){
    s[i] = tolower(s[i]);
    if(s[i] == 'm' && state == 0){
      state++;
    }
    else if(s[i] == 'm' && state == 1){
      state = state;
    }
    else if(s[i] == 'e' && state == 1){
      state++;
    }
    else if(s[i] == 'e' && state == 2){
      state = state;
    }
    else if(s[i] == 'o' && state == 2){
      state++;
    }
    else if(s[i] == 'o' && state == 3){
      state = state;
    }
    else if(s[i] == 'w' && state == 3){
      state++;
    }
    else if(s[i] == 'w' && state == 4){
      state = state;
    }
    else{
      cout << "NO" << endl;
      return;
    }
  }
  if(state == 4){
    cout << "YES" << endl;
  }
  /*else{
    cout << "NO" << endl;
  }*/
}


int main(void){
  ll t;
  cin >> t;
  while(t--){
      solve();
  }
}
