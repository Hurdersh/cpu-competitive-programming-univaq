#include<stdio.h>
#include<stdlib.h>


typedef struct{
  int a;
  struct llnode* next;
} llnode;

typedef struct{
  llnode* head;
} LinkedList;




void initialize(LinkedList* ll){
  (*ll).head = NULL;
}


void insertHead(LinkedList* ll,int element){
  llnode node;
  node.a = element;
  node.next = NULL;
  if((*ll).head){
    (*((*ll).head)).next = &node;
  }
  else{
    (*ll).head = &node;
  }
}


/*
 * typedef struct{
  int* array; 
  int size; //la quantita' di memoria allocata
  int dimension; //la quantita' di memoria utilizzata
} vector; 

void initialize(vector* v){
  (*v).array = malloc(sizeof(int));
  (*v).size = 1;
  (*v).dimension = 0;
}

void insert(vector* v, int elemento){
  if((*v).dimension+1 > (*v).size){
    (*v).size = 2 * (*v).size;
    (*v).array = realloc((*v).array,sizeof(int)*(*v).size);
  }
  (*v).array[(*v).dimension] = elemento;
  (*v).dimension++;
}

int remove_vector(vector* v,int location){
  if(location < 0 || location >= (*v).dimension)
    return -1;
  for(int i = location; i<(*v).dimension-1; i++){
    (*v).array[i] = (*v).array[i+1];
  }
  (*v).dimension--;
  if((*v).dimension*2 <= (*v).size){
    (*v).size/=2;
    (*v).array = realloc((*v).array,sizeof(int)*(*v).size);
  }
}

void print(vector* v){
  int dimension = (*v).dimension;
  for(int i=0;i<dimension;i++){
    printf("%d ",*((*v).array+i));
  }
  printf("\n");
}
*/





int main(void){
  LinkedList ll;
  initialize(&ll);
  insertHead(&ll,3);
  printf("%d\n",(*(ll.head)).a);
  return 0;
}


