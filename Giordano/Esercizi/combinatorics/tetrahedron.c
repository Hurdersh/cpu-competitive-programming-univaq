/*
 *  Author: Giordano Colli
 *  Date: 03-01-2023
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Linear Algebra
 *  Credits: Thank You Giuseppe Pipoli
 *  Problem name: Tetrahedron
 *  Link Problem: https://codeforces.com/problemset/problem/166/E
 *  Time Complexity: O(logn)
 *
 */

#include<stdio.h>
#include<stdlib.h>
#define mod 1000000007

long long** matrix;
 
long long** matrix_multiplication(long long** m1, long long** m2){
   long long** result;
   result = malloc(sizeof(long long*) * 4);
  //riga
  for(int i=0;i<4;i++){
    result[i] = malloc(sizeof(long long) * 4);
    //colonna
    for(int j=0;j<4;j++){
      result[i][j] = 0;
      for(int k=0;k<4;k++){
        result[i][j] = (result[i][j]%mod + (((m1[i][k]%mod) * (m2[k][j]%mod))%mod) )%mod;
      }
    }
  }
  return result;
}

long long** potenzamia(long long** b, long long e){
  long long** temp;
  temp = malloc(sizeof(long long*) * 4);
  for(int i=0;i<4;i++){
    temp[i] = malloc(sizeof(long long) * 4);
  }
  if(e == 1){
    return b;
  }
  if(e%2==0){
    temp = potenzamia(b,e/2);
    temp = matrix_multiplication(temp,temp);
    return temp;
  }
  else{
    temp = potenzamia(b,e/2);
    temp = matrix_multiplication(temp,temp);
    temp = matrix_multiplication(temp,b);
    return temp;
  }
}

int main( void ){
  /*la matrice di M=adiacenza(nodo-nodo) e' una matrice con tutti uni dato che
   * e' una clique a 4, rappresenta quanti path posso ottenere per passare da un nodo ad un altro
   * passando per 1 arco. M*M rappresenta quanti path posso ottenere per passare da un nodo ad un altro
   * passando per 2 archi. M^n rappresenta quanti path posso ottenere per passare da un nodo ad un altro
   * passando per n archi. 
   *
   * quindi trovo la matrice e poi trovo il numero di path in matrix[D,D] e lo faccio in tempo logaritmico
   * 
   */
  /*
   * Se riesco a diagonalizzare M
   * M = PDP^-1 dove P e' invertibile e D e' diagonale
   * M^n = (PDP^-1)^M = PDP^-1PDP^-1P..........P-1 = PD(P^-1P)D(P^-1P)..........P-1
   * = PD^nP^-1
   *
   * ma D^n essendo diagonale e' semplicemente D con la  diagonale elevata alla n
   * => moltiplico le e matrici fisse ed ottengo una formula chiusa
   * (3^n + 3*(-1)^n) / 4 
   * che richiede comunque tempo logaritmico per calcolare 3^n :)
   * pero' non posso usare questa dato il /4 per il modulo 10^9 + 7
   *
   * */
  int n;
  matrix = malloc(sizeof(long long*) * 4);
  for(int i=0;i<4;i++){
    matrix[i] = malloc(sizeof(long long) * 4);
    for(int j=0;j<4;j++){
      if(i==j){
        matrix[i][j] = 0;
      }
      else{
        matrix[i][j] = 1;
      }
    }
  }
  scanf("%d",&n);
  if(n == 1){
    printf("%d\n",0);
  }
  else if(n == 2){
    printf("%d\n",3);
  }
  else{
   /*for(int i=0;i<4;i++){
     for(int j=0;j<4;j++){
        printf("%lld ", gag[i][j]);
     }
     printf("\n");
   }
  */
  printf("%lld",potenzamia(matrix,n)[3][3]);
  }
  return 0;
}
