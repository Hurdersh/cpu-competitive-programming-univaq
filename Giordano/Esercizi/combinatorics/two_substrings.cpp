/*
 *  Author: Giordano Colli
 *  Date: 01-01-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Observation
 *  Credits:
 *  Problem name: Two Substrings
 *  Link Problem: https://codeforces.com/problemset/problem/550/A
 *  Time Complexity: O(n)
 *
 */

#include<iostream>
#include<string>
#define MAX 10000
using namespace std;
int main( void ){
  /*lemma: trovo le 2 sequenze se riconosco AB e poi BA oppure se riconosco BA e poi AB */
  /*semplice automa a stati finiti*/
  int i = 0;
  int size;
  string s;
  cin >> s;
  size = s.size();
  i=0;
  while(i < size-1 && (s[i] != 'A' || s[i+1] != 'B')){
    i++;
  }
  i+=2;
  while(i < size-1 && (s[i] != 'B' || s[i+1] != 'A')){
    i++;
  }
  if(i < size-1){
    cout << "YES\n";
    return 0;
  }
  i=0;
  while(i < size-1 && (s[i] != 'B' || s[i+1] != 'A')){
    i++;
  }
  i+=2;
  while(i < size-1 && (s[i] != 'A' || s[i+1] != 'B')){
    i++;
  }
  if(i < size-1){
    cout << "YES\n";
    return 0;
  }

  cout << "NO\n";
  return 0;
}
