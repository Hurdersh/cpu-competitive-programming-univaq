 #include <stdio.h>
     
 int main(void)
 {
	 int n, x = 0; // initialize x to 0

        scanf("%d", &n);

        char s[4];

        while (n--)
        {
            scanf("%s", s); // read the next statement
            if (s[0] == '+' || s[1] == '+') // check if the statement is ++X or X++
                x++; // increment x
            else
                x--; // decrement x
        }

        printf("%d\n", x); // print the final value of x
        return 0;
}
