/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: HelpFull maths
 *  Link Problem: https://codeforces.com/problemset/problem/339/A
 *  Platform: Codeforces
 *  Execution Time: 62ms
 */

#include<stdio.h>
#include<stdlib.h>

void merge(int* A, int l, int m,int r){
  int i = l,j = m+1,k=0;
  int* result = malloc(sizeof(int) * (r-l+1));
  while(i <= m && j <= r){
    if(A[i] < A[j]){
      result[k] = A[i];
      i++;
    }
    else{
      result[k] = A[j];
      j++;
    }
    k++;
  }  
  while(i <= m){
    result[k] = A[i];
    i++;
    k++;
  }
  while(j <= r){
    result[k] = A[j];
    j++;
    k++;
  }
  for(int h = 0;h < r-l+1; h++){
    A[h + l] = result[h]; 
  }
  free(result);
}

void mergesort(int* A, int l, int m,int r){
  if(l < r){
    mergesort(A,l,(l+m)/2,m);
    mergesort(A,m+1,(m+1+r)/2, r);
    merge(A,l,m,r);
  }
}



int main( void ){
  int* numbers;
  int n = 1; 
  int nreaded = 0; //number readed
  int readed = 0; //temp value readed
  numbers = malloc(sizeof(int) * 1); //alloco il vettore di dimensione dinamica ad 1
  
  while(scanf("%d",&readed) == 1){ //fino a che leggo un intero vado avanti
    if(nreaded >= n){ //se ho sforato i posti raddoppio la dimensione del vettore
      numbers = realloc(numbers, sizeof(int) * (n+1) );
      n = n+1;
    }
    numbers[nreaded] = readed; //inserisco il valore letto
    nreaded++; //segno quanti valori leggo
  }
  //utilizzo ora un algoritmo di sorting in-place insertion sort
  mergesort(numbers,0,(0+nreaded-1)/2, nreaded-1);
  /*
  for(int i=0;i<nreaded-1;i++){
   printf("%d+",numbers[i]);
  }
  printf("%d\n",numbers[nreaded-1]);
  */
  return 0;
}


