/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: HelpFull maths
 *  Link Problem: https://codeforces.com/problemset/problem/339/A
 *  Platform: Codeforces
 *  Execution Time: 30ms
 */

#include<stdio.h>
#include<stdlib.h>


void insertion_sort(int* A, int n){
  int up;
  int j;
  for(int i = 1;i<n;i++){
    up = A[i];
    j = i-1;
    while(j >= 0 && up < A[j]){
      A[j+1] = A[j];
      j--;
    }
    A[j+1] = up;
  }
}



int main( void ){
  int* numbers;
  int n = 1; 
  int nreaded = 0; //number readed
  int readed = 0; //temp value readed
  numbers = malloc(sizeof(int) * 1); //alloco il vettore di dimensione dinamica ad 1
  
  while(scanf("%d",&readed) == 1){ //fino a che leggo un intero vado avanti
    if(nreaded >= n){ //se ho sforato i posti raddoppio la dimensione del vettore
      numbers = realloc(numbers, sizeof(int) * n * 2);
      n = n * 2;
    }
    numbers[nreaded] = readed; //inserisco il valore letto
    nreaded++; //segno quanti valori leggo
  }
  //utilizzo ora un algoritmo di sorting in-place insertion sort
  insertion_sort(numbers,nreaded);
  for(int i=0;i<nreaded-1;i++){
    printf("%d+",numbers[i]);
  }
  printf("%d\n",numbers[nreaded-1]);
  return 0;
}


