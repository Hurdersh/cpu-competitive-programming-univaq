/*
 *  Author: Giordano Colli
 *  Date: 24-08-2022
 *  Language: C
 *  Problem name: Hit The Lottery
 *  Link Problem: https://codeforces.com/problemset/problem/996/A
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>


int main( void ){
  int n;
  scanf("%d",&n);
  int bills_sum = 0;
  int bills[5];
  int remainder;
  bills[0] = 1;
  bills[1] = 5;
  bills[2] = 10;
  bills[3] = 20;
  bills[4] = 100;
  for(int i=0;i<4;i++){
    if(n >= bills[4-i]){
      remainder = n/bills[4-i];
      n = n - (remainder*bills[4-i]);
      bills_sum = bills_sum + remainder;
    }
  }
  bills_sum = bills_sum + n;
  printf("%d\n",bills_sum);
  return 0;
}


