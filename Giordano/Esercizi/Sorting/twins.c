/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Platform: 
 *  Difficult:
 *  Algorithm Or Idea used:
 *  Credits: 
 *  Problem name: Twins
 *  Link Problem: https://codeforces.com/problemset/problem/160/A
 *
 */

#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b){
  int temp = *a;
  *a = *b;
  *b = temp;
}

void fix_heap(int* A,int n,int i){
  int f1 = 2*i + 1;
  int f2 = 2*i + 2;
  //ho il figlio sinistro
  if(f1 < n){ 
    //ho anche il destro?
    if(f2 < n){
      if(A[f1] <= A[f2] && A[f1] < A[i]){
        swap(A + i,A + f1);
        fix_heap(A,n,f1);
      }
      else if(A[f2] < A[f1] && A[f2] < A[i]){
        swap(A+i,A+f2);
        fix_heap(A,n,f2);
      }
    }
    else{
      if(A[f1] < A[i]){
        swap(A+f1,A+i);
        fix_heap(A,n,f1);
      }
    }
  }
  //se non ho il sinistro non ho figli
}


void heapify(int* A,int n){
  for(int i = n/2;i>=0;i--){
    fix_heap(A,n,i);
  }
}

void max_retrieve(int* A,int n){
  swap(A,A+n-1);
  fix_heap(A,n-1,0);
}



void heapsort(int* A,int n){
  heapify(A,n);
  for(int i=0;i<n;i++){
    max_retrieve(A,n-i);
  }
}

int main(void){
  int n;
  int* A;
  int lsum = 0;
  int rsum = 0;
  int j = 0;
  scanf("%d",&n);
  A = malloc(sizeof(int) * n);
  //se esiste una soluzione ottima,essa e' fatta dall'insieme delle monete piu' grandi a partire dal basso
  //si dimostra facilmente per assurdo. siano a_i delle monete di costo c_i,
  // Se esiste una partizione R1 tale per cui a_i sta in R1 
  //ed un'altra R2 che comprende tutte le a_j diverse dalle a_i
  //sia R1 il numero minimo di monete tale per cui sum(a_i) > sum(a_j) ipotizziamo per assurdo che 
  //a_i non sia formata dalle monete maggiori => potremmo sostiturire le monete "non maggiori" con quelle 
  //piu' grandi utilizzando un numero <= di monete, lascio la formalizzazione della dimostrazione al lettore
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
    //utilizzo left sum e right sum per tenere traccia delle monete che prendo
    //quelle che prendo sono in left_sum,quindi inizialmente a 0
    //rsum ovviamente ha somma totale all'inizio e poi verra' decrementata
    rsum = rsum + *(A+i);
  }
  heapsort(A,n);
  j = 0;
  while(j < n && lsum <= rsum){ 
    rsum = rsum - A[j];
    lsum = lsum + A[j];    
    j++;
  }
  printf("%d\n",j);
  //bottleneck dell'algoritmo,ordinamento, fattibile in tanti modi, usero' heapSort, potrei usare integerSort
  //o simili ma, non sfrutto il fatto che sono limitato  
  return 0; 
}

