/*
 *  Author: Giordano Colli
 *  Date: 27-07-2022
 *  Language: C
 *  Problem name: The New Year
 *  Link Problem: https://codeforces.com/problemset/problem/723/A
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>

void swap(int* a,int* b){
  int temp;
  temp = *a;
  *a = *b;
  *b = temp;
}


int main( void ){
  int a,b,c,temp;
  scanf("%d %d %d",&a,&b,&c);
  //e' la minima possibile
  // quindi ordiniamo i valori supponendo che a <= b <= c
  // la funzione di costo al variare del valore x selezionato e' la seguente
  // come si sa bene il valore assoluto rappresenta le distanze
  // f(x) = |a - x| + |b - x| + |c - x|
  // supponiamo che x > c -> f(x) = x - a + |b - x| + x - c = 2x-a-c + x - b = 3x -a -b -c
  // supponiamo che x < a -> f(x) = a - x + |b - x| + c - x = a + c - 2x + b - x = -3x + a + b + c 
  // supponiamo che a <= x <= c -> f(x) = x - a + |b - x| + c - x = c - a + |b - x|
  // verifichiamo che il punto x si trova sempre tra a e b
  // dimostriamo che la funzione calcolata in a <= x1 <= c e' sempre minore di quella calcolata in x2 > c
  // 1. c - a + |b-x1| <= 3x2-a-b-c
  //    |b - x1| <= 3x2 -b -2c 
  //    divido per casi se x1 >= b ->   x1 - b <= 3x2-b-2c -> x1 <= 3x2-2c <= 3x2-2x2 <= x2  QED 
  //                       x1 < b  ->   b - x1 <= 3x2-a-b-c -> -x1 <= 3x2-a-2b-c < 3x2-a-2b-x2 = 2x2-a-2b = 
  //                                    2(x2-b)-a
  //                                    quindi se 2(x2-b)-a>0 -> x2 > a/2 + b > a/2 + x1 > x1 > 0 QED
  // 2. Analoga alla 1  
  // Infine la funzione di costo della nel caso in cui a<=x<=c e' f(x) = c - a + |b-x|
  // c-a e' una costante fissa, bisogna minimizzare b-x, che e' minimo se e solo se b=x => b-x = 0 QED               
  if(a < b){
    if(b > c){
      //b e' il piu' grande
      if(a > c){
        swap(&a,&c);
      }
      swap(&b,&c);
    }
  }
  else{
    if(b > c){
      swap(&a,&c);
    }
    else{
      //b e' il piu' piccolo
      if(a > c){
        swap(&a,&b);
        swap(&b,&c);
      }
      else{
        swap(&a,&b);
      }
    }
  }
  printf("%d\n",c-a);
  return 0;
}


