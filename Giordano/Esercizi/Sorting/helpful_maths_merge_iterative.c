/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: HelpFull maths
 *  Link Problem: https://codeforces.com/problemset/problem/339/A
 *  Platform: Codeforces
 *  Execution Time: 62ms
 */

#include<stdio.h>
#include<stdlib.h>

int* array;
int count=0;
void mergeSub(int *aux, int low, int mid, int high)
{
    int i = 0;
    int j = 0;
    int k = 0;

    for (i=mid+1; i>low; i--)
        aux[i-1]=array[i-1];

    for (j=mid; j<high; j++)
        aux[high+mid-j] = array[j+1];// copy in inverse order

    for (k=low; k<=high; k++)// l'indice k scorre l'array x[]
    {
        if (aux[j] <= aux[i])
           array[k]=aux[j--];
       else
           array[k]=aux[i++];
    }
}

void merge()
{
    int tempCount = count;

    int *aux = (int*)malloc(sizeof(int) * tempCount);

    //mempcy on smarts
    for(int i=0; i<tempCount; i++)
        aux[i] = array[i];

    int i = 0;
    int dim = 1;
    int t1 = 0;
    int t2 = 0;

    for (; dim < tempCount; dim = dim+dim)
        for (i=0; i < tempCount-dim; i = i+dim+dim)
        {
            t1 = i+dim+dim-1;
            t2 = tempCount-1;

            mergeSub( aux, i, i+dim-1, t1<t2 ? t1 : t2);
        }
}



  

int main( void ){
  int n = 1; 
  int readed = 0; //temp value readed
  array = (int*) malloc(sizeof(int)); 
  while(scanf("%d",&readed) == 1){ //fino a che leggo un intero vado avanti
    if(count >= n){ //se ho sforato i posti raddoppio la dimensione del vettore
      n+=1;
      array = (int*) realloc(array, sizeof(int) * n);
    }
    array[count] = readed; //inserisco il valore letto
    count++; //segno quanti valori leggo
  }
  //utilizzo ora un algoritmo di sorting in-place insertion sort
  merge();
 /* for(int i=0;i<count-1;i++){
   printf("%d+",array[i]);
  }
  printf("%d\n",array[count-1]);*/
  
  return 0;
}


