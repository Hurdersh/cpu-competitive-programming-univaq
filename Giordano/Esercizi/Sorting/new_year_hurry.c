/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: New Year and Hurry
 *  Link Problem: https://codeforces.com/problemset/problem/750/A
 *  Platform: Codeforces
 *  Complexity: O(n),O(log(n)),O(1)(forse...)
 *  Execution Time: 31 ms
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int translate(int m){
  return (5*(m+1)*(m+2))/2;
}
//ottimo algoritmo per trovare elementi minori uguali del cercato piu' grandi
//il piu' grande elemento <= k
int element_greater_less(int* A, int k,int l,int r){
  int m;
  while (l <= r){
    m = (l+r)/2;
    if(A[m] > k){
      r = m-1;
    }
    else{
      l = m+1;
    }
  }
  return l-1;
}

int element_greater_less_direct(int k,int l,int r){
  int m;
  while (l <= r){
    m = (l+r)/2;
    if(translate(m) > k){
      r = m-1;
    }
    else{
      l = m+1;
    }
  }
  return l-1;
}

int linear_solution(int n,int k){
  //genero un vettore che contiene la somma delle soluzioni
  //e' vero che 1 =< n <= 10 ma in un caso piu' generale potremmo avere n locazioni
  int* A = malloc(sizeof(int)*n);
  A[0] = 5;
  //per risolvere il primo problema ci metto 5 minuti,il primo + secondo 15, il primo + secondo + terzo = 15 + 15 = 30 etc 
  for(int i=1;i<n;i++){ 
    A[i] = A[i-1] + 5*(i+1);
  }
  //una volta generato l'array utilizzo la ricerca binaria per trovare il piu' grande elemento piu' piccolo del tempo
  //a disposizione, cioe' 240-k
  return element_greater_less(A,240-k,0,n-1);
}

int logaritmic_solution(int n,int k){
  //la soluzione logatimica consiste nel notare che non devo per forza "scorrere tutto l'array" per conoscere
  //il valore di A[n],quindi non ho bisogno di crearlo in tempo lineare
  //basti pensare alla sommatoria sum_i=0^n of 5i = 5*1 + 5*2 + 5*3
  //supponendo che la somma riesca ad arrivare ad n 5 * sum_i=0^n i = (utilizzo GAUSS) 5 *(n*(n+1))/2
  //questo e' il valore di A[n+1], quindi basta chiamare la funzione che si occupa di tradurre nel codice,
  //la chiameremo translate
  return element_greater_less_direct(240-k,0,n-1);
  //eliminando de facto il bottleneck di effettuare tutte le somme
}

int oneshot_solution(int n, int k){
  float formula;
  int floored;
  //sappiamo che A[n] = 5*((n+1)*(n+2))/2 e noi vogliamo che A[n] sia il piu' grande elemento minore di 240-k
  //chiamo 240-k = j  per comodita'
  //5*((n+1)*(n+2))/2 <= j
  // ((n+1) * (n+2)) <= 2/5 * j
  // n^2+3n+2-2/5j <= 0
  // quindi basta massimizzare il polinomio trovato su, dipendente da n e da j
  // consideriamo l'equazione associata e la risolviamo con la classica formuletta
  // (-3 +- sqrt(9-8+8/5j))/2
  // ora considerando che la soluzione con il + e' il piu' alto valore intero di n
  // e considerando che la funzione n^2+3n+c e' crescente per n >= 0, in particolare per n >= 2/3
  // possiamo fare floor della funzione in questione,presa ovviamente con segno positivo
  floored = floor((-3 + sqrt( 1 + ((8 * (240-k)) / 5) ))/2);
  //result2 = floor(-3 + sqrt((float)((1 + 8/5 * (240-k))/2) ) );
  //printf("%d\n",floored);
  
  //nota bene: se inserisco i valori precisi dell'array in oneshot solution => ottengo i precisi indici dell'array
  //oneshot_solution(5) = 0
  //oneshot_solution(15) = 1
  //oneshot_solution(30) = 2
  //etc, trovando una funzione di hashing perfetto e che approssima esattamente l'andamento del grafico delle soluzione
  //dicesi interpolazione

  //se il risultato supera gli n problemi ovviamente ne risolviamo n,ritornando n-1 a cui sara' aggiunto un uno
  //(escamotage per non mettere un if nel main :) )
  if(floored+1 >= n)
    return n-1;
  return floored;
  /*ATTENZIONE, LA SOLUZIONE NON E' DAVVERO O(1), purtroppo pur avendo trovato la formula chiusa
   * di interpolazione, il calcolo di sqrt(n) avviene in tempo O(log(n)), ma mentre il logaritmo di basava su n numero di problemi(elementi dell'array)
   * la radice quadrata si basa sul numero di minuti necessari +1 + 8/5, il che puo' essere un vantaggio
   * o uno svantaggio a seconda dei casi.
   * sia con le serie di taylor che con l'algoritmo babilonese la complessita' non scende :(
   */
}

int main( void ){
  int n;
  int k;
  scanf("%d %d",&n,&k);
  //considero il seguente fatto: i problemi sono sono risolti in tempi
  //che sono tutti multipli di 5, e sono in ordine crescente di tempo
  // => non esiste una combinazione di problemi non contigua ottimale
  // per assurdo se esistesse una combinazione non contigua ottimale
  // fatta da indici i e j potrei usare almeno un problema i+1 
  // per risolvere >= numero di problemi, siamo greedy
  //printf("%d\n",linear_solution(n,k)+1);
  //printf("%d\n",logaritmic_solution(n,k)+1);
  printf("%d\n",oneshot_solution(n,k)+1);
  return 0;
}


