/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Amusing Joke
 *  Link Problem: https://codeforces.com/problemset/problem/141/A
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>
#include<stdlib.h>


void insertion_sort(char* A, int n){
  int up;
  int j;
  for(int i = 1;i<n;i++){
    up = A[i];
    j = i-1;
    while(j >= 0 && up < A[j]){
      A[j+1] = A[j];
      j--;
    }
    A[j+1] = up;
  }
}

int check_equality(char* first,int nfirst, char* second, int nsecond){
  int i = 0;
   while(i < nfirst && first[i] == second[i]){
     i++;
   }
   if(i < (nfirst-1)){
     return 0;
   }
   return 1;
}



int main( void ){
  //stringhe in input
  char* first;
  char* second;
  //dimensione degli array dinamici
  int nfirst=1;
  int nsecond=1;
  //dimensione dell'input letto
  int readfirst = 0;
  int readsecond = 0;
  //indexes
  int i = 0;
  first = malloc(sizeof(char) * 1); 
  second = malloc(sizeof(char) * 1);
  char ch;
  
  while((ch = getchar()) != '\n'){ 
    if(readfirst >= nfirst){ //se ho sforato i posti raddoppio la dimensione del vettore
      first = realloc(first, sizeof(char) * nfirst * 2);
      nfirst = nfirst * 2;
    }
    first[readfirst] = ch; //inserisco il valore letto
    readfirst++; //segno quanti valori leggo
  }
  while((ch = getchar()) != '\n'){ 
    if(readfirst >= nfirst){ //se ho sforato i posti raddoppio la dimensione del vettore
      first = realloc(first, sizeof(char) * nfirst * 2);
      nfirst = nfirst * 2;
    }
    first[readfirst] = ch; //inserisco il valore letto
    readfirst++; //segno quanti valori leggo
  }
  while((ch = getchar()) != '\n'){ 
    if(readsecond >= nsecond){ //se ho sforato i posti raddoppio la dimensione del vettore
      second = realloc(second, sizeof(char) * nsecond * 2);
      nsecond = nsecond * 2;
    }
    second[readsecond] = ch; //inserisco il valore letto
    readsecond++; //segno quanti valori leggo
  }
  //check iniziale
  if(readfirst != readsecond){
    printf("NO\n");
  }
  else{
    //utilizzo ora un algoritmo di sorting in-place insertion sort

    insertion_sort(first,readfirst);
    insertion_sort(second,readsecond);

    /*for(int i=0;i<readfirst;i++){
      printf("%c",first[i]);
    }
    printf("\n");
    for(int i=0;i<readsecond;i++){
      printf("%c",second[i]);
    }*/
    if(check_equality(first,readfirst,second,readsecond)){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }

  return 0;
}


