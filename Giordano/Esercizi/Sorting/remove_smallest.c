/*
 *  Author: Giordano Colli
 *  Date: 27-07-2022
 *  Language: C
 *  Problem name: Remove Smallest
 *  Link Problem: https://codeforces.com/problemset/problem/1399/A
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>
#include<stdlib.h>

void merge(int* A, int l, int m,int r){
  int i = l,j = m+1,k=0;
  int* result = malloc(sizeof(int) * (r-l+1));
  while(i <= m && j <= r){
    if(A[i] < A[j]){
      result[k] = A[i];
      i++;
    }
    else{
      result[k] = A[j];
      j++;
    }
    k++;
  }
  while(i <= m){
    result[k] = A[i];
    i++;
    k++;
  }
  while(j <= r){
    result[k] = A[j];
    j++;
    k++;
  }
  for(int h = 0;h < r-l+1; h++){
    A[h + l] = result[h];
  }
  free(result);
}

void mergesort(int* A, int l, int m,int r){
  if(l < r){
    mergesort(A,l,(l+m)/2,m);
    mergesort(A,m+1,(m+1+r)/2, r);
    merge(A,l,m,r);
  }
}


int main( void ){
  int n;
  int l;
  int* array = malloc(sizeof(int));
  int k;
  scanf("%d",&n);
  for(int i=0;i<n;i++){
    scanf("%d",&k);
    array = realloc(array,sizeof(int) * k);
    for(int j=0;j<k;j++){
      scanf("%d",array+j);
    }
    mergesort(array,0,k/2,k-1);
  /*  for(int j=0;j<k;j++){
      printf("%d ",*(array+j));
    }
    printf("\n");*/
    l = 0;
    while(l < k-1 && array[l+1] - array[l] <= 1){
      l++;
    }
    if(l == k-1){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }
  return 0;
}


