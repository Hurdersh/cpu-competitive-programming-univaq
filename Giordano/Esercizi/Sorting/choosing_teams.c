/*
 *  Author: Giordano Colli
 *  Date: 31-08-2022
 *  Language: C
 *  Problem name: Choosing Team
 *  Link Problem: https://codeforces.com/problemset/problem/432/A
 *  Platform: Codeforces
 *  Execution Time: 15ms
 */

#include<stdio.h>
#include<stdlib.h>

int main(void){
  int n;
  int k;
  int* A;
  int i;
  int studenti = 0;
  //conto gli studenti che possono partecipare al torneo almeno k volte
  //cioe' quelli che hanno ancora k partecipazioni disponibili
  //per ottenere le partecipazioni disponibili = 5 - A[i]
  //una volta ottenuto il numero di studenti che possono partecipare mi basta dividerli
  //equamente /3 
  scanf("%d %d",&n,&k);
  A = malloc(sizeof(int) * n);
  for(i=0;i<n;i++){
    scanf("%d",A+i);
    if( (5-A[i]) >= k ){
      studenti++;
    }
  }
  printf("%d\n",studenti/3);
  return 0;
}


