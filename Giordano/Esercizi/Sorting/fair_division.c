/*
 *  Author: Giordano Colli
 *  Date: 24-08-2022
 *  Language: C
 *  Problem name: Fair Division
 *  Link Problem: https://codeforces.com/problemset/problem/1472/B
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>


int main( void ){
  int n;
  int m;
  int temp;
  int ones;
  int twos;
  scanf("%d",&n);
  
  
  for(int i=0;i<n;i++){
    ones = 0;
    twos = 0;
    scanf("%d",&m);
    for(int j=0;j<m;j++){
      scanf("%d",&temp);
      if(temp == 1){
        ones++;
      }
      else{
        twos++;
      }
    }
    //this one is an edge case
    if(ones == 0 && twos%2 == 1 || twos == 0 && ones%2 == 1){
      printf("NO\n");
    }
    //proof this one line
    //if the division exists lets define subsets
    //A1 = ones in the first set
    //A2 = twos in the first set
    //B1 = ones in the second set
    //B2 = twos in the second set
    // |A1| + 2|A2| = |B1| + 2|B2|  if the sum is equal
    // |A1| - |B1| = 2(|B2| - |A2|)
    // sum exists => clearly |A1| - |B1| must be even => |A1| and |B1| both even  or both odd
    // |A1|,|B1| alternate odd and even => sum not exists
    // now, it's easy to see that if the number of ones is even we cannot split into 2 even or 2 
    // odds => the sum does not exists
    // in constrast ones are even and twos are even we have a complete bipartition,easy
    // last case ones even and two odds, easy because we are considering the case
    // with one even [one - 2 ones ] and twos even [twos - 1 two], consider perfetc bipartion
    // and split one and twos in different parts and that's a linear solution
    else if(ones%2==0){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }
  return 0;
}


