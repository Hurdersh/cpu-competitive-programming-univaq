/*
 *  Author: Giordano Colli
 *  Date: 31-08-2022
 *  Language: C
 *  Problem name: Team Olimpiad
 *  Link Problem: https://codeforces.com/problemset/problem/490/A
 *  Platform: Codeforces
 *  Execution Time: 15ms
 */

#include<stdio.h>
#include<stdlib.h>

int min(int a,int b){
  if(a < b)
    return a;
  return b;
}
int main(void){
  int n;
  int i;
  int temp;
  int subjects[3];
  int** students;
  students = malloc(sizeof(int*) * 3);
  subjects[0] = 0;
  subjects[1] = 0;
  subjects[2] = 0;
  //per ogni elemento dell'array subjects corrispondono il numero di ragazzi bravi in quella materia
  //ovviamente una volta contati,il numero massimo di gruppi corrispondera' al minimo numero
  //di bambini bravi in una delle tre materie
  //ESEMPIO: 3 EF
  //         4 Programming
  //         5 Math
  //          ovviamente non posso fare piu' di 3 gruppi <= non avrei i ragazzi di educazione fisica
  //          sicuramente posso farne 3 <= ho almeno 3 ragazzi che fanno sia programming che math
  scanf("%d",&n);
  //ogni riga di students corrisponde ad una tipologia di studente
  for(i=0;i<3;i++){
    //utilizzando dei vettori dinamici possiamo ridurre la complessita' spaziale da O(3n) ad O(n)
    students[i] = malloc(sizeof(int) * n);
  }
  for(i=0;i<n;i++){
    scanf("%d",&temp);
    students[temp-1][subjects[temp-1]] = i;
    subjects[temp-1]++;
  }
  //riutilizzo la variabile temp per memorizzare il minimo, e formo le squadre in modo casuale
  temp = min(subjects[0], min(subjects[1],subjects[2]));
  printf("%d\n",temp);
  for(i=0;i<temp;i++){
    printf("%d %d %d\n",students[0][i]+1,students[1][i]+1,students[2][i]+1);
  }
  //free non necessarie ma consigliate
  for(i=0;i<3;i++){
    free(students[i]);
  }
  free(students);
  return 0;
}


