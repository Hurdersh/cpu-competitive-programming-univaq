/*
 *  Author: Giordano Colli
 *  Date: 26-07-2022
 *  Language: C
 *  Problem name: Another String Minimization Problem
 *  Link Problem: https://codeforces.com/problemset/problem/1706/A
 *  Platform: Codeforces
 *  Execution Time: 390ms
 */

#include<stdio.h>
#include<stdlib.h>

int main( void ){
  int cases,n,m,temp,c1,c2;
  char* string;
  scanf("%d",&cases);
  while(cases > 0){
    scanf("%d %d",&n,&m);
    string = malloc(sizeof(char) * m);
    for(int i=0;i<m;i++){
      string[i] = 'B';
    }
    for(int i=0;i<n;i++){
      scanf("%d",&temp);
      c1 = temp - 1; //choice 1 to remove
      c2 = (m+1-temp)-1; //choice 2 to remove 
      //se mi conviene m-temp
      if(c2 >= 0 && c2 < c1){
        //verifico che m-temp non sia gia' flaggato
        if(string[c2] == 'B'){
          string[c2] = 'A';
        }
        else{
          string[c1] = 'A';
        }
      }
      //se mi conviene temp
      else{
        //verifico che temp non sia gia' flaggato
        if(string[c1] == 'B'){
          string[c1] = 'A';
        }
        else{
          string[c2] = 'A';
        }
      }
    }
    for(int i=0;i<m;i++){
      printf("%c",string[i]);
    }
    printf("\n");
    free(string);
    cases--;
  }

}


