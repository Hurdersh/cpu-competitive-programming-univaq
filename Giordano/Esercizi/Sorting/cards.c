/*
 *  Author: Giordano Colli
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Cards
 *  Link Problem: https://codeforces.com/problemset/problem/1503/D
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct{
  int top;
  int bottom;
  int placed;
}card;

void insertion_sort(char* A, int n){
  int up;
  int j;
  for(int i = 1;i<n;i++){
    up = A[i];
    j = i-1;
    while(j >= 0 && up < A[j]){
      A[j+1] = A[j];
      j--;
    }
    A[j+1] = up;
  }
}


int main( void ){
  int n;
  int min;
  int max;
  int indexmin;
  int indexmax;
  card* cards;
  card* result;
  int* ordered;

  scanf("%d",&n);
  cards = malloc(sizeof(card) * n);
  result = malloc(sizeof(card) * n);
  ordered = malloc(sizeof(int) * 2* n);
  for(int i=0;i<n;i++){
    scanf("%d",&((*(cards+i)).top) );
    scanf("%d",&((*(cards+i)).bottom) );
  }
  //n^2 time complexity, se si vuole fare meglio abbassando a nlogn la ricerca basta utilizzare delle mappe
  for(int i=0;i<n/2;i++){
    //cerco minimo e massimo
    for(int j=0;j<n;j++){


  }
  return 0;
}


