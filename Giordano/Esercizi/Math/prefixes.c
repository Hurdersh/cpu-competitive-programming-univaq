/*
 *  Author: Giordano Colli
 *  Date: 19-10-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Pure Math
 *  Problem name: Prefixes
 *  Link Problem: https://codeforces.com/problemset/problem/1216/A
 *  Time Complexity: O(n)
 *
 */

#include <stdlib.h>
#include <stdio.h>

int main(void){
  int n;
  char* A;
  int swap = 0;
  //lemma: la stringa ha a e b in egual numero <=> ogni coppia consecutiva ([0,1],[2,3]...)ha egual numero di a e b
  // <= e' banale,se ogni coppia ha numero eguale di a e b => per induzione si dimostra facilmente la proprieta'
  // => se una coppia [x,x+1] non ha a e b in egual numero  => una coppia precedente deve avere 2 a e il prefix
  // che termina in quelle 2 a non ha un numero uguale di a e di b  
  scanf("%d",&n);
  A = malloc(sizeof(char) * n);
  getchar();
  for(int i=0;i<n;i=i+2){
    A[i] = getchar();
    A[i+1] = getchar();
    if(A[i] == A[i+1]){
      swap++;
      if(A[i] == 'b'){
        A[i] = 'a';
      }
      else{
        A[i] = 'b';
      }
    }
  }
  printf("%d\n",swap);
  for(int i=0;i<n;i++){
    putchar(A[i]);
  }
  return 0;
}

