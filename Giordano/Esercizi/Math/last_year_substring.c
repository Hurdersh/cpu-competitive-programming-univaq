/*
 *  Author: Giordano Colli
 *  Date: 16-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Dynamic Programming/KMP
 *  Problem name: The Cake is a lie
 *  Link Problem: https://codeforces.com/problemset/problem/160/A
 *  Time Complexity: O(1)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_LENGTH 200

int main(void){
  int n,a,b,x;
  char string[200];
  char match[4] = {'2','0','2','0'};
  scanf("%d",&x);
  for(int h=0;h<x;h++){
    scanf("%d",&n);
    scanf("%s",string);
    //noto semplicemente che se la stringa esistesse => gli estremi della stringa
    //devono essere appartenenti alla mia stringa finale, ed in modo contiguo, 
    //data la struttura del problema, per cui si generano esattamente 5 casi
    //1. prendo 1 elemento a partire da sinistra e 3 a partire da destra
    //2. prendo 2 elementi a partire da sinistra e 2 a partire da destra
    //3. prendo 3 elementi a partire da sinistra e 1 a partire da destra
    //4. prendo 4 elementi a partire da sinistra e 0 a partire da destra
    //5. prendo 0 elementi a partire da sinistra e 4 a partire da destra
    //l'algoritmo semplicemente potrebbe essere enumerativo, ma applichiamo una
    //decisione nel selezionare,cercando di formare la stringa, utilizzo 2 puntatori
    //per andare a decidere cosa inserire
    a = 0;
    b = 0;
    for(int i=0;i<4;i++){
      //se da sinistra posso prendere un elemento, che all'inizio sara' un 2
      //poi uno 0 poi un due poi uno 0, cioe' match[a]
      if(string[a] == match[a]){
        a++;
      }
      //stessa cosa da destra
      else if(string[n-1-b] == match[3-b]){
        b++;
      }
    }
    if(a+b == 4){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }
  return 0;
}

