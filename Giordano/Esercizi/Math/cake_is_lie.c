/*
 *  Author: Giordano Colli
 *  Date: 16-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Pure Math and induction generation
 *  Problem name: The Cake is a lie
 *  Link Problem: https://codeforces.com/problemset/problem/160/A
 *  Time Complexity: O(1)
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
  int x,n,m,k;
  scanf("%d",&x);
  for(int h=0;h<x;h++){
    scanf("%d %d %d",&n,&m,&k);
    //l'idea e' molto semplice e si dimostra costruttivamente 
    //che tutte le strade che portano ad (x,y) hanno tutte lo stesso numero di passi
    //quindi seleziono una delle strade,per esempio mi sposto a destra di tutte le colonne
    //ed in basso di tutte le righe ottenendo la funzione di costo desiderata = (m-1)+m*(n-1)
    //se essa e' uguale a k => ottimo, esiste una strada con lunghezza k,altrimenti no
    if((m-1)+m*(n-1) == k){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }
  return 0;
}

