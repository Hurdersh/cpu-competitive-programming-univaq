/*
 *  Author: Giordano Colli
 *  Date: 10-04-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Math(Linear Algebra)
 *  Credits: Me
 *  Problem name: Walking Master
 *  Link Problem: https://codeforces.com/contest/1806/problem/A
 *  Time Complexity: O(1)
 *
 */

#include<iostream>

using namespace std;

/*
 *
 * think at the problem starting in one point (x,y)
 * must do lambda moves of type (1,1) and gamma moves of type (-1,0)
 * ok, point that i will touch is (x,y) + lambda(1,1) + gamma(-1,0)
 * clearly this means that move in different order doesn't change final point touched
 * let's solve linear system and we obtain
 * lambda = b - y;
 * gamma = x+b-y-a;
 * i can ONLY do integer moves(discrete world) and only positive moves
 * if b - y < 0 => b < y i cannot do moves to bottom, cannot touch this point
 * if x + b - y - a < 0 => x-y < a-b => there is in a left side triangle not touchable
 *
 * total moves are the sum of right moves(lambda) + sum of left moves(gamma)  
 *
 */

void solve(){
  int x,y,a,b;
  int lambda,gamma;
  cin >> x >> y >> a >> b;
  lambda = b - y;
  gamma = x+b-y-a;
  if(lambda >= 0 && gamma >= 0){
    cout << lambda + gamma << endl;
  }
  else{
    cout << "-1" << endl;
  }
}
int main(void){
  int t;
  cin >> t;
  while(t--){
    solve();
  }
  return 0;
}
