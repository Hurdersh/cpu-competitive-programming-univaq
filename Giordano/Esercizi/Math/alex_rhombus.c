/*
 *  Author: Giordano Colli/Katiusha Pellone
 *  Date: 20-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Pure Math and induction generation
 *  Problem name: Alex and a Rhombus
 *  Link Problem: https://codeforces.com/problemset/problem/1180/A
 *  Time Complexity: O(1)
 *
 */

#include <stdio.h>
#include <stdlib.h>

int solve(int n){
  return 1+(2*n*(n-1));
}
int solve_kay(int n){
  return (n*n)+((n-1)*(n-1));
  /*
  * Se si iniziano a disegnare i quadratini per un n qualunque si nota che si ha in prima riga 
  * un quadratino, in seconda tre quadratini, in terza cinque e così via fino alla riga "centrale"
  * che ne ha x. Sotto a questa ci sono x-2 quadratini e così via finché non si torna a un quadratino
  * in ultima riga.
  * Qual è la riga centrale? Si nota anche qui che con n=2, la riga centrale è la seconda.
  * Con n=3, è la terza. Con n=4, è la quarta ecc.
  * Quindi riassumendo: bisogna sommare i primi n numeri dispari per avere il numero di quadrati 
  * sopra la riga centrale inclusa (e questo fa n^2). Poi bisogna sommare i numeri dispari delle 
  * righe sotto, e questi sono n-1 (quindi (n-1)^2).
  */
}
int main(void){
  int n;
  //e' facile vedere che induttivamente si fa la cornice , essa e' sempre 4*n
  //da cui la formula a_n = a_(n-1) + 4 * (n-1), scompattando la formula otteniamo
  //a_n = a_0 + 4(1 + 2 + ... + n-1) da cui la formula a_n = 1 + 2 * (n) * (n-1)
  scanf("%d",&n);
  printf("%d\n",solve(n));
  //printf("%d\n",solve_kay(n));
  return 0;
}

