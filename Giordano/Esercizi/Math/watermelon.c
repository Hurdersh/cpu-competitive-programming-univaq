/*
 *  Author: Giordano Colli
 *  Date: 19-10-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Pure Math 
 *  Problem name: Watermelon
 *  Link Problem: https://codeforces.com/problemset/problem/4/A
 *  Time Complexity: O(1)
 *
 */

#include <stdio.h>

int main(void){
  int n;
  scanf("%d",&n);
  //lemma: possiamo dividere in 2 parti pari n se e solo se n e' pari
  //se per assurdo n fosse dispari => a + b = n
  //ma siccome n e' dispari uno dei due tra a e b e' dispari
  if(n%2 == 0){
    //lemma: se il numero n e ' pari => (n-2) + 2 continua ad essere pari
    //ed e' una soluzione ammissibile, ovviamente non vale per n = 2 dato che 
    //n-2 = 0 
    if(n == 2){
      printf("NO");
    }
    else{
      printf("YES\n");
    }
  }
  else{
    printf("NO\n");
  }
  return 0;
}

