/*
 *  Author: Giordano Colli
 *  Date: 01-01-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Divisibility by eight
 *  Credits: ME
 *  Problem name: Divisibility by eight
 *  Link Problem: https://codeforces.com/problemset/problem/550/C
 *  Time Complexity: O(n)
 *
 */

#include<iostream>
#include<string>

int toint(char c){
  return c - '0';
}

using namespace std;
int main(void){
  string s;
  cin >> s;
  
  int right[10];
  int left[10];
  int found = 1;
  for(int i=0;i<10;i++){
    right[i] = -1;
    left[i] = 100;
  }

  //LEMMA: un numero e' divisibile per 8 iff le ultime 3 cifre sono divisibili per 8
  //CERCO I numero divisibili per 8 di al piu' 3 cifre
  //cerco 0 o 8 ed ho un numero divisibile per 8 
  for(int i=0;i<s.size();i++){
    right[toint(s[i])] = i;
    if(left[toint(s[i])] == 100){
      left[toint(s[i])] = i;
    }
    if(toint(s[i]) == 0 || toint(s[i]) == 8){
      found = toint(s[i]);
    }
  }
  if(found == 0 || found == 8){
    cout << "YES" << endl;
    cout << found << endl;
    return 0;
  }
  found = 0;
  
  //cerco le coppie div per 16 
  //LEMMA se esiste una tripla PXX dove P e' un numero pari => XX e' div per 8
  if(left[2] < right[4]){
    found = 24;
  }
  else if(left[3] < right[2]){
    found = 32;
  }
  else if(left[7] < right[2]){
    found = 72;
  }
  else if(left[1] < right[6]){
    found = 16;
  }
  else if(left[6] < right[4]){
    found = 64;
  }
  else if(left[9] < right[6]){
    found = 96;
  }
  else if(left[5] < right[6]){
    found = 56;
  }
  if(found){
    cout << "YES" << endl;
    cout << found << endl;
    return 0;
  }
 
  //se non ho ancora trovato niente rimane una unica conformazione D44 | D(1|5|9)2 | D(7|3)6
  //verifico l'esistenza di una di queste
  int j=0;
  int state = 0;
  while(j<s.size() && state < 3){
    if(toint(s[j])%2 == 1 && state == 0){
      found = toint(s[j]);
      state++;
    }
    else if (state == 1 && toint(s[j]) == 4){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    else if (state == 2 && toint(s[j]) == 4){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    j++;
  }
  if(state == 3){
    cout << "YES" << endl;
    cout << found << endl;
    return 0;
  }

  j=0;
  state=0;
  while(j<s.size() && state < 3){
    if(toint(s[j])%2 == 1 && state == 0){
      found = toint(s[j]);
      state++;
    }
    else if (state == 1 && (toint(s[j]) == 1 || toint(s[j]) == 5 || toint(s[j]) == 9)){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    else if (state == 2 && toint(s[j]) == 2){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    j++;
  }
  if(state == 3){
    cout << "YES" << endl;
    cout << found << endl;
    return 0;
  }

  j=0;
  state=0;
  while(j<s.size() && state < 3){
    if(toint(s[j])%2 == 1 && state == 0){
      found = toint(s[j]);
      state++;
    }
    else if (state == 1 && (toint(s[j]) == 3 || toint(s[j]) == 7)){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    else if (state == 2 && toint(s[j]) == 6){
      found*=10;
      found += toint(s[j]);
      state++;
    }
    j++;
  }
  if(state == 3){
    cout << "YES" << endl;
    cout << found << endl;
    return 0;
  }
  cout << "NO" << endl;
  return 0;
}
