/*
 *  Author: Giordano Colli
 *  Date: 12-11-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Pure Math
 *  Credits: Me,Myself and I
 *  Problem name: Bestie
 *  Link Problem: https://codeforces.com/problemset/problem/1732/A
 *  Time Complexity: O(n * log(max(a_n,a_n-1)))
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAXA 20

int gcd(int a, int b)
{
    int temp;
    while (b != 0)
    {
        temp = a % b;

        a = b;
        b = temp;
    }
    return a;
}

/*
 * Il problema si risolve osservando attentamente cio' che chiede
 * gcd(a_1,a_2,...,a_n) = 1 <==> esiste una coppia di indici i,j in n : gcd(a_i,a_j) = 1
 * Proof
 *  ------ =>
 *  supponiamo per ASSURDO che gcd(a_1,a_2,...,a_n) = 1 e per ogni (i,j) in n gcd(a_i,a_j) > 1
 *  consideriamo il caso 3, i successivi seguono per induzione
 *  => gcd(a_i,a_j,a_k) = gcd(gcd(a_i,a_j),a_k) = gcd(a_i,gcd(a_j,a_k)) = gcd(gcd(a_i,a_k),a_j)
 *  => in ognuno dei 3 casi il numero che rimane 'scoperto' da gcd ha almeno un fattore >1 in comune
 *  con gli altri 2 quindi gcd(scoperto,2coperti) e' > 1
 *  => per induzione i casi con n >= 3
 *  ------- <=
 *  supponiamo esistano (i,j) : gcd(a_i,a_j) = 1
 *  WLOG
 *  gcd(a_1,a_2,...,a_i,...,a_j,...,a_n) = gcd(gcd(a_i,a_j),...)= gcd(1,...) = 1
 *
 * quindi il problema si RIDUCE a dover trovare 2 indici (eventualmente 1 o 0) tali per cui essi siano coprimi
 * pensando agli elementi in posizione n ed n-1 (quelli con costo piu' efficiente)
 *
 * gcd(gcd(a_n,n),gcd(a_n-1,n-1)) = 1  ==> una delle seguenti
 *  - gcd(a_n-1 a_n) = 1
 *  - gcd(a_n,n-1) = 1
 *  - gcd(n,a_n-1) = 1
 *  - gcd(n,n-1) = 1 ************
 *  - gcd(a_n,n) = 1
 *  - gcd(a_n-1,n-1) =1
 *
 *  ma quella contrassegnata e' sempre vera,ovviamente
 *  
 *  Quindi ci sono 3 casi nel nostro problema
 *  - dobbiamo trovare una coppia (i,j): gcd(gcd(a_i,i),gcd(a_j,j)) = 1 RISOLTO, sono gli ultimi 2,sempre
 *  - dobbiamo trovare 0 elementi => calcolo del gcd tra tutti secondo la definizione
 *    gcd(a_1,a_2,...,a_n) = gcd(gcd(a_1,a_2),a_3,...,a_n) and so on
 *  -dobbiamo trovare 1 elemento, ma quali elementi ci conviene valutare? quelli che hanno costo minore
 *  degli ultimi 2 messi assime
 *  C(n,n-1) = (n-n+1) + (n-(n-1)+1) = 2
 *  quindi il costo del singolo elemento C(i) deve essere minore di 2
 *  => C(i) = n-i+1 < 2 
 *            -i<1-n
 *            i >= n-1
 *  Ha senso valutare sono gli ultimi 2 elementi :)
 *  da questo il seguente algoritmo con tempo di esecuzione O(n * log(max(a_n,a_n-1)))
 *
 *
 *
 */


int main(void){
  int k;
  int n;
  int A[MAXA];
  int DP[MAXA];
  scanf("%d",&k);
  while(k--){
    scanf("%d",&n);
    for(int i=0;i<n;i++){
      scanf("%d",A+i);
    }
    //caso base
    DP[0] = A[0];
    //calculating gcd of all elements
    for(int i=1;i<n;i++){
      DP[i] = gcd(A[i],DP[i-1]);
    }
    //sono tutti coprimi
    if(DP[n-1] == 1){
      printf("0\n");
    }
    else{
      //non sono tutti coprimi
      //verifico che gcd(A[n-1],n) sia coprimo a tutti i precedenti
      if( gcd(gcd(A[n-1],n),DP[n-2]) == 1){
        printf("1\n");
      }
      else if( (gcd(gcd(A[n-2],n-1), DP[n-3]) == 1) || gcd(gcd(A[n-2],n-1),A[n-1]) == 1){
        printf("2\n");
      }
      else{
        printf("3\n");
      }
    }
  }
  return 0;
}

