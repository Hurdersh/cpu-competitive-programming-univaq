/*
 *  Author: Giordano Colli
 *  Date: 09-10-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1900
 *  Algorithm Or Idea used: Fenwick Tree
 *  Credits: Peter Fenwick
 *  Problem name: Multiset
 *  Link Problem: https://codeforces.com/problemset/problem/1354/D
 *  Time Complexity: O(q log(n))
 *
 */

#include <stdio.h>
#include <stdlib.h>

int abs(int temp){
  if(temp >= 0)
    return temp;
  return -1 * temp;
}

//magic function
int p(int i){
  return (i & (i+1));
}

//classico update dei fenwick tree
int update(int v,int i, int* fenwick_tree, int n){
  int j = i;
  while(j < n){
    fenwick_tree[j] += v;
    j = j|(j+1);
  }
}

//sum from o to i of fenwick tree,classical
int minisum(int i,int* fenwick_tree,int n){
  int res = 0;
  int j = i;
  while(j >= 0){
    res += fenwick_tree[j];
    j = p(j)-1;
  }
  return res;
}

//sum based on prefix sum,classical
int sum(int i,int j,int* fenwick_tree,int n){
  if(i > 0){
    return minisum(j,fenwick_tree,n);
  }
  return minisum(j,fenwick_tree,n) - minisum(i-1,fenwick_tree,n); 
}

//smallest i >= k
//Core Algorithm, qui svolgiamo una ricerca binaria sulle prefx sum
//avendo cura di ricalcolare la somma, questo bottleneck e' O(log^2(n))
//eseguo una ricerca binaria in tempo logaritmico per capire quale i ha k-order 
//piu' e >= di k
int findkorder(int k, int* fenwick_tree, int n){
  int l = 0;
  int r = n-1;
  int m;
  while (l <= r){
    m = (l+r)/2;
    if(minisum(m,fenwick_tree,n) >= k){
      r = m-1;
    }
    else{
      l = m+1;
    }
  }
  return r+1;
}

int main(void){
  int n;
  int temp;
  int q;
  int* A;
  int* order_statistics;
  int* fenwick_tree;
  int i;
  int j;

  scanf("%d",&n);
  scanf("%d",&q);
  A = malloc(sizeof(int) * n);
  order_statistics = malloc(sizeof(int) * n);
  fenwick_tree = malloc(sizeof(int) * n);

  //initializing array A;
  for(i=0;i<n;i++){
    A[i]=0;
  }

  //segno in un array tutti i valori dei k-th order statistics 
  for(i=0;i<n;i++){
    scanf("%d",&temp);
    A[temp-1]++;
  }

  //creation of data structure with sum of order statistics,the prefix sum of
  //fenwick tree creation
  order_statistics[0] = A[0]; 
  for(i=1;i<n;i++){
    order_statistics[i] = order_statistics[i-1] + A[i];
  }
  free(A);

  //creation of fenwick_tree,a classical example of creation
  for(i=0;i<n;i++){
    if(p(i) > 0){
      fenwick_tree[i] = order_statistics[i] - order_statistics[p(i)-1];
    }
    else{
      fenwick_tree[i] = order_statistics[i];
    }
  } 

  free(order_statistics);
    

  //scorro tutte le query
  for(i=0;i<q;i++){
    scanf("%d",&temp);
    if(temp < 0){
      //remove query, se devo andare a rimuovere trovo il k order statistics ed aggiorno
      //il valore dello stesso a -1
      temp = abs(temp);
      j = findkorder(temp,fenwick_tree,n);     
      update(-1,j,fenwick_tree,n);
    }
    else{
      //query di inserimento, vado ad inserire l'iesimo elemento
      update(1,temp-1,fenwick_tree,n);
    }
    //debug print of fenwick tree update
    /*for(int k=0;k<n;k++){
      printf("%d ",fenwick_tree[k]);
    }
    printf("\n");*/
  }

  //alla fine della tiritera devo provare a vedere se c'e' qualcuno nel multiset
  //e lo faccio con il metodo gia' creato findkorder con parametro >= 1
  j = findkorder(1,fenwick_tree,n);
  //se non trovo niente printo 0
  if( j == n ){
      printf("0\n");
  }
  else{
    printf("%d\n",j+1);
  }
  return 0;
}
