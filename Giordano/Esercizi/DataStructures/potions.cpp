/*
 *  Author: Giordano Colli
 *  Date: 09-01-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Binary Search
 *  Credits: Me
 *  Problem name: Potions
 *  Link Problem: https://codeforces.com/problemset/problem/1526/C1
 *  Time Complexity: O(nlogn)
 *
 */

#include<iostream>
#include<queue>
#define MAXN 2000

using namespace std;

int main(void){
  priority_queue <long long, vector<long long>, greater<long long>> coda;
  long long vita = 0;
  long long result = 0;
  long long n;
  long long a[MAXN];
  cin >> n;
  for(long long i=0;i<n;i++){
    cin >> a[i];
    
    if(a[i] >= 0){
      vita = vita+a[i];
      result++;
    }
    //a[i] < 0
    else{
      //posso inserirlo? lo inserisco ma tengo a mente che posso rimuoverlo
      if(vita + a[i] >= 0){
        vita += a[i];
        result++;
        coda.push(a[i]);
      }
      //non posso inserirlo, vedo se posso rimuovere qualcuno
      else{
        if(!coda.empty() && a[i] >=  coda.top()){
          vita = vita + a[i] - coda.top();
          coda.pop();
          coda.push(a[i]);
        }
        //se non posso inserirlo e' sconveniente
      }
    }
  } 
  cout << result << endl;
  return 0;
}

