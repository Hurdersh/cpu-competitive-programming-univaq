/*
 *  Author: Giordano Colli
 *  Date: 15-01-2023
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1700
 *  Algorithm Or Idea used: Segment Tree
 *  Credits: Me
 *  Problem name: Xenia and Bit Operations
 *  Link Problem: https://codeforces.com/problemset/problem/339/D
 *  Time Complexity: O(klogn) where k is the number of queries
 *
 */



#include<stdio.h>
#define MAXN (1 << 19)+1

long long vettore[MAXN];
long long tree[2*MAXN];

/*Creazione/Aggiornamento del segment tree a partire dall'alto, tenendo conto via via dell'operazione da 
 * effettuare , mi fermo ad un passo base che non e' la foglia ma un attimo prima per 'settare' la prima operazione*/
long long build(long long x, long long left, long long right){
  if(right-left == 1){
    //passo base inserisco nell'albero l'or tra i due elementi
    tree[2*x+1] = vettore[left];
    tree[2*x+2] = vettore[right];
    tree[x] = (tree[2*x+1] | tree[2*x+2]);
    return 0;
  }
  long long m = (right+left)/2;
  long long op;
  op = build(2*x+1,left,m);
  build(2*x+2,m+1,right);
  if (op == 0){
    tree[x] = tree[2*x+1] ^ tree[2*x+2];
    return 1;
  }
  tree[x] = tree[2*x+1] | tree[2*x+2];
  return 0;
}

long long update(long long x, long long left,long long right, long long p, long long b){
  if(right-left == 1){
    //passo base inserisco nell'albero l'update
    if(p == left){
      tree[2*x+1] = b;
    }
    else{
      tree[2*x+2] = b;
    }
    tree[x] = (tree[2*x+1] | tree[2*x+2]);
    return 0;
  }
  long long m = (right+left)/2;
  long long op;
  if(p <= m){
    op = update(2*x+1,left,m,p,b);
  }
  else{
    op = update(2*x+2,m+1,right,p,b);
  }
  if(op == 0){
    tree[x] = tree[2*x+1] ^ tree[2*x+2]; 
    return 1;
  }
  tree[x] = tree[2*x+1] | tree[2*x+2]; 
  return 0;
}

long long main(void){
  long long n,m;
  long long p,b;
  long long integers;
  scanf("%lld %lld",&n,&m);
  integers = 1 << n;


  //input of numbers
  for(long long i=0;i<integers;i++){
    scanf("%lld", vettore+i);
  }
  
  
  //creo il segment tree
  build(0,0,integers-1);
  
  for(long long i=0;i<m;i++){
    scanf("%lld %lld",&p,&b);
    //query di update
    update(0,0,integers-1,p-1,b);
    printf("%lld\n",tree[0]);
  }
 
  
  
  
  //debug print
  /*
  for(long long i=0;i<2 * integers-1;i++){
    printf("%lld ",tree[i]);
  }
  printf("\n");
  */
  
  //oinz
  return 0;
}
