/*
 *  Author: Giordano Colli
 *  Date: 05-11-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: DP
 *  Problem name: Traffic Light
 *  Link Problem: https://codeforces.com/problemset/problem/1744/C
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#define MAXCHAR 200000

//l'idea e' di modellare il problema come segue: 
//array di 0 e 1 dove 1 sono i green e 0 sono gli y r
//posso iniziare una sequenza solo da 0 con y o r che corrispondono alla posizione iniziale
//trovo la massima sequenza di 0 iniziata da uno 0s e finita da un 1
int main(void){
  int t;
  int n;
  char c;
  int temp;
  int max;
  char s[MAXCHAR];
  scanf("%d",&t);
  while(t--){
    scanf("%d",&n);
    scanf(" %c",&c);
    temp = 0;
    max = 0;
    scanf("%s",s);
    //se inizio dal verde e' garantito che ci sia un verde => ritorno 0
    if(c == 'g'){
      printf("0\n");
    }
    else{
      for(int i=0;i<n;i++){
        //quando incontro un verde -> finisce la streak di zeri e mi segno il value
        if(s[i] == 'g'){
          if(temp > max){
            max = temp;
          }
          temp = 0;
        }
        //se incontro un rosso o un giallo
        else if(s[i] == 'r' || s[i] == 'y'){
          //se ero gia' in una streak bene -> continuo la streak
          if(temp > 0){
            temp++;
          }
          //se non lo ero allora la inizio se e solo se mi trovo nel carattere iniziale c
          else if(s[i] == c){
            temp++;
          }
        }
      }

    //ci potrebbe essere una intersezione solo fino al primo green
    //se ho chiuso la streak => un green termina la streak, le streak che andremo
    //a guardare sono le stesse
    //quindi se e solo se temp > 0 alla fine (streak iniziata) => proseguo fino al primo verde da incontrare 
    //non aggiunge complessita' => il controllo postumo tutt'alpiu' mi fa ciclare n volte
    //O(2n) = O(n)
    
    if(temp > 0){
      for(int i=0;i<n && s[i] != 'g'; i++){
        temp++;
      }
      if(temp > max){
        max = temp;
      }
    }
    printf("%d\n",max);
   }
  }
  return 0;
}

