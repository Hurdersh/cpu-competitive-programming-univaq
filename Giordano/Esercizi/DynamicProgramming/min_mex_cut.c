/*
 *  Author: Giordano Colli
 *  Date: 25-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Specific Theorem & finite state automata
 *  Credits: Me
 *  Problem name: MIN-MEX Cut
 *  Link Problem: https://codeforces.com/problemset/problem/1566/B
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#include <stdlib.h>



int main(void){
  /*
   * Banalmente mex(0) = 1
   *            mex(1) = 0
   *            noto che se ci sono piu' uni che si susseguono mex(11111111111) = 0
   *            noto che se ci sono piu' zeri che si susseguono mex(0000000000) = 1
   *            quindi posso considerare la stringa di tutti 1 come un singolo 1 e la
   *            stringa di 0 come un singolo 0
   *            TEOREMA: il costo minimo della suddivisione mex e' >=2 se e solo sela stringa contiene
   *            uno 0 seguito da un uno seguito da uno 0 
   *             se: basta notare che i 2 zeri "separati da un uno" costano minimo uno a testa
   *             solo se: se il costo minimo fosse 1 o 0 allora esisterebbe una suddivisione
   *                      tale per cui le sottostringhe facciano WLOG tutte 0 ed una 1
   *                      ma notato che piu' zeri e piu' uni da soli sono identici tale stringa
   *                      e' qualunque stringa che non contiene 0...1...0
   *                      tutte le altre conformazioni contenenti uno 0 producono come min_mex un uno
   *                      solo la conformazione con tutti 1 produce uno 0
   *            dato questo teorema per vero, ci si accorge che min_mex di una stringa che contiene 
   *            0..1..0 deve pesare almeno 2 => tutta la sottostringa pesa 2 => abbiamo una soluzione ottima
   *            se la stringa contiene un solo 0....  => il costo vale almeno uno, ma non contenendo 
   *            un'altra sequenza di zeri il costo vale necessariamente 1 al minimo
   *            la sequenza che non contiene 0 vale esattamente 1
   *
   *
   */
  int k;
  char actual;
  int status;
  scanf("%d",&k);
  getchar();
  for(int h=0;h<k;h++){
    status = 0; //automa nello stato 0
    //non posso ottimizzare uscendo prima dall'automa dato che le getchar si rompono, e non mi
    //e' dato sapere quanto e' lunga la stringa,dovrei fare I/O con getline,ma linear per linear
    //l'ottimizzazione suddetta e' lasciata al lettore
    while( (actual = getchar()) && actual != '\n' && actual != EOF){
      if(actual == '0'){
         if(status == 0 || status == 2){ //se sono al primo 0 o dopo l'uno incremento
           status++;
         }
      }
      else{
        if(status == 1){ //se lo status == 1 cioe' ho incontrato il primo 0 incremento lo status
          status++;
        }
      }
    }
    if(status == 3){ //ho incontrato 010
      printf("2\n");
    }
    else if(status >= 1){ //ho incontrato almeno uno 0 ma non 010
      printf("1\n");
    }
    else{ //ho incontrato solo 1
      printf("0\n");
    }
  }
  return 0;
}

