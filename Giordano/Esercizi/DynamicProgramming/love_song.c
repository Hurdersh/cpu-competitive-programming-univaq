/*
 *  Author: Giordano Colli
 *  Date: 21-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: prefix sum
 *  Problem name: Love Song
 *  Link Problem: https://codeforces.com/problemset/problem/1539/B
 *  Time Complexity: O(n+q) dove n e' la lunghezza della stringa 
 *                   e q il numero di query
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
  int n,k,l,r;
  char* A;
  int* prefix;
  scanf("%d %d",&n,&k);
  //utilizzo un metodo chiamato prefix sum, tengo un array di interi lungo esattamente
  //n locazioni, nell'ennesima locazione tengo memorizzata tutta la somma dei primi n elementi
  //quindi se voglio ottenere la somma degli elementi da i a j, prendo la somma di tutti gli 
  //elementi fino a j - la somma di tutti gli elementi fino ad i :)
  //ovviamente ogni lettera ha un numero fissato di lettere che rappresenta
  // a = 1,b=2,c=3 etc
  A = malloc(sizeof(char) * n);
  prefix = malloc(sizeof(int) * n);
  scanf("%s",A);
  //scorro l'array, la lettera x va convertita ad int attraverso 'x' - 'a' + 1 
  prefix[0] = (int) (A[0] - 'a' + 1);
  for(int i=1;i<n;i++){
   prefix[i] = prefix[i-1] + ((int)(A[i] - 'a' + 1));
  }

  for(int i=0;i<k;i++){
    scanf("%d %d",&l,&r);
    l--;
    r--;
    if(l == 0){
      printf("%d\n",prefix[r]);
    }
    else{
      printf("%d\n",prefix[r]-prefix[l-1]);
    }
  }
  free(A);
  free(prefix);
  return 0;
}

