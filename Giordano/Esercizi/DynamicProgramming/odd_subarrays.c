/*
 *  Author: Giordano Colli
 *  Date: 24-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Math Deduction
 *  Problem name: Odd Subarray
 *  Link Problem: https://codeforces.com/problemset/problem/1686/B
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#define MAX_ROW 100000

int main(void){
  /*
   * Dopo aver analizzato attentamente il problema ci accorgiamo che esso ci chiede di trovare
   * il massimo numero di scambi distinti, cioe' se esistono 2 scambi (i,j),(h,k) => con i,j,h,k gli elementi
   * => i != j != h != k.
   *
   * AD ESEMPIO 2 4 3 6 1 => dobbiamo contare 4 3 e 6 1
   * 
   * lemma1: il numero massimo di subarray e' dato dal numero di coppie distinte, da dimostrazione ovvia
   * lemma2: il numero di coppie distinte massimali e' dato dalle coppie adiacenti, essendo un problema 
   * del matching e potendo matchare tutti, il numero di matching massimale sono n/2, scelti in qualunque 
   * ordine
   *
   *
   */
  int h;
  int n;
  int b[MAX_ROW];
  int counter;
  scanf("%d",&h);
  for(int j=0;j<h;j++){
    counter = 0;
    scanf("%d",&n);
    for(int i=0;i<n;i++){
      scanf("%d",b+i);
    }
    for(int i=0;i<n-1;i++){
      if(b[i] > b[i+1]){
        counter++;
        i++;
      }
    }
    printf("%d\n",counter);
  }
}

