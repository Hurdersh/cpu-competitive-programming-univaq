/*
 *  Author: Giordano Colli
 *  Date: 11-11-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Dp and greedy
 *  Problem name: New Theatre Square
 *  Link Problem: https://codeforces.com/problemset/problem/1359/B
 *  Time Complexity: O(n*m) where n*m is input
 *
 */

#include <stdio.h>

int min(int a,int b){
  if(a < b)
    return a;
  return b;
}

//global data structure used
char A[100][1000];
int DP [100];

int solve_DP(int n,int m,int x,int y){
  //la formulazione DP e' la seguente
  //dato il LEMMA1 => considero di utilizzare un array DP per ogni riga
  //DP[n] = minimo costo ottenibile da ogni casella
  //DP[0] = {x se la casella e' bianca(uso solo lei),
  //         0 se e' nera(non la riempio)}
  //
  //DP[1] = {DP[0]     se la casella 1 e' nera(non la riempio)
  //         min(2x,y) se la casella 1 e' bianca e la 0 e' bianca(posso usare due caselline 1x1 o una 1x2),
  //         x         se la 1 e' white e la 0 e' black}
  //
  //DP[n] = {DP[n-1]                    se n e' nera  (non devo riempire niente)
  //         DP[n-1] + x                se n e' bianca e n-1 e' nera (devo usare per forza una tessera)
  //         min(DP[n-2]+y,DP[n-1]+x)   se n e' bianca e n-1 e' bianca (o utilizzo due tessere da x o una da y)
  int sum = 0;
  for(int i=0;i<n;i++){
    //DP[0]
    if(A[i][0] == '.'){
      DP[0] = x;
    }
    else{
      DP[0] = 0;
    }
    //DP[1]
    if(A[i][1] == '*'){
      DP[1] = DP[0];
    }
    else if(A[i][0] == '.'){
      DP[1] = min(2*x,y);
    }
    else{
      DP[1] = x;
    }
    for(int j=2;j<m;j++){
      //nuova casella black
      if(A[i][j] == '*'){
        DP[j] = DP[j-1];
      }
      //sono white e il mio precedente e' black
      else if(A[i][j-1] == '*'){
        DP[j] = DP[j-1] + x;
      }
      //sono white e il mio precedente e' white
      else{
        DP[j] = min(DP[j-2]+y,DP[j-1]+x);
      }
    }
    sum = sum + DP[m-1];
  }
  return sum;
}

int solve_greedy(int n, int m, int x,int y){
  //usare la Dp per questo problema iff il bazuka e la zanzara
  //basta notare che ci sono due casi 
  //se 2x <= y allora non piazzero' mai caselline 1x2 e la mia soluzione sara' conveniente
  //se 2x > y mi conviene sempre piazzare caselline da 2 quando e' possibile

  //nel caso 2x <= y conto tutte le caselle bianche e poi le moltiplico per x
  //nel caso 2x > y trovo la caselle adiacenti ed appena ne conto 2 adiacenti le copro con una y
  //per ogni conformazione di caselle bianche adiacenti abbiamo una ed una sola conformazione ottimale
  //data dalle caselle da 2
  int whites = 0;
  int sum = 0;
  if( 2*x <= y){
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++){
        if(A[i][j] == '.'){
          whites++;
        }
      }
    }
    sum = whites * x;
  }
  else{ //2x > y (conviene usare y) 
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++){
        if(A[i][j] == '.'){
          whites++;
        }
        else{
          sum += ((whites/2)*y) + (x*(whites%2));
          whites = 0;
        }
      }
      //svuotamento ultimi white
      sum += ((whites/2)*y) + (x*(whites%2));
      whites = 0;
    }
  }
  return sum;
}

//operazioni bitwise
int solve_greedy_bitwise(int n, int m, int x,int y){
  int whites = 0;
  int sum = 0;
  if( (x<<1) <= y){
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++){
        if(A[i][j] == '.'){
          whites++;
        }
      }
    }
    sum = whites * x;
  }
  else{ //2x > y (conviene usare y) 
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++){
        if(A[i][j] == '.'){
          whites++;
        }
        else{
          sum += ((whites >> 1)*y) + (x*(whites%2));
          whites = 0;
        }
      }
      //svuotamento ultimi white
      sum += ((whites >> 1)*y) + (x*(whites%2));
      whites = 0;
    }
  }
  return sum;
}



int main(void){
  //LEMMA1. la prima considerazione arriva dalla seguente proposizione siano R1,R2,...,Rn le n righe
  //min(R1,R2,R3,...,Rn) = min(R1) + min(R2) + min(R3) + ... + min(Rn)
  //ipotizziamo per assurdo che il min(R1,R2,R3,...,Rn) >= min(R1) + min(R2) + min(R3) + ... + min(Rn)
  // => esiste una conformazione di caselle che applicate a tutte le righe e' meno costosa della conformazione
  // minima tra tutte le righe, ma le righe non hanno alcuna tessera in comune quindi essa non puo' esistere
  int t;
  scanf("%d",&t);
  int n,m;
  int x,y;
  while(t > 0){
    scanf("%d %d %d %d",&n,&m,&x,&y);
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++){
        scanf(" %c",&(A[i][j]));
      }
    }
    printf("%d\n",solve_greedy_bitwise(n,m,x,y));
    t--;
  }
  return 0;
}

