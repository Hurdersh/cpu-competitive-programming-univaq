/*
 *  Author: Giordano Colli
 *  Date: 24-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Math Deduction
 *  Problem name: Computer Game
 *  Link Problem: https://codeforces.com/problemset/problem/1598/A
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_ROW 100

int main(void){
  /*
   * la soluzione si basa sulla seguente considerazione
   * posso sempre passare da una colonna ad un altra a patto
   * che ci sia almeno uno 0 nella colonna successiva.
   * il che implica che se in una colonna ci sono 2 uni e' 
   * l'unico caso in cui non riesco ad arrivare a termine
   * 
   */
  int k;
  int n;
  int i;
  char first_row[MAX_ROW];
  char second_row[MAX_ROW];
  scanf("%d",&k);
  for(int j;j<k;j++){
    scanf("%d",&n);
    scanf("%s",first_row);
    scanf("%s",second_row);
    i = 0;
    //vado avanti fino a che non incontro una colonna di uni
    while(i < n && !(first_row[i] == '1' && second_row[i] == '1') ){
      i++;
    }
    //se non ho mai incontrato suddetta colonna printo yes,altrimenti non posso arrivare
    if(i >= n){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
  }
  return 0;
}

