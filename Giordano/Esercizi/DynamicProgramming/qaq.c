/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: High(800 fake)
 *  Algorithm Or Idea used: Arte e dp
 *  Problem name: QAQ
 *  Link Problem: https://codeforces.com/problemset/problem/894/A
 *
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 100


int solution(char A[],int n){
  //la soluzione naive e' la seguente, immagina di costruire un grafo orientato
  //con le lettere della stringa, gli archi vanno dalle Q alle A, e dalle A alle Q e sono tutti 
  //da sinistra a destra -> il grafo e' ordinato topologicamente
  //ora si immagini di considerare tutti i "cammini" lunghi esattamente 2 archi che finiscono in una Q,
  //Per ogni Q partendo dalla fine si puo' passare per ogni A nel mezzo, e per ognuna di queste A si puo' scegliere
  //una Q che precede tale A 
  //PER ESEMPIO QAQAQ, decido che il mio cammino deve finire sull'ultima Q, quindi necessariamente devo
  //passare per una delle due A, per esempio se scelgo la primissima A posso partire solo dalla primissima Q,
  //non dalla seconda Q => ipotesi di ordinamento.
  //L'idea di algoritmo quindi e' eliminare le Q dalla fine progressivamente e sommare il numero di percorsi
  //che finiscono in Q, per ogni Q.
  //pari quindi alla somma di Q che precedono le A, a loro volta precedenti le Q.
  //
  //PER ESEMPIO QAQAQ => elimino la Q piu' a destra e scorro all'indietro il vettore, noto
  //chi sono le A e conto per ogni A i percorsi possibili,cioe' le Q che stanno piu' a sinistra di queste A

  //per generare le Q precedenti alle A basta utilizzare una struttura di appoggio ed incrementare di 1
  //ogni volta che incontro una Q
  //ESEMPIO QAQQAQ [1,1,2,3,3,4], ovviamente bisogna guardare solo le locazioni corrispondenti alle A,
  //infatti la prima A ha 1 Q che la precede e la 2 ne ha 3
  int* QprevA = malloc(sizeof(int) * n);
  int* sumQprevA = malloc(sizeof(int) * n);
  int tot = 0;
  //caso base, devo basarmi sul precedente, si inizia sempre con una Q
  QprevA[0]=1;
  //genero quindi le Q che precedono le A
  for(int i = 1;i<n;i++){
    if(A[i] == 'Q'){
      QprevA[i] = QprevA[i-1]+1;
    }
    else{
      QprevA[i] = QprevA[i-1];
    }
  }
  //adesso mantengo le somme parziali delle Q che precedono le A [che precedono una certa Q] 
  //in modo molto semplice
  //la prima Q banalmente ha 0 A che la precedono, non posso formare QAQ con la prima Q,non 
  //ho altre Q per iniziare il tutto
  //dopodiche' se trovo una A,so quante Q la precedono,sommo queste Q con le precedenti
  //ESEMPIO QAQQAQ la prima A e' preceduta da 1 q [0,1]
  //la seconda A e' preceduta da 2 Q + 1 che era la preceduta della prima A [0,1,3] and so on
  //QUI UTILIZZIAMO LA PROGRAMMAZIONE DINAMICA,perche' andare a riguardare ogni volta
  //quante Q ho all'indietro se gia' conosco questo numero?
  sumQprevA[0] = 0;
  for(int i=1;i<n;i++){
    //se sono una A,vedo da quante Q sono preceduta e le sommo
    //per ottenere ricordo tutti i path, quindi la somma delle q che 
    //precedeno la prima A + la seconda + la terza etc
    if(A[i] == 'A'){
      sumQprevA[i] = sumQprevA[i-1] + QprevA[i];
    }
    else{
      sumQprevA[i] = sumQprevA[i-1];
    }
  }
  //generato finalmente questo vettore posso scorrere all'indietro il vettore
  //e se sono una Q aggiungere tutti i percorsi(Q che precedono le a precedenti la Q selezionata)
  //per trovare il numero totale desiderato
  for(int i=n-1;i>=0;i--){
    if(A[i] == 'Q'){
      tot += sumQprevA[i];
    }
  }
  free(QprevA);
  free(sumQprevA);
  return tot;

}

int main(void){
  //la prima considerazione da fare e' che non necessito neanche di conoscere
  //tutti i caratteri diversi da {P,Q}, quindi per acquisire l'input utilizzo un vettore dinamico
  //e salvo solo le p e le q
  //inoltre tutti i caratteri presi in input prima della prima Q non andranno mai a formare la stringa
  //QAQ, perche' non si hanno altre Q antecedenti
  char A[MAX_SIZE];
  int n = 0;
  char ch;
  //scarto tutti i caratteri non Q iniziali
  while((ch = getchar()) && ch != '\n' && ch != EOF && ch != 'Q'){}
  if(ch == 'Q'){
    A[n] = ch;
    n++;
  }
  //prendo input
  while((ch = getchar()) && ch != '\n' && ch != EOF){
    if(ch == 'Q' || ch == 'A'){
      A[n] = ch;
      n++;
    }
  }
  //stampa debug
  /*for(int i=0;i<n;i++){
    printf("%c ",A[i]);
  }
  printf("\n");*/
  printf("%d\n",solution(A,n));
  return 0;
}

