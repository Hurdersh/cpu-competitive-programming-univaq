/*
 *  Author: Giordano Colli
 *  Date: 30-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Optimal Substructure Principle and DP
 *  Credits: Me,Myself and I
 *  Problem name: Boredom
 *  Link Problem: https://codeforces.com/problemset/problem/455/A
 *  Time Complexity: O(nlogn)
 *
 */


#include<iostream>
#include<algorithm>

//dimensione dell'array massima + 1
#define MAX 100001
using namespace std;

int main( void ){
  long long N;
  long long  a[MAX];
  long long  v[MAX];
  /*LEMMA: se prendo un elemento => conviene prendere tutti gli elementi dello stesso valore
   * la dimostrazione e' evidente, non si perde niente ad inserire l'elemento dello stesso valore
   * dato che si sono gia' esclusi gli elementi a_k + 1 e a_k - 1
   * */
  //ragiono non su insiemi di elementi, ma se prendere i singoli elementi
  long long condensation[MAX];
  long long k = 0;
  cin >> N;
  for(int i=0;i<N;i++){
    cin >> a[i];
    //per ogni elemento mi segno in una reference diretta quante volte appare
    //si puo' fare in O(1) con un bucket o con un array stile 'integer sort'
    v[a[i]]++;
  }
  //sorto per l'ordine necessario ad indexare velocemente chi e' predecessore/successore di chi
  sort(a,a+N);

  //creo la condensazione, cioe' l'array che contiene tutti i numeri distinti tra loro
  condensation[k] = a[k];
  for(int i=1;i<N;i++){
    if(a[i] != condensation[k]){
      condensation[k+1] = a[i];
      k++;
    }
  }
  /* FORMULAZIONE DP
   *  
   *
   * DP[k] = max punteggio che posso ottenere analizzando i primi k elementi
   *
   * sottostruttura ottima data dal fatto che tengo il kesimo elemento o non lo tengo
   * se il kesimo elemento non comporta svantaggi => condensation[k] != condensation[k-1]+1
   * prenderlo non fa mai diminuire il punteggio e mi riconduco ad un sottoproblema in cui 'incasso punti gratis'
   * DP[k] = DP[k-1] + elemento_kesimo * volte_elemento_kesimo
   *
   * se devo invece applicare una scelta cioe' o tengo elemento_kesimo o il k-1 => condensation[k] == condensation[k-1]
   * 
   * se non prendo l'elemento => analizzo il sottoproblema che contiene il k-1 elemento
   *
   * DP[k] = DP[k-1] se non tengo il kesimo elemento
   *
   * se prendo l'elemento => analizzo il sottoproblema che NON contiene il k-1 elemento => parte dal k-2 esimo
   * DP[k] = DP[k-2] + elemento_kesimo * volte_elemento_kesimo
   *
   * e svolgo il MAX tra i 2 ovviamente
   *
   * 
   * */
  long long DP[k+2];
  DP[0] = 0;
  DP[1] = condensation[0] * v[condensation[0]];
  for(int i=2;i<k+2;i++){
    if(condensation[i-1] != condensation[i-2]+1){
      DP[i] = DP[i-1] + condensation[i-1] * v[condensation[i-1]];
    }
    else{
      DP[i] = max(DP[i-1],DP[i-2] + condensation[i-1] * v[condensation[i-1]]);
    }
  }
  cout << DP[k+1] << endl;
  
  return 0;
}
