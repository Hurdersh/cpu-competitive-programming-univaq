/*
 *  Author: Giordano Colli
 *  Date: 25-09-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Greedy
 *  Problem name: Even Subset Sum
 *  Link Problem: https://codeforces.com/problemset/problem/1323/A
 *  Time Complexity: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#define ARR_SIZE 100

int main(void){
  int k;
  int n;
  int j;
  int dispari[2];
  int disparicount;
  int paricount;
  int pari;
  int a[ARR_SIZE];
  scanf("%d",&k);
  for(int h=0;h<k;h++){
    dispari[0]= -1;
    dispari[1]= -1;
    pari = -1;
    paricount = 0;
    disparicount = 0;
    scanf("%d",&n);
    for(int i=0;i<n;i++){
      scanf("%d",a+i);
    }
    //vado avanti fino a che trovo 1 pari o 2 dispari e me li segno
    j = 0;
    while(j< n && paricount < 1 && disparicount < 2){
      if(a[j]%2 == 0){
        pari = j;
        paricount++;
      }
      else{
        dispari[disparicount] = j;
        disparicount++;
      }
      j++;
    }

    if(paricount == 1){
      printf("1\n");
      printf("%d\n",pari+1);
    }
    else if(disparicount == 2){
      printf("2\n");
      printf("%d %d\n",dispari[0]+1,dispari[1]+1);
    }
    else{
      printf("-1\n");
    }
  }
  return 0;
}

