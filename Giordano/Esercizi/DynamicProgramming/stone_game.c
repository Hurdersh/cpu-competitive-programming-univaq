/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Greedy and case analysis
 *  Problem name: Stone Game
 *  Link Problem: https://codeforces.com/problemset/problem/1538/A
 *
 */

#include <stdio.h>
#include <stdlib.h>
int minv(int a,int b){
  if(a < b)
    return a;
  return b;
}

int main(void){
  int k,n,min,max,indexl,indexr,temp;
  int* A;
  scanf("%d",&k);
  for(int h=0;h<k;h++){
    scanf("%d",&n);
    A = malloc(sizeof(int) * n);
    indexl = 0;
    indexr = n-1;
    //input separated for clearness
    for(int i=0;i<n;i++){
      scanf("%d",A+i);
    }
    min = A[0];
    max = A[0];
    indexl = 0;
    indexr = 0;

    for(int i=1;i<n;i++){
      if(A[i] > max){
        max = A[i];
        indexl = i;
      }
      else if(A[i] < min){
        min = A[i];
        indexr = i;
      }
    }
    if(indexl >= indexr){
      temp = indexr;
      indexr = indexl;
      indexl = temp;
    }
    //l'idea e' che gli unici 3 casi sono: 
    //1 tolgo a partire dal lato destro
    //2 tolgo a partire dal lato sinistro
    //3 tolgo a partire dai lati
    //e non ci sono fortunatamente altri casi
    //nel caso 1 togliendo dal lato destro conto tutte le caselle fino ad indexl
    //nel caso 2 togliendo dal lato sinistro conto tutte le caselle fino ad indexr
    //nel primo caso tolgo da destra fino ad indexr e da sinistra fino ad indexl
    //fare i conti e' molto semplice e sono senz'altro ottimizzabili questi conticini,
    //inserisco la printf con i conti ottimizzati :)
    printf("%d\n",minv( minv(indexl+(n-indexr-1)+2, indexl + (indexr-indexl-1) + 2) , 
          (n-indexr-1)+(indexr-indexl-1)+2));
    //printf("%d\n",minv( minv(n+indexl-indexr+1,indexr+1),n-indexl));
    free(A);
  }
  return 0;
}

