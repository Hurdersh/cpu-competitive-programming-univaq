/*
 *  Author: Giordano Colli
 *  Date: 25-10-2022
 *  Language: C
 *  Platform: CodeForces
 *  Difficult: 800
 *  Algorithm Or Idea used: maximum non-decreasing subarray
 *  Problem name: Kefa and First Steps
 *  Link Problem: https://codeforces.com/problemset/problem/580/A
 *  Time Complexity: O(n)
 *
 */

#define MAX 100000
#include <stdio.h>
int main(void){
  //c'e' sempre un elemento che forma un maximum subarray da se
  int A[MAX];
  int max = 1;
  int counter = 1;
  int n;
  scanf("%d",&n);
  for(int i=0;i<n;i++){
    scanf("%d",A+i);
  }
  for(int i=0;i<(n-1);i++){
    if(A[i] <= A[i+1]){
      counter++;
    }
    else{
      if(counter > max){
        max = counter;
      }
      counter = 1;
    }
  }
  if(counter > max){
    max = counter;
  }
  printf("%d\n",max);
  return 0;
}

