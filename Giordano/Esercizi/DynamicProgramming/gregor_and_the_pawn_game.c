/*
 *  Author: Giordano Colli
 *  Date: 22-07-2022
 *  Language: C
 *  Platform: Codeforces 
 *  Difficult: 800
 *  Algorithm Or Idea used: Matroids and Greedy
 *  Credits: Rado Theorem
 *  Problem name: Gregor and the Pawn Game
 *  Link Problem: https://codeforces.com/problemset/problem/1549/B
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#define maxn 200000

/*
 * La soluzione si basa sul fatto che gli insiemi massimali di coppie selezionate
 * ha sempre la stessa taglia, e si dimostra notando che questo insieme e' un
 * MATROIDE, pertanto l'algoritmo greedy trova sempre una soluzione ottima
 * in questo caso massima == massimale.
 * la dimostrazione e' molto semplice, sia a=(a_1,a_2,a_3,...,a_n) un insieme soluzione
 * cioe' ogni a_i e' associato UNIVOCAMENTE ad un elemento x_i-1,x_i,x_i+1 ove possibile
 * e supponiamo b=(b_1,b_2,...,b_n,b_n+1) anch'essa una soluzione
 * DIMOSTRIAMO LA VALIDITA' DELLA PROPRIETA' DI SCAMBIO
 * quindi deve esistere un b_j in b tale per cui a U {b_j} sia ammissibile
 * supponiamo PER ASSURDO non sia cosi' => ogni b_i e' associato ad un elemento UNIVOCAMENTE,
 * che deve essere lo stesso di un certo a_h, allora riordiniamo e supponiamo ci sia una coincidenza tra
 * a_i e b_i senza perdita di generalita'. a questo punto b_n+1 non coincide con nessun x che sia stato 
 * associato a nessun'altra b e quindi a nessun'altra a, ASSURDO.
 *
 * in particolare la struttura del problema e' assimilabile a quella di un MATROIDE MATCHING
 *
 */

int solve(char* my,char* yours,int n){
  int counter = 0;
  for(int i=0;i<n;i++){
    if(my[i] == '1'){
      if(yours[i] == '0'){
        yours[i] = 'x';
        counter++;
      }
      else if(i-1 >=0 && yours[i-1] == '1'){
        yours[i-1] = 'x';
        counter++;
      }
      else if(i+1 < n && yours[i+1] == '1'){
        yours[i+1] = 'x';
        counter++;
      }
    }
  }
  return counter;
}


int main(void){
  int k,n;
  char yours[maxn];
  char my[maxn];
  scanf("%d",&k);
  for(int h;h<k;h++){
    scanf("%d",&n);
    scanf("%s",yours);
    scanf("%s",my);
    printf("%d\n", solve(my,yours,n));
  }
  return 0;
}

