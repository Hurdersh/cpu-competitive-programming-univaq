/*
 *  Author: Giordano Colli
 *  Date: 09-01-2023
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: DP
 *  Credits: Me
 *  Problem name: Mortal Kombat Tower
 *  Link Problem: https://codeforces.com/problemset/problem/1418/C
 *  Time Complexity: O(n)
 *
 */

#include<stdio.h>
#define MAXN 200000
#define A 0
#define I 1

int min(int a,int b){
  if (a < b)
    return a;
  return b;
}

/*FORMULAZIONE DP 
 *
 * reverso l'array in modo da pensarla top-down (di piu' semplice comprensione)
 * DP[i][A] = numero minimo di scarti che posso fare se affronto il sottoproblema iesimo essendo l'amico
 * DP[i][I] = numero minimo di scarti che posso fare se affronto il sottoproblema iesimo essendo io
 * 
 *
 * e' facile notare che DP[i][I] e' il minimo tra sconfiggere uno o due boss e passare la palla all'amico
 * DP[i][I] = min(DP[i-1][A], DP[i-2][A])
 *
 *
 * invece per DP[i][A] ci sono vari casi tra cui fare il minimo
 *  -incontro prima lo il numero a_n e poi il numero a_n-1  (l'array e' reversato)
 *  a[n] = 0 e a[n-1]=0 min(DP[n-1][I],DP[n-2][I])  (uccido un boss o entrambi e passo il turno)
 *  a[n] = 0 e a[n-1]=1 min(1+DP[n-2][I],DP[n-1][I]) (uccido il boss zeresimo oppure uccido entrambi e pago 1)
 *  a[n] = 1 e a[n-1]=0 min(DP[n-2][I]+1,1+DP[n-1][I]) (pago 1 e uccido entrambi i boss o pago 1 uccidendo il primo boss e passo il turno)
 *  a[n] = 1 e a[n-1]=1 min(2 + DP[n-2][I], DP[n-1][I] + 1) (pago 2 e uccido entrambi i boss o pago 1 e uccido solo un boss)
 *
 */

int main(void){

  int t;
  int n;
  int a[MAXN];
  int DP[MAXN][2];
  scanf("%d",&t);
  while(t--){
    scanf("%d",&n);
    for(int i=0;i<n;i++){
      scanf("%d",&(a[n-i-1]));
    }
    //caso base da 1 elemento
    DP[0][I] = 0;
    DP[1][I] = 0;
    DP[0][A] = a[0];
    if(n == 1){
      printf("%d\n",DP[0][A]);
    }
    else{
      //caso base considerevole
      if(a[1] == 0){
        DP[1][A] = 0;
      }
      else{
        DP[1][A] = 1;
      }
      //formulazione DP vera e propria
      for(int i=2;i<n;i++){
        if(a[i] == 0){
          if(a[i-1] == 0){ //caso 00
            DP[i][A] = min(DP[i-1][I],DP[i-2][I]);
          }
          else{ //caso 01 
            DP[i][A] = min(DP[i-2][I]+1,DP[i-1][I]);
          }
        }
        else{
          if(a[i-1] == 0){ //caspo 10
            DP[i][A] = min(DP[i-2][I]+1,1+DP[i-1][I]);
          }
          else{ //caso 11
            DP[i][A] = min(DP[i-2][I]+2,DP[i-1][I]+1);
          }
        }
        DP[i][I] = min(DP[i-1][A], DP[i-2][A]);
      }
      printf("%d\n",DP[n-1][A]);
      //scrivi fino a qui
    }

  }
  return 0;
}
