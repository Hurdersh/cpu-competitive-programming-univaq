/*
 *  Author: Giordano Colli
 *  Date: 20-07-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Greedy 
 *  Problem name: Last Minute Enhancements
 *  Link Problem: https://codeforces.com/problemset/problem/1466/B
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
  int k,n,last,div;
  int* A;
  scanf("%d",&k);
  for(int h=0;h<k;h++){
    scanf("%d",&n);
    A = malloc(sizeof(int) * n);
    //input separated for clearness
    for(int i=0;i<n;i++){
      scanf("%d",A+i);
    }
    div = 1;
    last= -1;
    //l'algoritmo deriva dal fatto che l'array mi viene fornito ordinato
    //o cerco di fare un incremento gia' fatto nel passo precedente oppure nessuno arriva
    //ad avere la mia taglia, il 3 if serve solo per dire di incrementare uno stesso numero una volta
    //sola e tenere traccia di chi ho incrementato precedentemente
    for(int i=1;i<n;i++){
      if(A[i] == last+1){
        last = A[i];
        div++;
      }
      else if(A[i] > A[i-1]){
        div++;
      }
      else if(A[i] == A[i-1]){ 
        //se sono uguale al precedente e non sono stato ottenuto da incrementi precedenti => 
        //posso incrementare il mio valore
        //contando una diversita' in piu'
        if(A[i] != last){
          if(A[i] > last+1){
            //se ho ottenuto gia' il valore in questione => non lo conto come diversita'
            div++;
          }
          last = A[i];
        }
      }
    }
    printf("%d\n",div);
    free(A);
  }
  return 0;
}

