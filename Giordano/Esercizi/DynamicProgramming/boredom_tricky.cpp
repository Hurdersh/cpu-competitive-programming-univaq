/*
 *  Author: Giordano Colli
 *  Date: 30-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Optimal Substructure Principle and DP
 *  Credits: Me,Myself and I
 *  Problem name: Boredom
 *  Link Problem: https://codeforces.com/problemset/problem/455/A
 *  Time Complexity: O(n)
 *
 */


#include<iostream>
#include<algorithm>

//dimensione dell'array massima + 1
#define MAX 100002
using namespace std;

int main( void ){
  long long N;
  long long  a[MAX];
  long long temp;

  for(int i=0;i<MAX;i++){
    a[i]=0;
  }
  /*LEMMA: se prendo un elemento => conviene prendere tutti gli elementi dello stesso valore
   * la dimostrazione e' evidente, non si perde niente ad inserire l'elemento dello stesso valore
   * dato che si sono gia' esclusi gli elementi a_k + 1 e a_k - 1
   * */
  //ragiono non su insiemi di elementi, ma se prendere i singoli elementi
  long long k = 0;
  cin >> N;
  for(int i=0;i<N;i++){
    cin >> temp;
    a[temp]+=temp;
  }
  //faccio una cosa stupida al livello algoritmico, ma sensata a fini pratici. SO che i numeri sono 
  //al piu' 100000 => uso un array DP lungo esattamente 10000 in cui valuto sempre il caso 'cattivo'
  //e' illogico ma non avendo bisogno del sorting "ottimizzo", in realta' non e' proprio cosi'
  //dato che vado ad eseguire sempre 10000 operazioni, anche per 3 numeri quindi non riesco a valutare
  //in funzione dell'input => Theta(10000) ma siccome n <= 10000 => O(n)
  //WARNING => POSSO FARLO SE E SOLO SE I NUMERI ONO COMPRESI TRA UN CERTO NUMERO ED UN ALTRO
  for(int i=2;i<=100001;i++){ 
    a[i]=max(a[i-1], a[i]+a[i-2]);
  }
  cout<<a[MAX-1];
  
  return 0;
}
