/*
 *  Author: Giordano Colli
 *  Date: 28-07-2022
 *  Language: C
 *  Problem name: Deadly Laser
 *  Link Problem: https://codeforces.com/problemset/problem/1721/B
 *  Platform: Codeforces
 *  Execution Time: 
 */

#include<stdio.h>
#include<stdlib.h>
#define MAX_PATH 1000000

int n,m,sx,sy,d;

int abs(int a){
  if(a >= 0)
    return a;
  return -1 * a;
}
int legal(int x,int y){
  return x >= 0 && x <= n-1 && y >=0 && y<=m-1;
}

int not_lasered(int x, int y){
  return abs(sx - x) + abs(sy - y) > d;
}

int main( void ){
  int t;
  int lasered1;
  int lasered2;
  int j;
  int i;
  scanf("%d",&t);
  for(int k=0;k<t;k++){
    scanf("%d %d %d %d %d",&n,&m,&sx,&sy,&d);
    sx--;
    sy--;
    if(not_lasered(n-1,m-1)){
    //controllo se il laser tocca la mia cella(n-1,m-1)
      lasered1 = 0;
      lasered2 = 0;
      //se la prima riga e l'ultima colonna non sono state laserate ci metto tempo minimo
      //cioe' (n-1) + (m-1)
      j=0;
      while(j<m && not_lasered(0,j)){
        j++;
      }
      if(j < m){
        lasered1 = 1; 
      }
      i=0;
      while(i<n && not_lasered(i,m-1)){
        i++;
      }
      if(i < n){
        lasered1 = 1;
      }

      //se la prima colonna e l'ultima riga non sono state laserate ci metto tempo minimo cioe' (n-1) + (m-1)
      j=0;
      while(j<m && not_lasered(n-1,j)){
        j++;
      }
      if(j < m){
        lasered2 = 1; 
      }
      i=0;
      while(i<n && not_lasered(i,0)){
        i++;
      }
      if(i < n){
        lasered2 = 1;
      }
      if(lasered1 && lasered2){
        printf("-1\n");
      }
      else{
        printf("%d\n",n-1+m-1);
      }
      
      //se sono state laserate contemporaneamente
      //1. prima riga e prima colonna => il passaggio e' ostruito 
      //2. prima riga e ultima riga => il passaggio e' stato ostruito
      //3. prima colonna e ultima colonna => il passaggio e' stato ostruito
      //4. prima colonna e ultima riga => il passaggio e' stato ostruito

    }
    else{
      printf("-1\n");
    }
  }
  return 0;
}


