#include <iostream>
#include <cmath>

using namespace std;
//l'idea di base e':o passo da sopra o passo da sotto, se non posso fare nessuno dei due e' perche' non posso arrivaci

int main() {  
int s;
cin>>s;
int S[s];
for(int p=0; p<s; p++){
    int n,m,sx,sy,d;
    cin>>n;
    cin>>m;
    cin>>sx;
    cin>>sy;
    cin>>d;
    bool pErr= false;
    int passiX=0;
    int passiY=0;
    while(passiX<n-1 && !pErr){ //mi facio tutte le colonne
        if ((abs((passiX+2)-sx))+(abs((passiY+1)-sy))<=d)pErr=true;//controllo se posso arivare alla casella a destra
        else passiX++;
    }
    while(passiY<m-1 && !pErr){//inizio la discesa
        if ((abs(sx-n))+(abs((passiY+2)-sy))<=d)pErr=true;//controll se posso andare alla casella sotto
        else passiY++;
    }
    
    //se la variabile booleana e'true significa che la prossima casella sarebbe stata fatale quindi provo lo stesso persors ma partendo dalla discesa e dopo arrivare al punto
    if(pErr==true){
        pErr= false;
        passiX=0;
        passiY=0;
        while(passiY<m-1 && !pErr){
            if ((abs(sx-(passiX+1)))+(abs((passiY+2)-sy))<=d)pErr=true;//controllo se posso scendere
            else passiY++;
        }
        while(passiX<n-1 && !pErr){
            if ((abs((passiX+2)-sx))+(abs(m-sy))<=d)pErr=true;//controllo se posso andare a destra
            else passiX++;
        }
        
    }
    if(pErr) S[p]=-1;
    else S[p]=(passiX+passiY);
  }
  for(int stp=0;stp<s;stp++){
      cout<<S[stp]<<endl;
  }
}