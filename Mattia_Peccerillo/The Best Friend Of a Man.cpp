/*
 *  Author: Mattia Peccerillo
 *  Date: 7-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Salvo il numero di uscite di un determinato giorno in un array, per poi scorrere e verificare che la somma tra
 *							la posizione precedente dell'array e quella corrente sia almeno k, altrimenti modifico la posizione corrente e 
 *							tengo traccia delle uscite che aggiungo
 *  Credits: Me,Myself and I
 *  Problem name: Filling Shapes
 *  Link Problem: https://codeforces.com/problemset/problem/732/B
 *  Time Complexity: O(n)
 *
 */
#include <iostream>
using namespace std;

int main(){
	
	//input
	int n,k;

	cin >> n >> k;
	
	int walks[n];
	int addWalks = 0;
	
	for(int i = 0; i < n; i++)
		cin >> walks[i];
	
	for(int i = 1; i < n; i++){
		
		if(walks[i-1] + walks[i] < k){
			addWalks += k - (walks[i-1] + walks[i]);
			walks[i] += k - (walks[i-1] + walks[i]);
		}
	}
	
	cout << addWalks << endl;
	
	for(int i = 0; i < n; i++)
		cout << walks[i] << " ";
	
	return 0;
}
