/*
 *  Author: Mattia Peccerillo
 *  Date: 7-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Se il numero di colonne non � pari sono sicuro che non ci sono modi per riempire la figura, altrimenti se � pari
 *							vedo posso posizionare 2 figure in 2 colonne in 2 modi, quindi ho 2^(n/2) modi per posizionare le figure
 *  Credits: Me,Myself and I
 *  Problem name: Filling Shapes
 *  Link Problem: https://codeforces.com/problemset/problem/1182/A
 *  Time Complexity: O(n/2)
 *
 */
#include <iostream>
using namespace std;

int main(){
	
	//input
	int n;
	
	cin >> n;
	
	int result = 1;
	
	if(n%2 == 0){
	
		for(int i = 0; i < n/2; i++){
			result *= 2;
		}
		cout << result;
	}
	else
		cout << 0;
	
	return 0;
}
