/*
 *  Author: Mattia Peccerillo
 *  Date: 15-11-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Passare per i bordi della griglia e vedere se il percorso � bloccato dal laser. Provare prima scorrendo colonne e dopo righe, poi il viceversa
 *  Credits: Me,Myself and I
 *  Problem name: Deadly Laser
 *  Link Problem: https://codeforces.com/problemset/problem/1721/B
 *  Time Complexity: O(n*m)
 *
 */
 
 #include <iostream>
using namespace std;

int assoluto(int x){
	if(x < 0){
		x *= -1;
	}
	return x;
}


int main(){
	
	//input
	int tst;
	int row;
	int col;
	int xlaser;
	int ylaser;
	int d;
	
	cin >> tst;
	
	int result[tst];
	int i = 0;
	int x = 1;
	int y = 1;
	bool valid;
	
	while(i < tst){
		
		valid = true;
		
		cin >> row;
		cin >> col;
		cin >> xlaser;
		cin >> ylaser;
		cin >> d;
		
		x = 1;
		y = 1;
		
		//prima orizzontale poi verticale
		//***
		while(y < col && valid){
					
			y++;
			if((assoluto(xlaser - x) + assoluto(ylaser - y) )<= d){
				valid = false;
			}	
		}
		
		if(valid){
			while(x < row && valid){
				x++;
				if((assoluto(xlaser - x) + assoluto(ylaser - y))<= d){
					valid = false;
				}
			}
		}
		//***
		if(valid){
			result[i] = row + col - 2;
		}
		//prima verticale poi orizzontale
		//***
		else{
			valid = true;
			x = y = 1;
			while(x < row && valid){
				x++;
				if((assoluto(xlaser - x) + assoluto(ylaser - y)) <= d){
					valid = false;
				}
			}
			if(valid){
				while(y < col && valid){	
					y++;		
					if((assoluto(xlaser - x) + assoluto(ylaser - y) )<= d){
						valid = false;
					}
				}
			}
			if(valid){
				result[i] = row + col - 2;
			}else{
				result[i] = -1;
			}
		}
		i++;
	}
	
	for(i = 0; i < tst; i++){
		cout << result[i] << endl;
	}
	
	return 0;
}



