/*
 *  Author: Mattia Peccerillo
 *  Date: 13-11-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1000
 *  Algorithm Or Idea used: Verifico se conviene inserire le caselle 1x2 rispetto a 2 1x1, in tal caso
 *							controllo se ci sono 2 punti uno dietro l'altro e aggiorno il risultato; altrimenti
 *							scorro semplicemente i n*m caratteri in arrivo (senza tener conto della distinzione tra
 *							righe e colonne) e aggiungo al risultato il costo di una casella 1x1 ogni
 *							volta che trovo un punto
 *  Credits: Me,Myself and I
 *  Problem name: New Theatre Square
 *  Link Problem: https://codeforces.com/problemset/problem/1359/B
 *  Time Complexity: O(n*m)
 *
 */

#include <iostream>
using namespace std;

int main(){
	
	//input
	int tst;	
	int row;
	int col;
	int cost_1x1;
	int cost_1x2;
	
	cin >> tst;
	
	char cur_tile;	//tiene traccia del colore della casella corrente
	char pre_tile;	//tiene traccia del colore della casella precedente
	
	int result[tst];
	
	for(int i = 0; i < tst; i++){
		
		result[i] = 0;	
		cin >> row;
		cin >> col;
		cin >> cost_1x1;
		cin >> cost_1x2;
		
		//controllo se conviene inserire le caselle 1x2
		if((2*cost_1x1) <= cost_1x2){
			
		//caso in cui non conviene: aggiungo il costo di una 1x1 per ogni punto che trovo
			for(int j = 0; j < row * col; j++){
				cin >> cur_tile;
				if(cur_tile == '.'){
					result[i] += cost_1x1;
				}
			}
		} 
		else{
			
		//caso in cui conviene: se posso inserire una 1x2, ne aggiungo il costo al risultato
		//altrimenti aggiungo quello di una 1x1
			for(int j = 0; j < row; j++){
				
				cin >> cur_tile;	//prendo il primo elemento di ogni riga per fare il confronto con la successiva
				
				for(int k = 1; k < col; k++){
					pre_tile = cur_tile;	//l'attuale del giro di prima � la precedente di questo giro
					cin >> cur_tile;
					
					if(pre_tile == '.'){	
					//se prima avevo un punto devo controllare se anche il carattere corrente � un punto
						if(cur_tile == '.'){
							//ho indivituato la sequnza punto punto, aggiungo una 1x2
							result[i] += cost_1x2;
							//cambio il simbolo attuale (cio� il prossimo pre_tile) cos� da non ricontarla come tile bianca
							cur_tile = '*';	
						} else{
							//ho individuato la sequenza punto asterisco, aggiungo una 1x1
							result[i] += cost_1x1;
						}			
					}	
				}
				
				//controllo sull'ultimo carattere della riga
				if(cur_tile == '.'){
					result[i] += cost_1x1;
				}
			}
		}
	
	}
	
	for(int i = 0; i < tst; i++){
		cout << result[i] << endl;
	}
	
	return 0;
}
