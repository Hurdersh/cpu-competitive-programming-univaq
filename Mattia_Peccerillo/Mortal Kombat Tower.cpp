/*
 *  Author: Mattia Peccerillo
 *  Date: 14-01-2023
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 1500
 *  Algorithm Or Idea used: Tengo traccia di quanti Skip Points ho bisogno per arrivare all'i-esimo boss dell'array a nell'i-esima posizione
 *							dell'array b. se a[i] = 0, b[i] � uguale al numero attuale di Skip Points; se a[i] = 1 e i precedenti due elementi
 *							di a sono 1 e i due precedenti elementi di b sono 0, b[i] = Skip Points + 1, ovvero se sono costretto a far 
 *							affrontare il boss all'amico perch� i precedenti 2 li ho affrontati io
 *  Credits: Me,Myself and I
 *  Problem name: Mortal Kombat Tower
 *  Link Problem: https://codeforces.com/problemset/problem/1418/C
 *  Time Complexity: O(n)
 *
 */
 
 #include <iostream>
using namespace std;

int main(){
	
	//input
	int t;
	cin >> t;
	int result[t];
	
	int c = 0;				//contatore per i test
	int skipPoints = 0;		//contatore dei Skip Points a ogni giro

	while(c < t){
		
		skipPoints = 0;
		
		//input
		int n;
		cin >> n;
		int a[n];
		
		//array b inizializzato a 0
		int b[n] = {0};	
		
		for(int i = 0; i < n; i++){
			cin >> a[i];
		}
		
		//caso base: dimensione di a == 1
		if((sizeof(a)/sizeof(int)) == 1){
			result[c] = a[0];
		}
		// dimensione di a > 1: inizializzo i primi due elementi di b
		else{
			skipPoints = a[0];
			b[0] = a[0];
			if(a[1] == 0)
				b[1] = skipPoints;
		
			//for principale		
			for(int i = 2; i < n;i++){
				//caso a[i] = 0: b[i] = skipPoints, nessuna variazione
				if(a[i] == 0)
					b[i] = skipPoints;
				//caso a[i] = 1: se i precendenti due elementi di a sono 1 e i precedenti due di b sono 0, b[i] = ++skipPoints, altrimenti
				//resta a 0
				else{
					if(b[i-2] == 0 && b[i-1] == 0 && a[i-2] == 1 && a[i-1] == 1){
						b[i] = ++skipPoints;
					}						
				}
			}
			result[c] = skipPoints;						
		}		
		c++;	
	}
	
	for(int i = 0; i < t; i++)
		cout << result[i] << "\n";
}



