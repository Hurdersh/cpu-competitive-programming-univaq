/*
 *  Author: Mattia Peccerillo
 *  Date: 13-11-2022
 *  Language: C
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Scorrere tutta la stringa in input e contare le occorrenze di Q e di Q seguite da A
 *  Credits: Me,Myself and I
 *  Problem name: QAQ
 *  Link Problem: https://codeforces.com/problemset/problem/894/A
 *  Time Complexity: O(n)
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(){
	
	char str[100];
	scanf ("%100s", str);
	
	int Qcounter = 0; //contatore delle Q che incontro
	int QAcounter = 0; // contatore delle sequnze QA che incontro
	int result = 0;
	
	int i = 0;
	
	//cerco una Q di partenza, finch� non ne trovo una non conto le A
	while(i < sizeof(str)/sizeof(char)){
		
		if(str[i] == 'Q'){
			Qcounter++;
			i++;
			break;
		}
		i++;
	}
	
	//analizzo carattere per carattere tutta la stringa e ogni volta che trovo:
	//	-	Q, aggiungo 1 a Qcounter e aggiungo il numero QAcounter al risultato, ovvero
	//		quante sequenze QAQ ho formato con la scoperta di questa Q; 
	//	-	A, aggiungo Qcounter a QAcounter, cio� quante sequenze QA ho formato con
	//		la scoperta di questa A.
	//	-	ne A ne Q, non faccio nulla
	
	for(;i < sizeof(str)/sizeof(char); i++){	
	
		if(str[i] == 'Q'){
			Qcounter++;
			result += QAcounter;
		}
		
		if(str[i] == 'A'){
			QAcounter += Qcounter;
		}
	}

	printf ("%d", result);
	
	return 0;
}
