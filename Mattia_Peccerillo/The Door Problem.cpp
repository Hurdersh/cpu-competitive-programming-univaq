/*
 *  Author: Mattia Peccerillo
 *  Date: 11-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 2000
 *  Algorithm Or Idea used: Ricondurmi ad un problema 2SAT e trovare le SCC con una singola DFS dato che il grafo risultante � sempre simmetrico
 *  Credits: Me,Myself and I
 *  Problem name: The Door Problem
 *  Link Problem: https://codeforces.com/problemset/problem/776/D
 *  Time Complexity: O(n+m)
 *
 */
#include <iostream>
#include <list>
#include <stack>
using namespace std;
 
class Door{				//memorizza lo stato della porta e i due switch al quale � connessa
	public:
		bool state; 	//la porta inizialmente � aperta o chiusa?
		int s1,s2;		//la porta � connessa a Switch1 e Switch2
		
		Door(){
			s1 = 0;
			s2 = 0;
		}
}; 
 
class Graph{
	int v;
	list<int> *adjList;
  	
	public:
		Graph(int v){
			adjList = new list<int>[v];
			this->v = v;
		}
		
		void AddEdge(int s, int d) {
  			adjList[s].push_back(d);
		}
		
		/*	questa DFS tiene traccia delle SCC dato che il grafo � simmetrico, quindi ogni nodo � SCC con ogni suo figlio che a sua volta �
		*	SCC con ogni suo figlio e cos� via
		*/
		void DFS(int s, int result[], int sccCounter){
			result[s] = sccCounter;
			list<int>::iterator i;
			for(i = adjList[s].begin(); i != adjList[s].end(); i++){
				if(result[*i] == 0)
					DFS(*i, result, sccCounter);
			}
		}
};
 
int main(){
	
	//input
	int nDoors, nSwitches;
	
	cin >> nDoors >> nSwitches;
	Door doors[nDoors];
		
	//prende in input gli stati iniziali delle porte
	for(int i = 0; i < nDoors; i++){
		cin >> doors[i].state;
	}
	
	//variabili di appoggio
	int s,d;
	
	//prende in input gli interruttori a cui sono collegate le porte
	for(int i = 0; i < nSwitches; i++){
		cin >> s;
		for(int j = 0; j < s; j++){
			cin >> d;
			if(doors[d-1].s1 == 0)
				doors[d-1].s1 = i+1;
			else
				doors[d-1].s2 = i+1;	
		}
	}	
    
    Graph G = Graph(nSwitches * 2);		
    
    /* 	Aggiunge gli archi al grafo in base allo stato iniziale della porta
    *
    * 	Se la porta � chiusa ho che i due switch associati alla porta sono in XOR tra loro
    *	forma canonica = (A OR B) AND (NOT A OR NOT B)
    *
    *	Se la porta � aperta ho che i due switch associati alla porta sono in NXOR tra loro
    *	forma canonica = (A OR NOT B) AND (NOT A OR B)
    *
	*	trasformo ogni clausola in implicazione logica e le aggiungo al grafo
    */
	for(int i = 0; i < nDoors; i++){
		if(doors[i].state == 0){	//se la porta inizialmente � chiusa
			G.AddEdge(doors[i].s1 + nSwitches - 1,doors[i].s2 - 1);
			G.AddEdge(doors[i].s2 - 1,doors[i].s1 + nSwitches - 1);
			G.AddEdge(doors[i].s1 - 1,doors[i].s2 + nSwitches - 1);
			G.AddEdge(doors[i].s2 + nSwitches - 1,doors[i].s1 - 1);
		} 
		if(doors[i].state == 1){	//se la porta inizialmente � aperta
			G.AddEdge(doors[i].s1 - 1,doors[i].s2 - 1);
			G.AddEdge(doors[i].s2 - 1,doors[i].s1 - 1);
			G.AddEdge(doors[i].s1 + nSwitches - 1,doors[i].s2 + nSwitches - 1);
			G.AddEdge(doors[i].s2 + nSwitches - 1,doors[i].s1 + nSwitches - 1);
		}
	}
 
    int *result = new int[nSwitches * 2];	//tiene traccia di quali nodi appartengono a quale SCC
	  	for (int i = 0; i < nSwitches * 2; i++)
    		result[i] = 0;
			   	
    int sccCounter = 0;				//tiene conto del numero di SCC trovate
    	
    //DFS
    for (int s = 0; s < nSwitches * 2; s++) {   	
    	if (result[s] == 0) { 			
 			sccCounter++;
     		G.DFS(s, result, sccCounter);     		
		}	
	}
	
	/*	controlla se un nodo fa parte della stessa SCC del suo negato (i negati si trovano nella seconda met� dell'array)
	*	in posizione a, il suo negato si trova in posizione a + (lunghezza array/2)
	*	in questo caso l'array contenente tutti i nodi � lungo nSwitches * 2, quindi lunghezza array / 2 � solo nSwitches
	*/	
	for(int i = 0; i < nSwitches; i++){
		if(result[i] == result[i + nSwitches]){
			cout << "NO";
			return 0;	
		}
	}	
	cout << "YES";
	return 0;
}
 
