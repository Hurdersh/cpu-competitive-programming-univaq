/*
 *  Author: Mattia Peccerillo
 *  Date: 6-12-2022
 *  Language: C++
 *  Platform: Codeforces
 *  Difficult: 800
 *  Algorithm Or Idea used: Salvare le chiavi dietro le porte in un array e vedere se con quella di partenza visito quella col valore 0 per ultima
 *  Credits: Me,Myself and I
 *  Problem name: Three Doors
 *  Link Problem: https://codeforces.com/problemset/problem/1709/A
 *  Time Complexity: O(n)
 *
 */
#include <iostream>
#include <string>
using namespace std;

int main(){
	
	//input
	int tst;
	int key;
	int doors[3];
	
	int k;
	
	cin >> tst;
	string result[tst];
	
	for(int i = 0; i < tst; i++){
		
		cin >> key;
		
		for(int j = 0; j < 3; j++){
			cin >> doors[j];
		}
			
		for(k = 0; k < 3; k++){
			if(doors[key - 1] == 0)
				break;
			else
				key = doors[key - 1];
		}
		
		if(k == 2)
			result[i] = "YES";
		else
			result[i] = "NO";
	}
	
	for(int i = 0; i < tst; i++){
		cout << result[i] << "\n";
	}
	
	return 0;
}
