#include <iostream>
using namespace std;
int main()
{
	int ntest;
	int n;//numero di righe della griglia
	int m;//numero di colonne della griglia
	int laserx;//coordinata x del laser 
	int lasery; //coordinata y del laser
	int d; //distanza del laser
	cin>>ntest;
	int min[ntest];
	for(int i=0;i<ntest;i++)
	{
		cin>>n;
		cin>>m;
		cin>>laserx;
		cin>>lasery;
		cin>>d;
		if(laserx+d>=n && laserx-d<=1)
		{
			min[i]=-1;
			continue;
		}
		if(lasery+d>=m && lasery-d<=1)
		{
			min[i]=-1;
			continue;
		}
		if(laserx+d>=n && lasery+d>=m)
		{
			min[i]=-1;
			continue;
		}
		if(laserx-d<=1 && lasery-d<=1)
		{
			min[i]=-1;
			continue;
		}
		min[i]=n+m-2;
	}
	for(int i=0;i<ntest;i++)
	{
		cout<< min[i]<<endl;
	}
	return 0;
}
