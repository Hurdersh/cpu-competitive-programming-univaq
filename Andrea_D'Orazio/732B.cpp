#include<iostream>
using namespace std;
int main()
{
	int n;//numero di giorni per cui bisognano programmare le passeggiate
	int k;// numero minimo di passeggiate per due giorni consecutivi
	cin>>n;
	cin>>k;
	int A[n];//array che contiene la programmazione iniziale 
	for(int i=0;i<n;i++)
	{
		cin>>A[i];
	}
	int x;//variabile d'appoggio
	int sum=0;//variabile che contiene il numero di passeggiate aggiunte rispetto alla programmazione iniziale
	for(int i=1;i<n;i++)//vado a scorrere l'array e partiamo da 1 visto che nel giorno 0 non vado ad aggiungere niente
	{
		//verifico se non viene rispettato il numero minimo di passeggiate confrontando k con la somma tra il giorno attuale e quello precedente
		if(k>A[i-1]+A[i])
		{
			//in caso positivo vado a vedere quanti giorni mancano e li vado ad aggiungere all'i-esimo giorno che per dimostraziione � la soluzione pi� efficente
			x=k-A[i-1]-A[i];
			sum+=x;
			A[i]+=x;
		}	
	}
	cout<<sum<<endl;
	for(int i=0;i<n;i++)
	{
		cout<<A[i]<<" ";
	}
	return 0;
}
