#include <iostream>
#include <string>
using namespace std;
int main()
{
	int numtest;//numero di test effettuati 
	cin>>numtest;
	int n;//numero di righe
	int m;//numero di colonne
	int c1;//costo di una mattonella
	int c2;//costo di 2 mattonelle
	int min[numtest];//array in cui andiamo a salvare i risultati di ogni test
	int sum;
	char c;
	for(int i=0;i<numtest;i++)
	{
		sum=0;
		cin>>n;
		cin>>m;
		cin>>c1;
		cin>>c2;
		if(c1*2<=c2)
		{
			for(int j=0;j<n;j++)
			{
				for(int k=0;k<m;k++)
				{
					cin>>c;
					if(c=='.')
					{
						sum+=c1;
					}
				}
			}
		}
		else
		{
			bool and1;
			for(int j=0;j<n;j++)
			{
				and1=false;
				for(int k=0;k<m;k++)
				{
					cin>>c;
					if(c=='.' && !and1)
					{
						sum=sum+c1;
						and1=true;
						continue;
					}
					if(c=='.' && and1)
					{
						sum=sum+c2-c1;
						and1=false;
						continue;
					}
					if(c=='*')
					{
						and1=false;
						continue;
					}
				}
			}
		}
		min[i]=sum;
	}
	for(int i=0;i<numtest;i++)
	{
		cout<<min[i]<<endl;
	}
}
