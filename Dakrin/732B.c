#include <stdio.h>

int main() {

	int n,k; 
	
	//Input
	scanf("%d",&n);
	scanf("%d", &k);
	int b[n];
	for(int i = 0; i < n; i++){
		scanf("%d", &b[i]);
	}

	int diff;
	int min = 0;
	
	//n-1 perchè l'ultimo elemento dell'array non ha un elemento + 1 (out of bound) 
	for(int i = 0; i < n-1; i++)
	{
		//Verifico se Soddisfo o Meno il fabisogno di Cormen
		diff = k - b[i] - b[i+1];
		//Se non è soddisfatto aggiungo al secondo elemento della coppia quello che manca
		if(diff > 0){
			min += diff;
			b[i+1]+= diff;
		}
	}

	printf("%d\n", min);
	for(int i = 0; i < n; i++){
		printf("%d ", b[i]);
	}

	return 0;
}
