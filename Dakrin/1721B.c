#include<stdio.h>

int main(){
	long long tc;		//Test Cases
	long long n,m;		//Board Dimensions
	long long sx,sy;	//Laser Position
	long long d;		//Beam length
	
	scanf("%lli", &tc);

	for(long long i = 0; i < tc; i++){
		scanf("%lli",&n);
		scanf("%lli",&m);
		scanf("%lli",&sx);
		scanf("%lli",&sy);
		scanf("%lli",&d);
		// We only need to check if there is an available path , if the
		// robot spawns inside the border of the laser beam or if the
		// cell <n,m> is inside the laser beam.
		if((sx+d >= n && sx-d <= 1) || (sy+d >= m && sy-d <= 1) || (n <= sx+d && m <= sy+d) || (1 >= sx-d && 1 >= sy-d)){
			printf("-1\n");
		} else {
			printf("%d\n", (n+m-2));
		}	
	}
	
	return 0;
}
