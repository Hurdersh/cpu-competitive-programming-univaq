#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;
vector<bool> visited;
vector <vector<int>> adj; // Graph
stack<int> postInsert;
vector <vector<int>> rg; //Reversed Graph
vector<int> scc;
int n;
int m;

void normalDfs(int v){
 visited[v] = true;
 for(int i : adj.at(v)){
  if(!visited[i]){
   normalDfs(i);
  }
 }
 //cout << v << " ";
 postInsert.push(v);
}

void reversedDfs(int v,int c){
 visited[v] = true;
 for (int i : rg.at(v)) {
  if(!visited[i]) {
   reversedDfs(i,c);
  }
 }
 scc[v] = c;
}


int main() {

 //nodes and clauses
 cin >> n;
 cin >> m; 


 scc.resize(2*m);
 adj.resize(m*2);
 rg.resize(m*2);

 for(int i = 0; i < m*2; i++){
  visited.push_back(false);
 }

 int r[n];
 vector<vector<int>> a(n);

 for(int i = 0; i < n; i++) {
  cin >> r[i];
 }

 int u;
 int temp;
 for(int i = 0; i < m; i++){
  cin >> u; 
  for(int k = 0; k < u; k++){
   cin >> temp;
   a[temp-1].push_back(i);
  }
 }


 for(int i = 0;i<n;i++){
   int p1 = a[i][0];
   int p2 = a[i][1];
   if(r[i] == 1){
     adj.at(p1).push_back(p2); // p1 => p2
     adj.at(p2+m).push_back(p1+m);  //not p2 => not p1
     adj.at(p2).push_back(p1); //p2 => p1
     adj.at(p1+m).push_back(p2+m);  //not p1 => not p2
     //reverse graph
     rg.at(p2).push_back(p1); 
     rg.at(p1+m).push_back(p2+m);
     rg.at(p1).push_back(p2); 
     rg.at(p2+m).push_back(p1+m); 
   }
   else{
     adj.at(p1+m).push_back(p2); // not p1 => p2
     adj.at(p2+m).push_back(p1);  //not p2 => p1
     adj.at(p1).push_back(p2+m); //p1 => not p2
     adj.at(p2).push_back(p1+m);  //p2 => not p1
     //reverse graph
     rg.at(p2).push_back(p1+m); 
     rg.at(p1).push_back(p2+m); 
     rg.at(p2+m).push_back(p1);
     rg.at(p1+m).push_back(p2);
   }
 }
 

 //Cerco le SCC s e se ci sono verifico che un nodo v non ha il suo negato all'interno di s
 // Zio Kosaraju
 //DFS su tutti i nodi del grafo
 //cout << "\nDFS: ";
 for(int i = 0; i < 2 * m; i++) {
  if(!visited[i]){
   normalDfs(i);
  }
 }
  
 visited.clear();
 for(int i = 0; i < m*2; i++){
  visited.push_back(false);
 }
 int c = 0;
 while(!postInsert.empty()){
  int v = postInsert.top();
  postInsert.pop();
  if(!visited[v])  {
   reversedDfs(v,c);
   c++;
  } 
 }
 for(int i=0;i<m;i++){
   if(scc[i] == scc[i+m]){
     cout << "NO" << endl;
     return 0;
   }
 }
 cout << "YES" << endl; 
 return 0;
}
