#include <stdio.h>

/*
 * Parto con un array a invertito rispetto l'input così da facilitare la lettura e stesura del codice
 *
 * Ho una matrice che rappresenta per ogni riga i 2 giocatori, la riga 0 sarà il giocatore inesperto mentre la riga 2 quello esperto.
 *
 * Formulazione dp:
 * 	Giocatore Esperto:
 *		dp[k][1] = min(dp[k-1][0], dp[k-2][0]) (Sceglie se uccidere uno o entrambi i boss)
 * 	Giocatore Inesperto (dobbiamo verificare tra i vari casi):
 * 		dp[k][0] = min(dp[k-1][1], dp[k-2][1]) 		se a[k] == 0 e a[k-1] == 0 (Sceglie se uccidere uno o entrambi i boss)
 * 		dp[k][0] = min(dp[k-1][1], dp[k-2][1] + 1) 	se a[k] == 0 e a[k-1] == 1 (Sceglie se uccidere un boss e switchare o entrambi i boss con lo skip)
 * 		dp[k][0] = min(dp[k-1][1] + 1, dp[k-2][1] + 1) 	se a[k] == 1 e a[k-1] == 0 (Sceglie se skippare il primo boss e uccidere il secondo o effettuare solamente lo skip )
 * 		dp[k][0] = min(dp[k-1][1] + 1, dp[k-2][1]) + 2)	se a[k] == 1 e a[k-1] == 1 (Sceglie se skippare il primo boss e switchare o se skippare entrambi i boss)
 *	
 *	Fatto ciò basta restituire l'ultimo elemento della matrice sulla riga del player insperto, dato che è lui a partire con il primo fight
 *	
 *	La complessità finale sarà O(n)
 * */

int main(){
	int t;
	int n;

	scanf("%d", &t);
	for(int i = 0; i < t; i++){
		scanf("%d", &n);

		int a[200000];
		int dp[200000][2];
		
		for(int k = 0; k < n; k++){
			scanf("%d", &a[n-k-1]);	
		}
		
		dp[0][1] = 0;
		dp[1][1] = 0;
		dp[0][0] = a[0];
		
		if(n==1){
			printf("%d\n", dp[0][0]);
		} else {
			if(a[1] == 0){
				dp[1][0] = 0;
			} else {
				dp[1][0] = 1;
			}
			
			for(int k = 2; k < n; k++){
				if(a[k] == 0){
					if(a[k-1] == 0){
						if(dp[k-1][1] < dp[k-2][1]){
							dp[k][0] = dp[k-1][1];
						} else {
							dp[k][0] = dp[k-2][1];
						}
					} 
					else if(a[k-1] == 1){
						if(dp[k-1][1] < dp[k-2][1] + 1){
							dp[k][0] = dp[k-1][1];
						} else {
							dp[k][0] = dp[k-2][1] + 1;
						}
					} 
				} else {
					if(a[k-1] == 0){
						if(dp[k-1][1] + 1 < dp[k-2][1] + 1){
							dp[k][0] = dp[k-1][1] + 1;
						} else {
							dp[k][0] = dp[k-2][1] + 1;
						}
					} 
					else if(a[k-1] == 1){
						if(dp[k-1][1] + 1 < dp[k-2][1] + 2){
							dp[k][0] = dp[k-1][1] + 1;
						} else {
							dp[k][0] = dp[k-2][1] + 2;
						}
					}
				}

				if(dp[k-1][0] < dp[k-2][0]) {
					dp[k][1] = dp[k-1][0];
				} else {
					dp[k][1] = dp[k-2][0];
				}
			}
			
			printf("%d\n", dp[n-1][0]);	
		}
	}

	return 0;
}
