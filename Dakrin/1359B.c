#include <stdio.h>

int main(){
	int tc;		//Test Cases
	int n,m;	//Teather Size
	int x,y;	//Costs relative to the single tile and the double tile
	int finalCost;
	int numberOfDots;
	char mat[100][1000];

	scanf("%d", &tc);

	for(int i = 0; i < tc; i++){
		scanf("%d",&n);
		scanf("%d", &m);
		scanf("%d", &x);
		scanf("%d", &y);

		finalCost = 0;
		numberOfDots = 0;

		for(int k = 0; k < n; k++){
			for(int w = 0; w < m; w++){
				scanf(" %c", &mat[k][w]);
				if(mat[k][w] == '.') {
					numberOfDots++;
				}
			}
		}

		// If the cost of the 1x2 tiles is more or equal then double the cost of
		// the 1x1 tile, then we simply use only the 1x1 tile
		if(x <= y/2) {
			finalCost = numberOfDots * x;
		} else {
			// If the cost of the 1x2 tile is less then the 1x1 tile then if
			// there are two consecutive '.' we prefere to use the 1x2 tile
			for(int k = 0; k < n; k++) {
				for(int w = 0; w < m; w++) {
					if(mat[k][w] == '.'){
						if(w+1 < m && mat[k][w+1] == '.') {
							finalCost += y;
							w++;
						} else {
							finalCost += x;
						}
					}
				}
			}
		}
		printf("%d\n",finalCost);
	}


	return 0;
}
