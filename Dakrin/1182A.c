#include <stdio.h>

//Immaggina a perde tempo a cercare come funziona pow se la puoi fare già intera dato che soddisfa sempre n%2 = 0
int intpow(int a, int b){
	int res = a;
	if (b == 0) {
		return 1;
	}
	
	for(int i = 0; i < b-1; i++) {
		res *= a;	
	}
	return res;
}

int main(){
	int n;
	scanf("%d", &n);

	//Ci sono sempre due possibilità per ogni 2 caselle. Se le caselle sono dispari non si potrà mai riempire
	if(n%2 == 0){
		printf("%d", intpow(2,n/2));
	} else {
		printf("%d", 0);
	}
	return 0;
}
