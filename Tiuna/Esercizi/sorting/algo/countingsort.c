#include <stdlib.h>
#include <string.h>

void 
countingsort (int *arr, int size, int bound)
{
    int *mem = malloc(bound * sizeof(int));
    memset (mem, 0, bound);
    for (int i = 0; i < size; i++)
        mem[arr[i]]++;
    for (int j = 0, i = 0; i < bound; i++)
        if (mem[i]) arr[j++] = i; 
    free (mem);
}
