/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */


/* void fix_heap(int node, int *heap, int size)
{
    if(left(node) >= size) return;
    else
    {
        int u;
        if(right(node) < size)
            u = heap[left(node)] > heap[right(node)] ? left(node) : right(node);
        else u = left(node);

        if(heap[node] < heap[u])
        {
            int tmp = heap[node];
            heap[node] = heap[u];
            heap[u] = tmp;
            fix_heap(u, heap, size);
        }
    }
}

void heapify(int t, int *heap, int size)
{
    if(t+1 > size)  return;
    heapify(left(t), heap, size);
    heapify(right(t), heap, size);
    fix_heap(t, heap, size);
}
*/

#define left(x) 2*x + 1
#define right(x) 2*x + 2

void 
fix_heap (int node, int *heap, int size)
{
    int u, tmp, l, r;
    while(left(node) < size)
    {
        l = left(node);
        r = right(node);
        if(r < size)
            u = heap[l] > heap[r] ? l : r;
        else u = l;

        if(heap[node] >= heap[u]) break;
        tmp = heap[node];
        heap[node] = heap[u];
        heap[u] = tmp;
        node = u;
    }
}
void 
heapify (int *heap, int size)
{
    for(int i = size/2; i >= 0; i--)
        fix_heap(i,heap,size);
}

void
heapsort (int *array, int size)
{
    heapify(array, size);

    while(size > 1)
    {
        int tmp = array[0]; 
        array[0] = array[size-1];
        array[size - 1] = tmp;
        fix_heap(0, array, --size); 
    }

}
