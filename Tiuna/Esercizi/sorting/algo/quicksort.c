/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#include <array-utils.h>
#include <stdlib.h>
#include <stdio.h>

int 
median (int *v, int x1, int x2, int x3)
{
    int a = v[x1];
    int b = v[x2];
    int c = v[x3];

    if (a < b) {
        if (b < c) return x2;
        else if (a < c) return x3;
        else return x1;
    } else {
        if (a < c) return x1;
        else if (b < c) return x3;
        else return x2;
    }
}

int partition(int a[], int l, int h)
{
    int x = a[h];
    int i = l - 1;
  
    for (int j = l; j <= h - 1; j++) {
        if (a[j] <= x) {
            i++;
            swap(&a[i], &a[j]);
        }
    }
    swap(&a[i + 1], &a[h]);
    return (i + 1);
}

void 
quicksort (int* a, int i, int f)
{
    if (i >= f) return;
    
    int size = f-i+1;
    int pivot = median (a, i + (rand () % size), 
                           i + (rand () % size), 
                           i + (rand () % size));

    int m = partition (a, i, pivot);
    quicksort (a, i, m - 1);
    quicksort (a, m + 1, f);
}

