/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <array-utils.h>

void 
print_array (int *arr, int sz)
{
    printf("[ ");
    for(int i = 0; i < sz; i++)
    {
        if(i > 0) printf(", ");
        printf("%d", arr[i]);
    }
    printf(" ]\n");
}

void 
swap (int *x, int*y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

void
get_rand_array (int **array, int size)
{
    if (*array) return;

    srand(time(NULL));
    
    *array = malloc (size * sizeof(int));
    for (int i = 0; i < size; i++){
        (*array)[i] = rand();
    }
}

int 
get_rand_array_rand_len (int **array, int maxlen)
{
    if (*array) return -1;

    srand(time(NULL));
    int size = rand() % maxlen;
    
    *array = malloc (size * sizeof(int));
    for (int i = 0; i < size; i++){
        (*array)[i] = rand() % ARRAY_INT_SIZE_MAX;
    }
    
    return size;
}
