/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#include <stdlib.h>

void merge(int *a, int l, int m, int r)
{
    int k = 0, i = l, j = m + 1, len = r - l + 1;
    int *copy = malloc(len * sizeof(int));
    while(i <= m && j <= r)
    {
        if(a[i] <= a[j]) copy[k++] = a[i++];
        else copy[k++] = a[j++]; 
    }
    while(i <= m) copy[k++] = a[i++];
    while(j <= r) copy[k++] = a[j++];

    for(int c = 0; c < len; c++) a[c + l] = copy[c]; 
    free(copy);
}

void mergesort(int *a, int l, int r)
{
    if(l < r) 
    {
        int m = (l +r)/2;
        mergesort(a, l, m);
        mergesort(a, m + 1, r);
        merge(a, l, m, r);
    }
}

