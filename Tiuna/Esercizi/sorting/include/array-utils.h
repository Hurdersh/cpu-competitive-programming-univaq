/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#ifndef ARRAY_UTILS_H
#define ARRAY_UTILS_H
#define ARRAY_INT_SIZE_MAX 10000
void
print_array (int *arr, int sz);

void 
swap (int *x, int *y);

int 
get_rand_array_rand_len (int **arr, int maxlen);

void
get_rand_array (int **array, int len);

#endif
