/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#ifndef ORDERING_H
#define ORDERING_H

void 
quicksort (int *array, int start, int end);

void 
mergesort (int *array, int start, int end);

void
heapsort (int *array, int size);

void 
countingsort (int *array, int size, int bound);

#endif
