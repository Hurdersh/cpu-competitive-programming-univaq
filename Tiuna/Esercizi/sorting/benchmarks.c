/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#include <array-utils.h>
#include <sorting.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAX_BENCHMARK 1000000
#define ITERATION_NO 20
int *array = NULL;

double benchmark_quick ()
{
    
}

int main(void)
{   
    puts ("average\t\tmerge\t\tquick\t\theap");
    for (int i = 1, size = 100; size <= MAX_BENCHMARK; i++) {
        float m_average = 0.0, q_average = 0.0, h_average = 0.0;
        int *buf = malloc (size * sizeof(int)); 
        
        get_rand_array (&array, size); 
        
        memcpy (buf, array, size);
        
        for (int i = 0; i < ITERATION_NO; i++) {
            clock_t t;
            t = clock();
            mergesort (array, 0, size - 1);
            t = clock() - t;
            double time_taken = ((double)t)/CLOCKS_PER_SEC;
            m_average += time_taken;
            memcpy (buf, array, size);
        }
        
        
        for (int i = 0; i < ITERATION_NO; i++) {
            clock_t t;
            t = clock();
            quicksort (array, 0, size - 1);
            t = clock() - t;
            double time_taken = ((double)t)/CLOCKS_PER_SEC;
            q_average += time_taken;
            memcpy (buf, array, size);
        }
        for (int i = 0; i < ITERATION_NO; i++) {
            clock_t t;
            t = clock();
            heapsort (array, size);
            t = clock() - t;
            double time_taken = ((double)t)/CLOCKS_PER_SEC;
            h_average += time_taken;
            memcpy (buf, array, size);
        }
        free (buf);
        free (array);
        array = NULL;

        m_average /= ITERATION_NO;
        q_average /= ITERATION_NO;
        h_average /= ITERATION_NO;
        printf("size : %d\t%f\t%f\t%f\n", size, m_average, q_average, h_average);

        if (i % 3 == 2) size = (size + (size/4))*2;
        else size *= 2;
    }
    return 0;
}
