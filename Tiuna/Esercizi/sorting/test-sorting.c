/*
 *  Author: Tiuna Angelini
 *  Date: 23-07-2022
 *  Language: C
 *  Problem name: Sorting
 */

#include <array-utils.h>
#include <sorting.h>
#include <stdlib.h>
#include <stdio.h>

int *array = NULL;

int main(void)
{
    puts ("sorting demo");
    int size = get_rand_array_rand_len (&array, 50);
    
    puts ("generated array: ");
    print_array (array, size);
    
    mergesort (array, 0, size - 1);
    puts ("mergesort: ");
    print_array (array, size);

    quicksort (array, 0, size - 1);
    puts ("quicksort : ");
    print_array (array, size);

    heapsort (array, size);
    puts ("heapsort : ");
    print_array (array, size);

    countingsort (array, size, ARRAY_INT_SIZE_MAX);
    puts ("integersort : ");
    print_array (array, size);

    free (array);
    return 0;
}
