# Algoritmi efficienti di ordinamento
Questo progetto mostra i tre algoritmi più efficienti per risolvere il problema dell'ordinamento
usando un modello basato sui confronti.

## Struttura progetto
```
.
├── algo
│   ├── quicksort.c
│   ├── countingsort.c
│   ├── heapsort.c
│   ├── mergesort.c
│   └── array-utils.c
│
├── include
│   ├── sorting.h
│   └── array-utils.h
│
├── test-sorting.c
├── benchmarks.c
├── makefile
└── README.md
```
i file algo/*sort.c contengono gli algoritmi di sorting, algo/array-utils.c contiene funzioni di utilità per gli algoritmi. In benchmarks.c si trova il codice relativo al benchmarking, e in test-array.c un esempio di utilizzo.
 
## Compilazione
Per compilare tutti i sorgenti del progetto è sufficiente eseguire
```
$ make
```
verranno prodotti due eseguibili all'interno della cartella build
build/bench esegue benchmarking sui tre algoritmi,
build/test genera un array casuale e lo ordina con tutti gli algoritmi

```
$ make test
$ make bench
```
