#ifndef STACK_H
#define STACK_H 1

#include "vector.h"

/*
 * Questo stack basato
 * su vettore non è altro che 
 * un wrapper del vettore
 * con un interfaccia leggermente diversa.
 */

typedef struct
{
    size_t capacity;
    size_t top;
    int data[];
} stack;


stack *
stack_new ()
{
    return (stack*) vec_new ();
}

void
stack_delete (stack *ref)
{
    free (ref);
}

void
stack_push (stack **ref, int x)
{
    vec_push_back((vector**)ref, x);
}

int
stack_pop (stack **ref)
{
    int res = (*ref)->data[(*ref)->top - 1];
    
    vec_pop_back ((vector**)ref);
    return res;
}

int 
stack_peek (stack **ref)
{
    return (*ref)->data[(*ref)->top - 1];
}

void
stack_shrink_to_fit (stack **ref)
{
    vec_shrink_to_fit ((vector**) ref);
}

stack *
stack_copy_stack (stack *st)
{
    stack *s = (stack*) vec_copy_vec ((vector*)st);
    return s;
}

stack *
stack_copy_arr (size_t size, int array[static size]) 
{
    stack *s = (stack*) vec_copy_arr (size, array);
    return s;
}


bool
stack_empty (stack **ref)
{
    return (*ref)->top == 0;
}

void
stack_print (stack *s) 
{
    vec_print ((vector*)s);
}

#endif /* STACK_H */
