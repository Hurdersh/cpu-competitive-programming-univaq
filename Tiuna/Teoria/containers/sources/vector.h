#ifndef VECTOR_H
#define VECTOR_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    size_t capacity;
    size_t size;
    int data[];
} vector;

/*
 * la macro VEC_FOREACH permette di iterare il 
 * vector senza dover riscrivere ogni volta il codice.
 */

#define VEC_FOREACH(x, v)\
    size_t i = 0;        \
    for (int *x = &v->data[i]; i < v->size; x = &v->data[++i])


/* 
 * La funzione vec_new alloca la memoria 
 * per una struttura vector di dimensione 0, 
 * e inizializza i suoi campi ai valori predefiniti.
 */

vector *
vec_new ()
{
    /*
     * Allochiamo la memoria per la struct e per un vettore
     * con un buffer interno di capacità pari a size
     */
    vector *vec = malloc (sizeof (vector) 
            + sizeof (int));
    
    vec->capacity = 1;
    vec->size = 0;
    
    return vec;
}

/* 
 * La funzione vec_new alloca la memoria 
 * per una struttura vector di dimensione size, 
 * e inizializza i suoi campi ai valori predefiniti.
 */

vector *
vec_sized_new (size_t size)
{
    /*
     * Allochiamo la memoria per la struct e per un vettore
     * con un buffer interno di capacità pari a size
     */
    vector *vec = malloc (sizeof (vector) 
            + size * sizeof (int));
    
    vec->capacity = size;
    vec->size = size;
    
    return vec;
}

void
vec_delete (vector **ref)
{
    free (*ref);
}

/*
 * La funzione vec_push_back inserisce 
 * un elemento alla fine del vettore.
 */

void
vec_push_back (vector **ref, int x)
{
    /*
     * Nel caso in cui la dimensione logica
     * raggiunge quella fisica, raddoppia
     * la capacità del vettore
     */
    vector *vec = *ref;
    if (vec->size == vec->capacity) {
        vec->capacity <<= 1;

        *ref = realloc (*ref, sizeof (vector) 
                + vec->capacity * sizeof (int));
        vec = *ref;
    }
    
    /* Inserisci l'elemento, 
     * e incrementa la size */
    vec->data[vec->size] = x;
    vec->size++;
}

/*
 * La funzione vec_pop_back elimina l'elemento
 * alla fine del vettore.
 */

void
vec_pop_back (vector **ref)
{
    vector *vec = *ref;

    vec->size--;

    /*
     * Anche qui se la dimensione logica del vettore
     * diventa meno della metà della sua capacità
     * essa verrà dimezzata
     */

    if (vec->size < (vec->capacity >> 1)) {
        vec->capacity >>= 1;
        *ref = realloc (*ref, sizeof (vector)
                + vec->capacity * sizeof (int));
    }
}

/*
 * La funzione vec_insert inserisce l'elemento
 * x nella posizione i-esima del vettore,
 * traslando tutti gli altri elementi a partire dal
 * vecchio i-esimo di una posizione in avanti.
 */

void
vec_insert (vector **ref, int x, size_t i)
{
    vector *vec = *ref;

    if (vec->size + 1 == vec->capacity) {
        vec->capacity <<= 1;
        *ref = realloc (*ref, sizeof (vector) 
                + vec->capacity * sizeof (int));
        
        vec = *ref;
    }

    memmove (&vec->data[i + 1], &vec->data[i], 
            (vec->size - i) * sizeof (int));

    vec->size++;
    vec->data[i] = x;
}

void
vec_erase (vector **ref, size_t i) 
{
    vector *vec = *ref;

    memmove (&vec->data[i], &vec->data[i + 1], 
            (vec->size - i + 1) * sizeof (int));

    vec->size--;

    if (vec->size < vec->capacity >> 1) {
        vec->capacity >>= 1;
        *ref = realloc (*ref, sizeof (vector)
                + vec->capacity * sizeof (int));
        
        vec = *ref;
    }
}

/*
 * La funzione vec_resize cambia la taglia del vettore
 * se la dimensione è minore di quella attuale, 
 * cancella gli elementi alla fine.
 */

void
vec_resize (vector **ref, size_t new_size)
{
    vector *vec = *ref;

    vec->size = new_size;
    
    if (vec->size >= vec->capacity) {
        vec->capacity <<= 1;
        *ref = realloc (*ref, sizeof (vector) 
                + vec->capacity * sizeof (int));
        
        vec = *ref;
    } else if (vec->size < (vec->capacity >> 1)) {
        vec->capacity >>= 1;
        *ref = realloc (*ref, sizeof (vector)
                + vec->capacity * sizeof (int));

        vec = *ref;
    }
}

void
vec_shrink_to_fit (vector **ref)
{
    vector *vec = *ref;
    vec->capacity = vec->size;
    *ref = realloc (*ref, sizeof (vector) 
            + vec->capacity * sizeof (int));
}

/*
 * Questa funzione copia una struttura vector
 * in un altra struttura vector, servendosi di
 * memcpy, funzione di libreria standard.
 */

vector *
vec_copy_vec (vector *vec)
{
    vector *v = vec_sized_new (vec->size);
    memcpy (v->data, vec->data, vec->size * sizeof (int)); 
    return v;
}

/*
 * Questa funzione crea un vettore a 
 * partire da un semplice array di interi.
 */

vector *
vec_copy_arr (size_t size, int array[static size]) 
{
    vector *v = vec_sized_new (size);
    memcpy (v->data, array, size * sizeof (int));
    return v;
}


bool
vec_empty (vector **ref)
{
    return (*ref)->size == 0;
}

void
vec_print (vector *vec) 
{
    printf ("{");
    for (size_t i = 0; i < vec->size; i++) {
        printf ("%d, ", vec->data[i]); 
    }
    printf ("\b\b}\n");
}

#endif /* VECTOR_H */
