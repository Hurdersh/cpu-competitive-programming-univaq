#ifndef LIST_H
#define LIST_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node 
{
    struct node *next;
    struct node *prev;
    int value;
} node;

typedef struct
{
    size_t size;
    node *front;
    node *back;
} list;

#define LIST_FOREACH(e,l) \
    for(node *e = l->front; e != NULL; e = e->next)

list *
list_new () 
{
    list *l = malloc (sizeof *l);
    
    l->size = 0;
    l->back = NULL;
    l->front = NULL;
    return l;
}

void
list_delete (list *l)
{
    free (l);
}

void
list_push_front (list *l, int x)
{
    node *new_node = malloc (sizeof (node));
    new_node->value = x;
    
    new_node->next = l->front;
    new_node->prev = NULL;
     
    if (l->size == 0) {
        l->back = new_node;
    } else {
        l->front->prev = new_node;
    }
    
    l->front = new_node;
    l->size++;
} 

void 
list_push_back (list *l, int x) 
{
    node *new_node = malloc (sizeof (node));
    new_node->value = x;

    new_node->prev = l->back;
    new_node->next = NULL;
    
    if (l->size == 0) {
        l->front = new_node;
    } else {
        l->back->next = new_node;
    }
    
    l->back = new_node;
    l->size++;
}

void
list_pop_back (list *l)
{
    node *tmp = l->back;

    l->size--;
    l->back = l->back->prev;
    l->back->next = NULL;

    free(tmp);
}

void
list_pop_front (list *l)
{
    node *tmp = l->front;

    l->size--;
    l->front = l->front->next;
    l->front->prev = NULL;
    
    free(tmp);
}


void
list_resize_set (list *l, size_t size, int x)
{
    if (size < l->size) {
        for (size_t i = l->size; i > size; --i) {
            list_pop_back (l);
        } 
    } else if (size > l->size) {
        for (int i = l->size; i < size; i++) {
            list_push_back (l, x);
        }
    }
    l->size = size;
}

void
list_resize (list *l, size_t size)
{
    list_resize_set (l, size, 0);
}


list *
list_from_arr (size_t size, int array[static size])
{
    list *l = list_new ();
    for (size_t i = 0; i < size; i++) {
        list_push_back (l, array[i]);
    }
    return l;
}

void 
list_print (list *l)
{
    printf ("{");
    LIST_FOREACH (elem, l) {
        printf ("%d, ", elem->value);
    }
    printf ("\b\b}\n");
}

#endif /* LIST_H */
