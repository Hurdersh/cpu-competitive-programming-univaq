#include <list.h>

int 
main (void)
{
    /*  //  //  //  //  //  //  //  //  //  //  *
     *                  LIST                    *
     *  //  //  //  //  //  //  //  //  //  //  */

    list *l = list_from_arr (5, (int[]) {1,7,4,8,0});
    list_print (l);

    list_push_front (l, 13);

    printf ("push_front (13) : ");
    list_print (l);

    list_delete (l);

    l = list_new ();
    list_push_front (l,13);
    printf ("size : %ld", l->size); list_print (l);
    list_push_front (l,0);
    printf ("size : %ld", l->size); list_print (l);
    list_push_front (l,3);
    printf ("size : %ld", l->size); list_print (l);
    list_push_front (l,1);
    printf ("size : %ld", l->size); list_print (l);
    list_push_front (l,5);
    printf ("size : %ld", l->size); list_print (l);
    list_pop_back(l);
    printf ("size : %ld", l->size); list_print (l);
    list_resize (l, 6);
    printf ("size : %ld", l->size); list_print (l);
    list_pop_front (l);
    list_resize (l, 3);
    printf ("size : %ld", l->size); list_print (l);

    return 0;
}
