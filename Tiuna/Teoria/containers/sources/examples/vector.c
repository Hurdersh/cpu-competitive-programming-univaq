#include "vector.h"
#include "vector-stack.h"


/* NOTA: Questi non sono veri numeri casuali! */
void 
fill_random_vector (vector *vec)
{
    VEC_FOREACH (elem, vec) {
        *elem = rand ();
    }
}

int main (void)
{
    /*  //  //  //  //  //  //  //  //  //  //  *
     *                   VECTOR                 *
     *  //  //  //  //  //  //  //  //  //  //  */
    

    vector *v = vec_sized_new (6);
    printf ("Creating a vector at %p\n", v);
    
    fill_random_vector (v);
    vec_print (v);

    
    vec_push_back (&v, 32);
    
    printf ("push_back(32) : ");
    vec_print (v);

    
    vec_pop_back (&v);
    
    printf ("pop_back() : ");
    vec_print (v);

    
    vec_resize (&v, 1);    
    
    printf ("resize(1) : ");
    vec_print (v);

    vec_delete (&v);

    
    v = vec_copy_arr (4, (int []){2, 3, 6, 7});

    printf ("copy_arr({2,3,6,7}) : ");
    vec_print (v);

    
    vec_insert (&v, 69, 2);
    
    printf ("insert(69, 2) : ");
    vec_print (v);

    
    vec_erase (&v, 1);
    
    printf ("erase(1) : ");
    vec_print (v);

    /*  //  //  //  //  //  //  //  //  //  //  *
     *                  STACK                   *
     *  //  //  //  //  //  //  //  //  //  //  */
  
    stack *s = stack_new (); 
    printf ("\n\nCreating a stack at %p\n", s); 


    stack_push (&s, 4);
    
    printf ("push(4) : ");
    stack_print (s);


    stack_push (&s, 5);
    
    printf ("push(5) : ");
    stack_print (s);
    
    
    stack_push (&s, 21);
    
    printf ("push(21) : ");
    stack_print (s);

 
    stack_push (&s, 42);

    printf ("push(42) : ");
    stack_print (s);
    
    
    
    int e = stack_pop (&s);
    
    printf ("pop() : %d = ", e);
    stack_print (s);
    
}
