/* -----------------------
 *  Problem: Grass Hopper
 * -----------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C++
 */

#include <iostream>

#define GREEN "\033[;32m"
#define RESET "\033[0m"

/* INPUT SPECIFICATION
 * You are given an integer n, the length of A, 
 * and n integers a_i, the wheight of each location
 */

static void minpath (size_t n, int **a, long long **dp) 
{
    // base case
    dp[0][0] = 0;

    // first row
    for (size_t i = 1; i < n; ++i) {
        dp[i][0] = a[i][0] + dp[i-1][0];
    }

    // first column
    for (size_t j = 1; j < n; ++j) {
        dp[0][j] = a[0][j] + dp[0][j-1];
    }

    // induction
    for (size_t i = 1; i < n; ++i) {
        for (size_t j = 1; j < n; ++j) {
            dp[i][j] = a[i][j] + imax (dp[i-1][j], dp[i][j-1]);
        }
    } 
}


static void printroute (int n, int **a, long long **dp)
{
    bool **backtrack = new bool*[n];
    for (size_t i = 0; i < n; i++) {
        backtrack[i] = new bool[n]{};
    }

    backtrack[n-1][n-1] = true;
    backtrack[0][0] = true;

    for (size_t i = n - 1, j = n - 1; (i > 0) && (j > 0); ) {
        // notice that we will never go out of bounds, because if 
        // i == 1 then it means that by construction the if condition
        // is true
        if (dp[i][j] == dp[i-1][j] + a[i][j]) {
            i--;
            backtrack[i][j] = true;
        } else { // dp[i] == dp[i-2] + a[i]
            j--;
            backtrack[i][j] = true;
        }
    }

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if (backtrack[i][j]) {
                std::cout << GREEN 
                    << a[i][j] << " " << RESET;
            } else {
                std::cout << a[i][j] << " ";
            }
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
    delete [] backtrack;
}

int main ()
{
    size_t n;
    std::cin >> n;
   
    if (n <= 1) {
        std::cerr << "n must be strictly greater than 1!!" 
            << std::endl;
        return -1;
    }

    int **a = new int*[n];
    for (size_t i = 0; i < n; i++) {
        a[i] = new int[n];
    }

    long long **dp = new long long*[n];
    for (size_t i = 0; i < n; i++) {
        dp[i] = new long long[n];
    }   

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            std::cin >> a[i][j];
        }
    }
    
    if (n > 2) {
        minpath (n, a, dp);
    } else {
        dp[1][1] = a[0][1] + a[1][0];
    }

    std::cout << "The maximal path for the turtle has weight " 
        << dp[n-1] << "\n";
    std::cout << "and the route is:" << "\n";

    printroute (n, a, dp);

    return 0;
}
