/* -----------------------
 *  Problem: Turtle
 * -----------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define GREEN "\033[;32m"
#define RESET "\033[0m"

/* INPUT SPECIFICATION
 * You are given an integer n, the length of A, 
 * and n integers a_i, the wheight of each location
 */

static inline int 
imax (const int i1, const int i2)
{
    return i1 > i2 ? i1 : i2;
}

static void 
minpath (size_t n, const int a[n][n], 
        long long dp[n][n]) 
{
    // base case
    dp[0][0] = 0;

    // first row
    for (size_t i = 1; i < n; ++i) {
        dp[i][0] = a[i][0] + dp[i-1][0];
    }

    // first column
    for (size_t j = 1; j < n; ++j) {
        dp[0][j] = a[0][j] + dp[0][j-1];
    }

    // induction
    for (size_t i = 1; i < n; ++i) {
        for (size_t j = 1; j < n; ++j) {
            dp[i][j] = a[i][j] + imax (dp[i-1][j], dp[i][j-1]);
        }
    } 
}


static void 
printroute (size_t n, const int a[n][n], 
        const long long dp[n][n])
{
    bool backtrack[n][n];

    for (size_t i = 0; i < n; ++i) {
        memset(backtrack[i], false, n * sizeof *backtrack);
    }

    backtrack[n-1][n-1] = true;
    backtrack[0][0] = true;

    for (size_t i = n - 1, j = n - 1; (i > 0) && (j > 0); ) {
        // notice that we will never go out of bounds, because if 
        // i == 1 then it means that by construction the if condition
        // is true
        if (dp[i][j] == dp[i-1][j] + a[i][j]) {
            i--;
            backtrack[i][j] = true;
        } else { // dp[i] == dp[i-2] + a[i]
            j--;
            backtrack[i][j] = true;
        }
    }

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if (backtrack[i][j]) {
                printf ("%s%d%s ", GREEN, a[i][j], RESET);
            } else {
                printf ("%d ", a[i][j]);
            }
        }
        puts("");
    }

    puts ("");
}

int 
main (void)
{
    size_t n;
    scanf ("%lu", &n);
   
    if (n <= 1) {
        fprintf (stderr, "n must be strictly greater than 1!!\n");
        return -1;
    }

    int a[n][n];
    long long dp[n][n];
    

    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            scanf ("%d", &a[i][j]);
        }
    }
    
    if (n > 2) {
        minpath (n, a, dp);
    } else {
        dp[1][1] = imax (a[0][1], a[1][0]);
    }

    printf ("The maximal path for the turtle has weight ");
    printf ("%lld\n", dp[n-1][n-1]);
    printf ("and the route is:\n");

    printroute (n, a, dp);

    return 0;
}
