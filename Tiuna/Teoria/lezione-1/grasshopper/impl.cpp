/* -----------------------
 *  Problem: Grass Hopper
 * -----------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C++
 */

#include <iostream>

#define GREEN "\033[;32m"
#define RESET "\033[0m"

/* INPUT SPECIFICATION
 * You are given an integer n, the length of A, 
 * and n integers a_i, the wheight of each location
 */

static void minpath (int n, const int *a, long long *dp) 
{
    dp[0] = 0;
    dp[1] = a[1];

    for (int i = 2; i < n; ++i) {
        dp[i] = std::min (dp[i-1], dp[i-2]) + a[i];
    } 
}


static void printroute (int n, const int *a, const long long *dp)
{
    bool *backtrack = new bool[n]{};

    backtrack[n-1] = true;

    for (int i = n - 1; i > 0; ) {
        // notice that we will never go out of bounds, because if 
        // i == 1 then it means that by construction the if condition
        // is true
        if (dp[i] == dp[i-1] + a[i]) {
            i--;
            backtrack[i] = true;
        } else { // dp[i] == dp[i-2] + a[i]
            i = i - 2;
            backtrack[i] = true;
        }
    }

    for (int i = 0; i < n; ++i) {
        if (backtrack[i]) {
            std::cout << GREEN 
                << a[i] << " " << RESET;
        } else {
            std::cout << a[i] << " ";
        }
    }

    std::cout << std::endl;
    delete [] backtrack;
}

int main ()
{
    int n;
    std::cin >> n;
   
    if (n <= 1) {
        std::cerr << "n must be strictly greater than 1!!" 
            << std::endl;
        return -1;
    }

    int *a  = new int[n];
    long long *dp = new long long[n];
    

    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
    }
    
    if (n > 2) {
        minpath (n, a, dp);
    } else {
        dp[0] = 0;
        dp[1] = a[1];
    }

    std::cout << "The minimal route for the grasshopper has weight " 
        << dp[n-1] << "\n";
    std::cout << "and the route is:" << "\n";

    printroute (n, a, dp);

    return 0;
}
