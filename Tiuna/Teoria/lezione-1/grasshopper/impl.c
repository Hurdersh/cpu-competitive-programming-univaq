/* -----------------------
 *  Problem: Grass Hopper
 * -----------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define GREEN "\033[;32m"
#define RESET "\033[0m"

/* INPUT SPECIFICATION
 * You are given an integer n, the length of A, 
 * and n integers a_i, the wheight of each location
 */

static inline int 
imin (const int i1, const int i2)
{
    return i1 <= i2 ? i1 : i2;
}

static void 
minpath (size_t n, const int a[static n], 
        long long dp[static n]) 
{
    dp[0] = 0;
    dp[1] = a[1];

    for (size_t i = 2; i < n; ++i) {
        dp[i] = imin (dp[i-1], dp[i-2]) + a[i];
    } 
}


static void 
printroute (size_t n, const int a[static n], 
        const long long dp[static n])
{
    bool backtrack[n];
    memset(backtrack, false, n * sizeof *backtrack);

    backtrack[n-1] = true;

    for (size_t i = n - 1; i > 0; ) {
        // notice that we will never go out of bounds, because if 
        // i == 1 then it means that by construction the if condition
        // is true
        if (dp[i] == dp[i-1] + a[i]) {
            i--;
            backtrack[i] = true;
        } else { // dp[i] == dp[i-2] + a[i]
            i = i - 2;
            backtrack[i] = true;
        }
    }

    for (size_t i = 0; i < n; ++i) {
        if (backtrack[i]) {
            printf ("%s%d%s ", GREEN, a[i], RESET);
        } else {
            printf ("%d ", a[i]);
        }
    }

    puts ("");
}

int 
main (void)
{
    size_t n;
    scanf ("%lu", &n);
   
    if (n <= 1) {
        fprintf (stderr, "n must be strictly greater than 1!!\n");
        return -1;
    }

    int a[n];
    long long dp[n];
    

    for (size_t i = 0; i < n; ++i) {
        scanf ("%d", &a[i]);
    }
    
    if (n > 2) {
        minpath (n, a, dp);
    } else {
        dp[0] = 0;
        dp[1] = a[1];
    }

    printf ("The minimal route for the grasshopper has weight ");
    printf ("%lld\n", dp[n-1]);
    printf ("and the route is:\n");

    printroute (n, a, dp);

    return 0;
}
