/* --------------------------------
 *  Algorithm: Topological Sorting
 * --------------------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* INPUT SPECIFICATION:
 * You are given two integer n and m, the number of vertices and arcs,
 * followed by m lines containing two integers u v, representing an arc.
 * NOTE: THE VERTICES ARE EXPECTED TO LIE IN 1 <= V <= N
 */

typedef struct node
{
    size_t pos;
    int value;
    struct node *next;
} node;

static inline node *
node_new (int value)
{
    node *e = malloc (sizeof (node));
    e->value = value;
    return e;
}

typedef struct 
{
    int  n;      // number of nodes
    int  *indeg; // in degree
    node *adj[]; // adjacency list of the digraph
} digraph;

static inline digraph *
digraph_new (int n) 
{
    // allocating the graph on the heap
    digraph *g = malloc (sizeof (digraph) 
            + (n + 1) * sizeof (node*));
    // allocating indegree array
    g->indeg = malloc ((n + 1) * sizeof (int));
    // null initialize adjacency list
    memset (g->adj, 0, (n + 1) * sizeof (node*));
    memset (g->indeg, 0, (n + 1) * sizeof (int));

    g->n = n;

    return g;
}

static void
digraph_add_edge (digraph *g, int u, int v)
{
    node *new_node = node_new (v);
    // add node to the back of the list
    new_node->next = g->adj[u];
    g->adj[u] = new_node;
    g->indeg[v]++;
}

static void 
digraph_free (digraph *g) 
{
    for (int i = 0; i < g->n; i++) {
        node *e = g->adj[i];
        while (e != NULL) {
            node *tmp = e;
            e = e->next;
            free (tmp);
        }
    }
    free (g);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static int* 
topological_sort (digraph *g) // does not find cycles
{
    int res_pos = 0;
    int *res = malloc (g->n * sizeof (int));
    
    // create a queue: this is used to avoid recursion
    int front = 0, tail = 0; 
    int queue[g->n];

    for (int i = 1; i <= g->n; i++) {
        if (g->indeg[i] == 0) {
            queue[tail] = i;
            tail = (tail + 1) % g->n;
        }
    }
    
    // count the nodes we visit
    int count = 0;

    while (front != tail) { // while the queue is not empty 
        // pop the queue
        int current = queue[front];
        front = (front + 1) % g->n;
       
        res[res_pos++] = current;

        // for each neighbour
        for (node *e = g->adj[current]; e != NULL; e = e->next) {
            // take the reference to it
            int neighbour = e->value;
            // decrement the indegree, we are removing current
            g->indeg[neighbour]--; 
            // if no other node enters in neighbour
            if (g->indeg[neighbour] == 0) {
                // push it in the queue
                queue[tail] = neighbour;
                tail = (tail + 1) % g->n;
            }
        }
        count++;
    }

    if (count != g->n) {
        return NULL;
    }

    return res;
}

int
main (void)
{
    int n, m;
    scanf ("%d%d", &n, &m);
   
    if (n < 1 || m < 1) {
        fprintf (stderr, "n and m must be positive!!\n");
        return -1;
    }

    digraph *g = digraph_new (n);

    int source, u, v;
    
    // remember: the first node we read 
    // is the source of the visit
    scanf ("%d%d", &source, &v);
    digraph_add_edge (g, source, v);
    // read the rest of the nodes 
    for (int i = 0; i < m - 1; ++i) {
        scanf ("%d%d", &u, &v);
        digraph_add_edge (g, u, v);
    }

    int *res = topological_sort (g); // starting the visit from node v

    if (res == NULL) {
        printf ("the graph is not a DAG!!\n");
        return 0;
    }

    printf ("Topological sort: "); 
    for (int i = 0; i < n; i++) {
        printf ("%d ", res[i]);
    }
    puts ("");
    
    digraph_free (g);
    return 0;
}
