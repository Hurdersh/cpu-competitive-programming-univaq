/* ------------------------------
 *  Algorithm: Depth First Search
 * ------------------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C++
 */

#include <iostream>
#include <list>
#include <vector>

/* INPUT SPECIFICATION:
 * You are given two integer n and m, the number of vertices and arcs,
 * followed by m lines containing two integers u v, representing an arc.
 * and the first node in input is the source of the visit
 * NOTE: THE VERTICES ARE EXPECTED TO LIE IN 1 <= V <= N
 */

class digraph 
{
public:
    digraph (int n)
    {
        this->n = n;
        visited = new bool[n+1]{};
        adj.resize (n+1);
    }
    ~digraph () 
    {
        delete [] visited;
    }

    void add_edge (int u, int v) 
    {
        adj[u].push_back(v);
    }

    void depth_first_search (const int source)
    {
        visited[source] = true;
        visit (source);
        
        for (const int &neighbour : adj[source]) {
            if (!visited[neighbour]) {
                depth_first_search (neighbour);
            }
        }
    }

private:
    static inline void visit (int node)
    {
        printf ("visiting: %d\n", node);
    }

    std::vector<std::list<int>> adj;
    bool *visited;
    int n;
};

int main ()
{
    int n, m;
    std::cin >> n >> m;
   
    if (n < 1 || m < 1) {
        std::cerr << "n and m must be positive!!\n";
        return -1;
    }

    digraph *g = new digraph (n);

    int source, v;
    std::cin >> source >> v;
    g->add_edge (source, v);

    int u;
    for (int i = 0; i < m - 1; ++i) {
        std::cin >> u >> v;
        g->add_edge (u, v);
    }

    std::cout << "STARTING the DFS\n";
    g->depth_first_search (source); 
        
    delete g;
    return 0;
}
