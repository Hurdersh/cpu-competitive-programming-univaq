/* ------------------------------
 *  Algorithm: Depth First Search
 * ------------------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* INPUT SPECIFICATION:
 * You are given two integer n and m, the number of vertices and arcs,
 * followed by m lines containing two integers u v, representing an arc.
 * and the first node in input is the source of the visit
 * NOTE: THE VERTICES ARE EXPECTED TO LIE IN 1 <= V <= N
 */

typedef struct node
{
    size_t pos;
    int value;
    struct node *next;
} node;

static node *
node_new (int value)
{
    node *e = malloc (sizeof (node));
    e->value = value;
    return e;
}

typedef struct 
{
    int  n;        // number of nodes
    node *adj[];    // adjacency list of the digraph
} digraph;

static inline digraph *
digraph_new (int n) 
{
    // allocating the graph on the heap
    digraph *g = malloc (sizeof (digraph) 
            + (n + 1) * sizeof (node*));
    // null initialize adjacency list
    memset (g->adj, 0, (n + 1) * sizeof (node*));
    g->n = n;
    return g;
}

static void
digraph_add_edge (digraph *g, int u, int v)
{
    node *new_node = node_new (v);
    // add node to the back of the list
    new_node->next = g->adj[u];
    g->adj[u] = new_node;
}

static void 
digraph_free (digraph *g) 
{
    for (int i = 0; i < g->n; i++) {
        node *e = g->adj[i];
        while (e != NULL) {
            node *tmp = e;
            e = e->next;
            free (tmp);
        }
    }
    free (g);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static inline void
visit (int node)
{
    printf ("visiting: %d\n", node);
}

static void 
depth_first_search (digraph *g, int source)
{
    // this array keeps track of what we have visited
    // 0 means never seen
    // 1 means seen but not visited
    // 2 means visited
    int visited[g->n+1];
    // and we initalize it to all 0
    memset (visited, 0, (g->n + 1) * sizeof (int));

    // create a stack: this is used to avoid recursion
    int top = 0;
    int stack[g->n];

    stack[top++] = source; 
    visited[source] = 1;

    while (top != 0) { // while the stack is not empty 
        // pop the stack
        int current = stack[--top]; 
            
        // visit the node
        visit (current);
        visited[current] = 2;

        // for each neighbour
        for (node *e = g->adj[current]; e != NULL; e = e->next) {
            // take the reference to it
            int neighbour = e->value;
            
            // if we have never seen it
            if (visited[neighbour] == 0) {
                // set we have seen it
                visited[neighbour] = 1;
                // push it on the stack
                stack[top++] = neighbour;
            }
        }
    }
}

int
main (void)
{
    int n, m;
    scanf ("%d%d", &n, &m);
   
    if (n < 1 || m < 1) {
        fprintf (stderr, "n and m must be positive!!\n");
        return -1;
    }

    digraph *g = digraph_new (n);

    int source, u, v;
    
    // remember: the first node we read 
    // is the source of the visit
    scanf ("%d%d", &source, &v);
    digraph_add_edge (g, source, v);
    // read the rest of the nodes 
    for (int i = 0; i < m - 1; ++i) {
        scanf ("%d%d", &u, &v);
        digraph_add_edge (g, u, v);
    }

    puts ("STARTING the DFS:"); 
    depth_first_search (g, source); // starting the visit from node v
    
    digraph_free (g);
    return 0;
}
