/* --------------------------------------------------------
 *  Problem: Binary numbers of k digits not containing '11'
 * --------------------------------------------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C++
 */

#include <iostream>

/* INPUT SPECIFICATION
 * The input is a single integer k, the number of digits
 */

long long
numbers_count (int k)
{
    if (k == 0) { // there is only one string of zero digits
        return 1;
    }

    long long dp[k+1];
    
    dp[0] = 1;    
    dp[1] = 2;    // and for 1 digit we have 0 and 1.

    for (int i = 2; i <= k; ++i) {
        dp[i] = dp[i-1] + dp[i-2];
    } 
    return dp[k];
}


int
main (void)
{
    int k;
    std::cin >> k;

    if (k < 0) {
        std::cerr << "You must provide a nonnegative integer!\n";
        return -1;
    }

    long long res = numbers_count (k);

    std::cout << "There are " << res 
        << " binary numbers with " 
        << k << " digits not containing '11'" << std::endl;
    return 0;
}
