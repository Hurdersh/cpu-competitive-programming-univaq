/* --------------------------------------------------------
 *  Problem: Binary numbers of k digits not containing '11'
 * --------------------------------------------------------
 *  Author: Tiuna Angelini
 *  Date: 03-11-2022
 *  Language: C
 */

#include <stdio.h>

/* INPUT SPECIFICATION
 * The input is a single integer k, the number of digits
 */

long long
numbers_count (int k)
{
    if (k == 0) { // there is only one string of zero digits
        return 1;
    }

    long long dp[k+1];
    
    dp[0] = 1;    
    dp[1] = 2;    // and for 1 digit we have 0 and 1.

    for (int i = 2; i <= k; ++i) {
        dp[i] = dp[i-1] + dp[i-2];
    } 

    return dp[k];
}


int
main (void)
{
    int k;
    scanf ("%d", &k);

    if (k < 0) {
        fprintf (stderr,"You must provide a nonnegative integer!\n");
        return -1;
    }

    long long res = numbers_count (k);

    printf ("There are %lld binary numbers with ", res);
    printf ("%d digits not containing '11'\n", k);
    return 0;
}
