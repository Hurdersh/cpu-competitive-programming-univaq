# CPU Competitive Programming Univaq
Snippets di codice ed esercizi orientati all'allenamento, le reference ai singoli esercizi si trovano nelle slides

## Description
Gli Studenti sono invitati ad inviare le proprie soluzioni per discuterle e nel caso pubblicarle, attribuendo tutti i meriti del caso.

## Contatti
Giordano Colli = giordano.colli@student.univaq.it

Tiuna Angelini = tiunapierangelo.angelini@student.univaq.it

Katiuscia Pellone = katiuscia.pellone@student.univaq.it

Luca Forlizzi = luca.forlizzi@univaq.it

## Contributing
Giordano Colli

Tiuna Angelini

Katiuscia Pellone

Luca Forlizzi

## License
This project is licensed with the GPL 3.0 Open Source License

